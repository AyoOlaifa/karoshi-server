#!/bin/bash
#Copyright (C) 2007  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/global_prefs

TITLE=$"Student Internet Logs"
TITLEHELP=$"Internet logs are updated every three minutes."
HELPURL="#"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Custom scripts
echo '
<script language="JavaScript" src="/all/calendar2/calendar_eu.js"></script>
        <!-- Timestamp input popup (European Format) -->
<link rel="stylesheet" href="/all/calendar2/calendar.css">'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

DATE_INFO=$(date +%F)
DAY=$(echo "$DATE_INFO" | cut -d- -f3)
MONTH=$(echo "$DATE_INFO" | cut -d- -f2)
YEAR=$(echo "$DATE_INFO" | cut -d- -f1)

echo '
<form action="/cgi-bin/staff/dg_view_student_user_logs.cgi" name="testform" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Username"'</td>
				<td><div id="suggestions"></div><input class="karoshi-input" tabindex= "3" name="_USERNAME_" AUTOCOMPLETE = "off" size="14" type="text" id="inputString" onkeyup="lookup(this.value);"></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the name of the student that you want to check the internet logs for."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Log Date"'</td>
				<td>'
echo "<!-- calendar attaches to existing form element -->
	<input class=\"karoshi-input\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 name=\"_DATE_\"></td>
			<td>
	<script>
	new tcal ({
		// form name
		'formname': 'testform',
		// input name
		'controlname': '_DATE_'
	});

	</script>"

echo '
				</td>
			</tr>
			<tr>
				<td>'$"User Photo"'</td>
				<td><div id="photobox"><img alt="photo" src="/images/blank_user_image.jpg" width="120" height="150"></div></td>
				<td></td>
			</tr>
			

	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</display-karoshicontent>
</form>
</body>
</html>'
exit

