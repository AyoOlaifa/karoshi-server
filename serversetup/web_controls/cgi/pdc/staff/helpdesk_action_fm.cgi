#!/bin/bash
#Copyright (C) 2010  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/global_prefs

TITLE=$"Technical Support"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\.%+_:\-')
#########################
#Assign data to variables
#########################
END_POINT=5
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign JOBNAME
DATANAME=JOBNAME
get_data
JOBNAME="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/staff/helpdesk_view_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################
#Check to see that JOBNAME is not blank
if [ -z "$JOBNAME" ]
then
	MESSAGE=$"The job name cannot be blank."
	show_status
fi

if [ ! -f /opt/karoshi/server_network/helpdesk/todo/"$JOBNAME" ]
then
	MESSAGE=$"This job does not exist."
	show_status
fi

#Get data
source /opt/karoshi/server_network/helpdesk/todo/"$JOBNAME"
source /opt/karoshi/web_controls/version

#Show job data
echo '
<form action="/cgi-bin/staff/helpdesk_view_fm.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Request Summary"'</td>
				<td>'"$JOBTITLE"'</td>
			</tr>
			<tr>
				<td>'$"Name"'</td>
				<td>'"$NAME"'</td>
			</tr>
			<tr>
				<td>'$"Location"'</td>
				<td>'"$LOCATION"'</td>
			</tr>
			<tr>
				<td>'$"Department"'</td>
				<td>'"$DEPARTMENT"'</td>
			</tr>
			<tr>
				<td>'$"Category"'</td>
				<td>'"$CATEGORY"'</td>
			</tr>
			<tr>
				<td>'$"Extended Details"'</td>
				<td>'"$REQUEST"'</td>
			</tr>
			<tr>
				<td>'$"Feedback"'</td>
				<td>'"$FEEDBACK"'</td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Back"'" type="submit">
</form>
</display-karoshicontent>
</body>
</html>'
exit

