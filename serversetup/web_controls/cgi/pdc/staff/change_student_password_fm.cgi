#!/bin/bash
#Copyright (C) 2007  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Change a Student's Password"
TITLEHELP=$"This will change the password of the user for access to all servers on the Karoshi system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Custom scripts
echo '<script src="/all/js/script.js"></script>'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+-')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign PASSWORD1
DATANAME=PASSWORD1
get_data
PASSWORD1="$DATAENTRY"

#Assign PASSWORD2
DATANAME=PASSWORD2
get_data
PASSWORD2="$DATAENTRY"

#Check password settings
source /opt/karoshi/server_network/security/password_settings

echo '
<form action="/cgi-bin/staff/change_student_password.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Username"'</td>
 				<td><div id="suggestions"></div><input class="karoshi-input" tabindex= "1" name="____USERNAME____" value="'"$USERNAME"'" size="20" type="text" id="inputString" onkeyup="lookup(this.value);"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"Enter in the username that you want to change the password for."'</span></a></td>
			</tr>
			<tr>
 				<td>'$"New Password"'</td>
				<td><input class="karoshi-input" tabindex= "2" name="____PASSWORD1____" value="'"$PASSWORD1"'" size="20" type="password"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"Enter in the new password that you want the user to have."'<br><br>'
[ "$PASSWORDCOMPLEXITY" = on ] && echo ''$"Upper and lower case characters and numbers are required."'<br><br>'
echo ''$"The Minimum password length is "''"$MINPASSLENGTH"'.<br><br>'$"The following special characters are allowed"'<br><br> space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~</span></a>
				</td>
			</tr>
			<tr>
				<td>'$"Confirm New Password"'</td>
				<td><input class="karoshi-input" tabindex= "3" name="____PASSWORD2____" value="'"$PASSWORD2"'" size="20" type="password"></td>
			</tr>
			<tr>
				<td>'$"User Photo"'</td>
				<td><div id="photobox"><img alt="photo" src="/images/blank_user_image.jpg" width="120" height="150"></div></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit

