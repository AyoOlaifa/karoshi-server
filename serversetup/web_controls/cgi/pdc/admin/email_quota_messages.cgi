#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Quota Warning Messages"
TITLEHELP=$"These are the messages that are sent to users who have reached a certain level on their mailbox."
HELPURL="#"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "email_quota_messages.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

###########################
#Get current email messages
###########################
if [ ! -f /opt/karoshi/postfixdata/warning_messages/level1 ] || [ ! -f /opt/karoshi/postfixdata/warning_messages/level2 ] || [ ! -f /opt/karoshi/postfixdata/warning_messages/level3 ] || [ ! -f /opt/karoshi/postfixdata/warning_messages/level4 ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_quota_messages.cgi | cut -d' ' -f1)
	sudo -H /opt/karoshi/web_controls/exec/email_get_quota_messages "$REMOTE_USER:$REMOTE_ADDR:$Checksum"
fi

echo '<form action="/cgi-bin/admin/email_quota_messages2.cgi" method="post">'

#Level1
echo '<b>'$"Level"' 1</b><br>'
if [ -f /opt/karoshi/postfixdata/warning_messages/level1 ]
then
	echo -e \<textarea cols=\"80\" rows=\"10\" name=\"_LEVEL1_\"\>
	cat /opt/karoshi/postfixdata/warning_messages/level1
	echo '</textarea>'
else
	echo '<textarea cols="80" rows="10" name="_LEVEL1_"></textarea>'
fi
#Level2
echo '<br><br><b>'$"Level"' 2</b><br>'
if [ -f /opt/karoshi/postfixdata/warning_messages/level2 ]
then
	echo '<textarea cols="80" rows="10" name="_LEVEL2_">'
	cat /opt/karoshi/postfixdata/warning_messages/level2
	echo '</textarea>'
else
	echo '<textarea cols="80" rows="10" name="_LEVEL2_"></textarea>'
fi
#Level3
echo '<br><br><b>'$"Level"' 3</b><br>'
if [ -f /opt/karoshi/postfixdata/warning_messages/level3 ]
then
	echo '<textarea cols="80" rows="10" name="_LEVEL3_">'
	cat /opt/karoshi/postfixdata/warning_messages/level3
	echo '</textarea>'
else
	echo '<textarea cols="80" rows="10" name="_LEVEL3_"></textarea>'
fi
#Level4
echo '<br><br><b>'$"Level"' 4</b><br>'
if [ -f /opt/karoshi/postfixdata/warning_messages/level4 ]
then
	echo '<textarea cols="80" rows="8" name="_LEVEL4_">'
	cat /opt/karoshi/postfixdata/warning_messages/level4
	echo '</textarea>'
else
	echo '<textarea cols="80" rows="8" name="_LEVEL4_"></textarea>'
fi
echo '<input value="Submit" class="button primary" type="submit"> <input value="Reset" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit
