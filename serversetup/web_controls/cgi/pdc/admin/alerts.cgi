#!/bin/bash
#Change password
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Alerts"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	1: { sorter: false}
    		}
		});
    }
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Show warning messages
if [ -f /opt/karoshi/web_controls/warnings/summary.txt ]
then
	echo '
	<table id="myTable" class="tablesorter" style="text-align: left;" >
		<thead>
			<tr>
				<th><b>'$"Description"'</b></th>
				<th></th>
			</tr>
		</thead>
		<tbody>'
	for WARNING_FILE in $(ls /opt/karoshi/web_controls/warnings/raw_messages/)
	do
		DATA=$(cat /opt/karoshi/web_controls/warnings/raw_messages/"$WARNING_FILE")
		LINK=$(echo "$DATA" | cut -d"," -f1)
		DESCRIPTION=$(echo "$DATA" | cut -d"," -f2)
		echo '
			<tr>
				<td>'"$DESCRIPTION"'</td>
				<td style="text-align:right">
					<form style="display: inline;" name="ClearMessages" action="'"$LINK"'" method="post">
						<button name="'"$LINK"'" value="_">
							'$"Check"'
						</button>
					</form>
				</td>
			</tr>'
	done
	echo '
		</tbody>
	</table>
	<form name="ClearMessages" action="clear_warnings_fm.cgi" method="post">
		<button name="ClearMessages" value="_">
			'$"Clear all"'
		</button>
	</form>
'
else
	echo $"There are no warning messages."
fi

echo '
</display-karoshicontent>
</body>
</html>'

