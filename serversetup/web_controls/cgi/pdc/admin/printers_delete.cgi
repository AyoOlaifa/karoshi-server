#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
#Get User Preferences

source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Delete a Network Printer"
TITLEHELP=$"Choose the printer you want to delete."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Delete_Network_Printer"
ICON1=/images/submenus/system/delete.png

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#########################
#Get data input
#########################

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/printers.cgi";'
echo '</script>'
echo "</body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Check that a print server has been declared
function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'$"A print server has not yet been set up."'")';
echo 'window.location = "karoshi_servers_view.cgi";'
echo '</script>'
echo "</body></html>"
exit
}

[ ! -f /opt/karoshi/server_network/printserver ] && show_status


#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


echo '<form action="/cgi-bin/admin/printers_delete2.cgi" method="post">
		<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
			'$"Show Printers"'
		</button>

		<button formaction="/cgi-bin/admin/printers_add_fm.cgi" class="button" name="_AddPrinters_" value="_">
			'$"Add Printer"'
		</button>

		<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
			'$"Locations"'
		</button><br><br>'

PRINTERLIST=( $(sudo -H /opt/karoshi/web_controls/exec/printers_show_queues) )
PRINTERCOUNT="${#PRINTERLIST[@]}"
if [ "$PRINTERCOUNT" = 0 ]
then
	echo '<ul><li>'$"There are no printer queues to delete"'</ul></li></body></html>'
	exit
fi
#Show printer list to choose from
echo '
	<table id="myTable" class="tablesorter" style="text-align: left;" ><thead>
	<tr>
		<th class="karoshi-input">'$"Printer"'</th>
		<th style="width: 80px;">'$"Delete"'</th>
	</tr>
	</thead><tbody>'
COUNTER=0
while [ "$COUNTER" -lt "$PRINTERCOUNT" ]
do
	#Get printer name
	PRINTERNAME="${PRINTERLIST[$COUNTER]}"
	echo '<tr>
		<td style="height: 35px;">'"$PRINTERNAME"'</td><td>
			<button class="info" name="____PRINTERNAME____" value="'"$PRINTERNAME"'">
			<img src="'"$ICON1"'" alt="'$"Delete"'">
			<span>'$"Delete"'<br>'"$PRINTERNAME"'</span>
			</button>
		</td>
	</tr>
	'
	let COUNTER="$COUNTER"+1
done
echo '
	</tbody>
</table>
<input class="button primary" value="Submit" type="submit">
</form>
</display-karoshicontent>
</body>
</html>'
exit
