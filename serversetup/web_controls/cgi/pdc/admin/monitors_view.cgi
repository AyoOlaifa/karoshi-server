#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View Monitors"
TITLEHELP=$"Edit or delete your monitoring groups."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#Viewing_Monitors"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/monitors_add_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must choose a monitor."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
		<form action="/cgi-bin/admin/monitors_add_fm.cgi" method="post">
			<button class="button" name="_AddMonitor_" value="_">
				'$"Add Monitor"'
			</button>

			<button formaction="/cgi-bin/admin/mon_status.cgi" class="button" name="_NetworkStatus_" value="_">
				'$"Status"'
			</button>'

if [ -f /opt/karoshi/server_network/mon/activate_changes ]
then
	echo '
			<button formaction="/cgi-bin/admin/monitors_activate_changes.cgi" class="button primary" name="_NetworkStatus_" value="_">
				'$"Activate Changes"'
			</button>'
fi

echo '
		</form>'

#Show monitors

if [ ! -d /opt/karoshi/server_network/mon/monitors ]
then
	MESSAGE=$"No monitors available."
	show_status
fi 

if [[ $(ls -1 /opt/karoshi/server_network/mon/monitors | wc -l) = 0 ]] && [[ $(ls -1 /opt/karoshi/server_network/mon/monitors_disabled | wc -l) = 0 ]]
then
	MESSAGE=$"No monitors available."
	show_status
fi

#Show table of sites
echo '<table id="myTable" class="tablesorter" style="text-align: left;" >
<thead>
<tr><th class="karoshi-input"><b>'$"Monitors"'</b></th><th class="karoshi-input"><b>'$"Status"'</b></th><th style="width: 70px;"><b>'$"Edit"'</b></th><th style="width: 70px;"><b>'$"Delete"'</b></th><th style="width: 70px;"><b>'$"Info"'</b></th></tr></thead><tbody>'

for MONITORNAME in /opt/karoshi/server_network/mon/monitors/*
do
	MONITORNAME=$(basename "$MONITORNAME")
	MONITORNAME2=$(echo "$MONITORNAME" | sed 's/_/%%%%%/g')
	MONITOR_SERVICES=$(grep service /opt/karoshi/server_network/mon/monitors/"$MONITORNAME" | sed 's/service//g' |sed 's/$/<br>/g')
	SERVICE_TCPIPS=$(sed -n 4,4p /opt/karoshi/server_network/mon/monitors/"$MONITORNAME" | cut -d' ' -f3-)

	echo '<tr>

	<td>'"$MONITORNAME"'</td>
	<td>
		<form style="margin: 0;" action="/cgi-bin/admin/monitors_enable_disable.cgi" name="monitors" method="post">
			<button class="button smallbutton" name="_Disable_" value="_MONITOR_'"$MONITORNAME2"'_">
			<span>'$"On"'</span>
			</button>
		</form>
	</td>
	<td>
		<form style="margin: 0;" action="/cgi-bin/admin/monitors_add_fm.cgi" name="monitors" method="post">
			<button class="info" name="_Edit_" value="_MONITOR_'"$MONITORNAME2"'_">
			<img src="/images/submenus/system/edit.png" alt="'$"Edit"' - '"$MONITORNAME"'">
			<span>'$"Edit"' - '"$MONITORNAME"'</span>
			</button>
		</form>
	</td>
	<td>
		<form style="margin: 0;" action="/cgi-bin/admin/monitors_delete.cgi" name="monitors" method="post">
			<button class="info" name="_Delete_" value="_MONITOR_'"$MONITORNAME2"'_">
			<img src="/images/submenus/system/delete.png" alt="'$"Delete"' - '"$MONITORNAME"'">
			<span>'$"Delete"' - '"$MONITORNAME"'</span>
			</button>
		</form>
	</td>
	<td>
		<a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"TCPIP numbers monitored":'<br>'"$SERVICE_TCPIPS"'<br>'$"Services Monitored":'<br>'"$MONITOR_SERVICES"'</span></a>
	</td></tr>'
done

if [ -d /opt/karoshi/server_network/mon/monitors_disabled ]
then
	if [[ $(ls -1 /opt/karoshi/server_network/mon/monitors_disabled | wc -l) -gt 0 ]]
	then
		for MONITORNAME in /opt/karoshi/server_network/mon/monitors_disabled/*
		do
			MONITORNAME=$(basename "$MONITORNAME")
			MONITORNAME2=$(echo "$MONITORNAME" | sed 's/_/%%%%%/g')
			MONITOR_SERVICES=$(grep service /opt/karoshi/server_network/mon/monitors_disabled/"$MONITORNAME" | sed 's/service//g' |sed 's/$/<br>/g')
			SERVICE_TCPIPS=$(sed -n 4,4p /opt/karoshi/server_network/mon/monitors_disabled/"$MONITORNAME" | cut -d' ' -f3-)

			echo '<tr>

			<td>'"$MONITORNAME"'</td>
			<td>
				<form action="/cgi-bin/admin/monitors_enable_disable.cgi" name="monitors" method="post">
					<button class="button smallbutton" name="_Enable_" value="_MONITOR_'"$MONITORNAME2"'_">
					<span>'$"Off"'</span>
					</button>
				</form>
			</td>
			<td></td>
			<td>
				<form action="/cgi-bin/admin/monitors_delete.cgi" name="monitors" method="post">
					<button class="info" name="_Delete_" value="_MONITOR_'"$MONITORNAME2"'_">
					<img src="/images/submenus/system/delete.png" alt="'$"Delete"' - '"$MONITORNAME"'">
					<span>'$"Delete"' - '"$MONITORNAME"'</span>
					</button>
				</form>
			</td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"TCPIP numbers monitored":'<br>'"$SERVICE_TCPIPS"'<br>'$"Services Monitored":'<br>'"$MONITOR_SERVICES"'</span></a></td></tr>'
		done
	fi
fi

echo '</tbody></table></display-karoshicontent></body></html>'
exit
