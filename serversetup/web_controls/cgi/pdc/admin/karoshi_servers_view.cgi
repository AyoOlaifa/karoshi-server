#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

source /opt/karoshi/server_network/domain_information/domain_name

TITLE=$"View Servers"
TITLEHELP=$"This allows you to schedule updates for your servers."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate head section
source /opt/karoshi/web_controls/generate_head_section_admin

TITLE=''$"My Servers"' '"$SHORTNAME"''

#Generate page top
source /opt/karoshi/web_controls/generate_page_top_admin

#Page content
echo '
<div id="karoshicontent">
</div>'

#Generate side bar
source /opt/karoshi/web_controls/generate_sidebar_admin

#Add in scripts
/opt/karoshi/web_controls/generate_scripts

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	2: { sorter: "ipAddress" }
    		}
		});
    } 
);
</script>
'

############################
#Show page
############################
function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/karoshi_servers_add_fm.cgi";'
echo '</script>'
echo "</div></body></html>"
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/karoshi_servers_view.cgi | cut -d' ' -f1)

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE" | sudo -H /opt/karoshi/web_controls/exec/karoshi_servers_view
STATUS="$?"

echo '</display-karoshicontent>'

if [ "$?" = 102 ]
then
	MESSAGE=$"No Karoshi Servers have been set up with ssh."
	show_status
fi

echo '</body></html>'
exit
