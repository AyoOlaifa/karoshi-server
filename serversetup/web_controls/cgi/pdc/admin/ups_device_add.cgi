#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add a device"
TITLEHELP=$"This will add a device to the ups list to show that it is phsyically plugged into the ups. It will not configure the device."'<br><br>'$"This could be a network switch or a non Karoshi server."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=9
#Assign UPSSERVER
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = UPSSERVERcheck ]
	then
		let COUNTER=$COUNTER+1
		UPSSERVER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign SERVER
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = SERVERcheck ]
	then
		let COUNTER=$COUNTER+1
		SERVER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign DEVICENAME
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = DEVICENAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		DEVICENAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/ups_status.cgi";
</script>
<display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that UPSSERVER is not blank
if [ -z "$UPSSERVER" ]
then
	MESSAGE=$"The UPS driver cannot be blank."
	show_status
fi

#Check to see that DEVICENAME is not blank
if [ -z "$DEVICENAME" ]
then
	MESSAGE=$"The device name cannot be blank."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/ups_device_add.cgi | cut -d' ' -f1)
#Add UPS
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$UPSSERVER:$SERVER:$DEVICENAME:" | sudo -H /opt/karoshi/web_controls/exec/ups_device_add
EXEC_STATUS="$?"
MESSAGE="$SERVER - "$"The UPS device has been added."
if [ "$EXEC_STATUS" = 101 ]
then
	MESSAGE=$"There was a problem with this action."" "$"Please check the karoshi web administration logs for more details."
	show_status
fi

if [ "$EXEC_STATUS" = 105 ]
then
	MESSAGE=$"The UPS could not be contacted on that port."
	show_status
fi
echo '
	<script>
		window.location = "/cgi-bin/admin/ups_status.cgi";
	</script>
	<display-karoshicontent>
</body>
</html>
'
exit
