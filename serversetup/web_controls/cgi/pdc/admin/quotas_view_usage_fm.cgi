#!/bin/bash
#Copyright (C) 2008  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View Quota Usage"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/quotas_set_fm.cgi" name="selectservers" method="post">
	<button class="button"name="_QuotaSettings_" value="_">
		'$"Quota Settings"'
	</button>

	<button class="button" formaction="quotas_view_partitions.cgi" name="_EnabledPartitions_" value="_">
		'$"Enabled Partitions"'
	</button>
</form>
<form action="/cgi-bin/admin/quotas_view_usage.cgi" name="selectservers" method="post">
<table>
	<tbody>
		<tr>
 			<td class="karoshi-input">'$"Username"'</td>
			<td><input size="20" name="_USERNAME_" class="karoshi-input" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#View_Quota_Usage"><span class="icon-large-tooltip">'$"Choose the user that you want to check the quotas for. Leave this field blank if you want to check the quotas for a group."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Group"'</td>
			<td>'

#Show groups
/opt/karoshi/web_controls/group_dropdown_list

echo '
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#View_Quota_Usage"><span class="icon-large-tooltip">'$"Choose the group that you want to check the quotas on."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</body>
</html>
'
exit


<option value="allstudents">'$ALLSTUDENTSMSG'</option>
