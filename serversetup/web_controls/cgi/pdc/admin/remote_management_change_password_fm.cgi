#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Web Management Password"
TITLEHELP=$"This will change the password of the user for access to all servers on the Karoshi system."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<form action="/cgi-bin/admin/remote_management_change_password.cgi" method="post">
				<table>
					<tbody>
						<tr>
			 				<td class="karoshi-input">'$"Password"'</td>
							<td><input class="karoshi-input" required="required" tabindex="1" name="____PASSWORD1____" size="20" type="password"></td>
							<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User"><span class="icon-large-tooltip">'$"Enter in the new password that you want and confirm it on the line below."'</span></a></td>
						</tr>
						<tr>
							<td>'$"Confirm"'</td>
							<td><input class="karoshi-input" required="required" tabindex="2" name="____PASSWORD2____" size="20" type="password"></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
			</form>
		</display-karoshicontent>
	</body>
</html>
'
exit
