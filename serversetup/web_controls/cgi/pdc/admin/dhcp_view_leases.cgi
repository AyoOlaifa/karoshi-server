#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"DHCP Leases"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	0: { sorter: "ipAddress" },
	3: { sorter: "MAC" }
    		}
		});
    } 
);
</script>
'

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign OPTION
DATANAME=OPTION
get_data
OPTION="$DATAENTRY"

[ -z "$OPTION" ] && OPTION=active

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/dhcp_view_leases.cgi" method="post">


	<button class="button" formaction="dhcp_fm.cgi" name="_ConfigureDHCP_" value="_">
		'$"Configure"'
	</button>

	<button class="button" formaction="dhcp_reservations.cgi" name="_DoDHCPReservations_" value="_">
		'$"Reservations"'
	</button>

	<button class="button" formaction="dhcp_bans.cgi" name="_DHCPBans_" value="_">
		'$"Bans"'
	</button>

	<button class="button" name="_DHCPActiveLeases_" value="_OPTION_active_">
		'$"Active Leases"'
	</button>


'

#Only show backup leases button if we have a secondary dhcp server
if [ -d /opt/karoshi/server_network/dhcp_servers ]
then
	echo '
		<button class="button" name="_DHCPBackupLeases_" value="_OPTION_backup_">
			'$"Backup Leases"'
		</button>'
fi

echo '
	<button class="button" name="_DHCPFreeLeases_" value="_OPTION_free_">
		'$"Free Leases"'
	</button>

'
if [ -f /opt/karoshi/server_network/dhcp/restart_required ]
then
	echo '
		<button class="button" formaction="dhcp_bans.cgi" name="_DHCPRestart_" value="_ACTION_restartdhcp_">
			'$"Activate Changes"'
		</button>
'

fi

echo '</form>'

if [ "$OPTION" = restartdhcp ]
then
	echo "$REMOTE_USER:$REMOTE_ADDR:" | sudo -H /opt/karoshi/web_controls/exec/dhcp_restart
	OPTION=""
	#Reload page
	echo '<form id="reloadpage" name="reservervations" action="/cgi-bin/admin/dhcp_view_leases.cgi" method="post"><script>
	document.getElementById("reloadpage").submit();
	</script></form>'
fi

#Show lease information
/opt/karoshi/web_controls/leasecheck.pl "$OPTION"
echo '</display-karoshicontent></body></html>'
exit

