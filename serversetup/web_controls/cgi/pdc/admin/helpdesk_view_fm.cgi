#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Technical Support Requests"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

DATA=$(cat | tr -cd 'A-Za-z0-9\.%+_:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
#Assign SEARCHCRITERIA
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = SEARCHCRITERIAcheck ]
	then
		let COUNTER=$COUNTER+1
		SEARCHCRITERIA=`echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%3D/=/g'`
		break
	fi
	let COUNTER=$COUNTER+1
done

[ -z "$SEARCHCRITERIA" ] && SEARCHCRITERIA=ASSIGNED

#Reload the page every 3 minutes
echo '
<form id="refresh_form" action="/cgi-bin/admin/helpdesk_view_fm.cgi" method="post">
 <input type="hidden" name="_SEARCHCRITERIA_'$SEARCHCRITERIA'_" value="_SEARCHCRITERIA_'$SEARCHCRITERIA'_"> 
</form>
<script>
setTimeout(function(){
document.getElementById("refresh_form").submit();
}, 180000);
</script>
'

#Check to see if there are any new jobs
if [ ! -d /opt/karoshi/server_network/helpdesk/todo/ ]
then
	echo $"There are no new requests to view."'</display-karoshicontent></body></html>'
	exit
fi

if [[ $(ls -1 /opt/karoshi/server_network/helpdesk/todo/ | wc -l) = 0 ]]
then
	echo $"There are no new requests to view."'</display-karoshicontent></body></html>'
	exit
fi

if [ $MOBILE = yes ]
then
	echo '
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th style="width: 90px;"><b>'$"Date"'</b></th>
				<th style="width: 120px;"><b>'$"Location"'</b></th>
				<th style="width: 30px;"></th>
				<th style="width: 60px;"><b>'$"Action"'</b></th>
			</tr>
		</thead>
		<tbody>'
else
	echo '
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th style="width: 100px;"><b>'$"Date"'</b></th>
				<th style="width: 130px;"><b>'$"Name"'</b></th>
				<th style="width: 140px;"><b>'$"Request Summary"'</b></th>
				<th style="width: 120px;"><b>'$"Location"'</b></th>
				<th style="width: 90px;"><b>'$"Wait Time"'</b></th>
				<th style="width: 90px;"><b>'$"Priority"'</b></th><th style="width: 100px;"><b>'$"Assigned to"'</b></th>
				<th style="width: 30px;"></th><th style="width: 60px;"><b>'$"Action"'</b></th>
			</tr>
		</thead>
		<tbody>'
fi

for NEWJOB in $(grep -w -l "$SEARCHCRITERIA" /opt/karoshi/server_network/helpdesk/todo/*)
do
	NEWJOB=`basename $NEWJOB`
	DATE=`echo $NEWJOB | cut -d"." -f1`
	TIME=`date +%H:%M -d @$DATE`
	DATE=`date +%d-%m-%y -d @$DATE`
	source /opt/karoshi/server_network/helpdesk/todo/$NEWJOB
	NOW=`date +%s`
	let WAITTIME=($NOW-$DATE2)
	if [ $WAITTIME -lt 60 ]
	then
		#Show time in seconds
		if [ $WAITTIME = 1 ]
		then 
			WAITTIME=`echo $WAITTIME $"second"`
		else
			WAITTIME=`echo $WAITTIME $"seconds"`
		fi
	else
		#Convert to minutes
		let WAITTIME=$WAITTIME/60
		if [ $WAITTIME -lt 60 ]
		then
			#Show time in minutes
			if [ $WAITTIME = 1 ]
			then
				WAITTIME=`echo $WAITTIME $"minute"`
			else
				WAITTIME=`echo $WAITTIME $"minutes"`
			fi
		else
		#Convert time to hours
		let WAITTIME=$WAITTIME/60
			if [ $WAITTIME -lt 24 ]
			then
				#Show time in hours
				if [ $WAITTIME = 1 ]
				then
					WAITTIME=`echo $WAITTIME $"hour"`
				else
					WAITTIME=`echo $WAITTIME $"hours"`
				fi
			else
				#Covert time to days
				let WAITTIME=$WAITTIME/24
				if [ $WAITTIME = 1 ]
				then
					WAITTIME=`echo $WAITTIME $"day"`
				else
					WAITTIME=`echo $WAITTIME $"days"`
				fi
			fi
		fi
	fi

	ASSIGNED2=$ASSIGNED
	[ -z "$ASSIGNED2" ] && ASSIGNED2=$"Not Assigned"


	if [ "$SEARCHCRITERIA" != ASSIGNED ]
	then
		ASSIGNED=ASSIGNED
	fi

	if [ $MOBILE = yes ]
	then
		echo '
			<tr>
				<td style="vertical-align: top;">'$DATE'<br>'$TIME'</td>
				<td style="vertical-align: top;">'$LOCATION'</td>
				<td>
					<form style="display: inline;" action="/cgi-bin/admin/helpdesk_view_fm.cgi" method="post">
						<button class="info" name="_AssignedJob_" value="_SEARCHCRITERIA_'$ASSIGNED'_">
							<img src="/images/submenus/user/helpdesk/staff.png" alt="'$ASSIGNED2'">
							<span>'$ASSIGNED2'</span>
						</button>
					</form>
				</td>
				<td>
					<form style="display: inline;" action="/cgi-bin/admin/helpdesk_action_fm.cgi" method="post">
						<button class="info" name="_ViewUserAssignedJobs_" value="_JOBNAME_'$NEWJOB'_">
							<img src="/images/submenus/user/helpdesk/action.png" alt="'$JOBTITLE'">
							<span><b>'$"Name"'</b><br>'$NAME'<br><br><b>'$"Priority"'</b><br>'$PRIORITY'<br><br><b>'$"Location"'</b><br>'$LOCATION'<br><br><b>'$"Request Summary"'</b><br>'$JOBTITLE'</span>
						</button>
					</form>
				</td>
			</tr>'
	else
		echo '
			<tr>
				<td style="vertical-align: top;">'$DATE' '$TIME'</td>
				<td style="vertical-align: top;">'$NAME'</td>
				<td style="vertical-align: top;">'$JOBTITLE'</td>
				<td style="vertical-align: top;">'$LOCATION'</td>
				<td style="vertical-align: top;">'$WAITTIME'</td>
				<td style="vertical-align: top;">'$PRIORITY'</td>
				<td style="vertical-align: top;">'$ASSIGNED2'</td>
				<td>
					<form style="display: inline;" action="/cgi-bin/admin/helpdesk_view_fm.cgi" method="post">
						<button class="info" name="_AssignedJob_" value="_SEARCHCRITERIA_'$ASSIGNED'_">
							<img src="/images/submenus/user/helpdesk/staff.png" alt="'$ASSIGNED2'">
							<span>'$ASSIGNED2'</span>
						</button>
					</form>
				</td>
				<td>
					<form style="display: inline;" action="/cgi-bin/admin/helpdesk_action_fm.cgi" method="post">
						<button class="info" name="_ViewUserAssignedJobs_" value="_JOBNAME_'$NEWJOB'_">
							<img src="/images/submenus/user/helpdesk/action.png" alt="'$JOBTITLE'">
							<span>'$JOBTITLE'</span>
						</button>
					</form>
				</td>
			</tr>'
	fi
done

echo '
		</tbody>
	</table>'

echo '
</display-karoshicontent>
</body>
</html>'
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:no:" | sudo -H /opt/karoshi/web_controls/exec/helpdesk_warning_message
exit


