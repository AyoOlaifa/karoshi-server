#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Quota Warning Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\%-')
#########################
#Assign data to variables
#########################
END_POINT=20
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ADMIN_EMAIL
DATANAME=ADMINEMAIL
get_data
ADMINEMAIL="$DATAENTRY"

#Assign THRESHOLD1
DATANAME=THRESHOLD1
get_data
THRESHOLD1=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign THRESHOLD2
DATANAME=THRESHOLD2
get_data
THRESHOLD2=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign THRESHOLD3
DATANAME=THRESHOLD3
get_data
THRESHOLD3=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign THRESHOLD4
DATANAME=THRESHOLD4
get_data
THRESHOLD4=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign INTERVAL1
DATANAME=INTERVAL1
get_data
INTERVAL1=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign INTERVAL2
DATANAME=INTERVAL2
get_data
INTERVAL2=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign INTERVAL3
DATANAME=INTERVAL3
get_data
INTERVAL3=$(echo "$DATAENTRY" | tr -cd '0-9\n')

#Assign INTERVAL4
DATANAME=INTERVAL4
get_data
INTERVAL4=$(echo "$DATAENTRY" | tr -cd '0-9\n')

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_quota_settings.cgi"
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that ADMINEMAIL is not blank
if [ -z "$ADMINEMAIL" ]
then
	MESSAGE=$"The administrator account must not be blank."
	show_status
fi
#Check to see that THRESHOLD1 is not blank
if [ -z "$THRESHOLD1" ]
then
	MESSAGE=''$"Level"' 1 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that THRESHOLD2 is not blank
if [ -z "$THRESHOLD2" ]
then
	MESSAGE=''$"Level"' 2 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that THRESHOLD3 is not blank
if [ -z "$THRESHOLD3" ]
then
	MESSAGE=''$"Level"' 3 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that THRESHOLD4 is not blank
if [ -z "$THRESHOLD4" ]
then
	MESSAGE=''$"Level"' 4 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that INTERVAL1 is not blank
if [ -z "$INTERVAL1" ]
then
	MESSAGE=''$"Interval"' 1 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that INTERVAL2 is not blank
if [ -z "$INTERVAL2" ]
then
	MESSAGE=''$"Interval"' 1 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that INTERVAL3 is not blank
if [ -z "$INTERVAL3" ]
then
	MESSAGE=''$"Interval"' 1 - '$"Incorrect data entry."''
	show_status
fi
#Check to see that INTERVAL4 is not blank
if [ -z "$INTERVAL4" ]
then
	MESSAGE=''$"Interval"' 1 - '$"Incorrect data entry."''
	show_status
fi

#Check to see that the thresholds are in the correct order
if [ "$THRESHOLD1" -gt "$THRESHOLD2" ] || [ "$THRESHOLD2" -gt "$THRESHOLD3" ] || [ "$THRESHOLD3" -gt "$THRESHOLD4" ]
then
	MESSAGE=$"The Levels must be in the correct order."
	show_status
fi

#Check that the intervals are in the correct order
if [ "$INTERVAL1" -lt "$INTERVAL2" ] || [ "$INTERVAL2" -lt "$INTERVAL3" ] || [ "$INTERVAL3" -lt "$INTERVAL4" ]
then
	MESSAGE=$"The warning intervals must be in the correct order."
	show_status
fi


Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_quota_settings2.cgi | cut -d' ' -f1)
#Create config file
sudo -H /opt/karoshi/web_controls/exec/email_quota_settings_apply "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ADMINEMAIL:$THRESHOLD1:$THRESHOLD2:$THRESHOLD3:$THRESHOLD4:$INTERVAL1:$INTERVAL2:$INTERVAL3:$INTERVAL4"
MESSAGE=$"E-mail quota warning settings have been applied."
show_status
exit
