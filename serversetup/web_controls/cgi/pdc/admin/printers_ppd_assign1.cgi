#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Assign PPD File"
TITLEHELP=$"Choose the PPD that you want to use for the printer."" "$"You can upload a PPD file by choosing this option from the printer PPD dropdown."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/12345UNDERSCORE12345/g' | sed 's/QUADUNDERSCORE/_/g')

#########################
#Assign data to variables
#########################
END_POINT=3
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign PRINTERNAME
DATANAME=PRINTERNAME
get_data
PRINTERNAME="${DATAENTRY//12345UNDERSCORE12345/_}"

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/printers.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

#########################
#Check data
#########################
#Check to see that PRINTERNAME is not blank
if [ -z "$PRINTERNAME" ]
then
	MESSAGE=$"The printer name cannot be blank."
	show_status
fi

#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
<form action="/cgi-bin/admin/printers_ppd_assign2.cgi" method="post">
	<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
		'$"Show Printers"'
	</button>

	<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinters_" value="_">
		'$"Delete Printer"'
	</button>

	<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
		'$"Locations"'
	</button>
</form>
<form action="/cgi-bin/admin/printers_ppd_assign2.cgi" method="post">
<input type="hidden" name="____PRINTERNAME____" value="'"$PRINTERNAME"'">
<table><tbody><tr><td class="karoshi-input">'$"Printer"'</td><td>'"$PRINTERNAME"'</td><td></td></tr>'

#######################
#Guess default paper size
#######################
A4SELECTED='selected="selected"'
LETTERSELECTED=""

#Show list of page sizes
echo '
<tr><td>'$"Default Page Size"'</td><td>
<select class="karoshi-input" name="____PAGESIZE____">
<option value="A2">A2</option>
<option value="A3">A3</option>
<option value="Letter" '"$LETTERSELECTED"'>Letter</option>
<option value="A4" '"$A4SELECTED"'>A4</option>
<option value="A5" >A5</option>
<option value="A6">A6</option>
</select></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the default page size for this printer."'</span></a></td></tr>
<tr><td>'$"Print in Colour?"'</td><td>
<select class="karoshi-input" name="____COLOUR____">
<option value="yes">'$"yes"'</option>
<option value="no">'$"No"'</option>
</select></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This setting will only affect printers that can print in colour."'</span></a></td></tr>'

#Show printer manufacturer list to choose from
echo '<tr><td>'$"Printer Make"'</td><td><select class="karoshi-input" name="____PRINTERPPD____">'
echo '<option value="" label="blank"></option>'
echo '<option value="uploadppd">'$"Upload PPD File"'</option>'
#Get list of printer drivers
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/printers_ppd_assign1.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/printers_show_drivers "$REMOTE_USER:$REMOTE_ADDR:$Checksum:"

echo '</select></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the make of your printer from this list. If the printer make is not listed you will need to get a PPD from the internet and use the Upload PPD option."'</span></a></td></tr></tbody></table><input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">'

echo '</form></display-karoshicontent></body></html>'
exit

