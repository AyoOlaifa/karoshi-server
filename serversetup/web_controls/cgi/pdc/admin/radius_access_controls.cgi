#!/bin/bash
#Copyright (C) 2017  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Radius Access Controls"
TITLEHELP=$"Radius Access Controls"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Radius_Server#Viewing_Access_Controls"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	2: { sorter: false}
    		}
		});
    }
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign GROUP
DATANAME=GROUP
get_data
GROUP="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/radius_access_controls.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Generate navigation bar
if [ "$MOBILE" = no ]
then
	ICON1=/images/submenus/user/group_yes.png
	ICON2=/images/submenus/user/group_no.png
else
	ICON1=/images/submenus/user/group_yes_m.png
	ICON2=/images/submenus/user/group_no_m.png

fi

#Check data
[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = allow ] || [ "$ACTION" = allowall ] || [ "$ACTION" = deny ] || [ "$ACTION" = denyall ] || [ "$ACTION" = activatechanges ] && [ ! -z "$GROUP" ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/radius_access_controls.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$ACTION:$GROUP:" | sudo -H /opt/karoshi/web_controls/exec/radius_access_controls	
fi

echo '

<form action="/cgi-bin/admin/radius_access_points.cgi" method="post">
	<button class="button" name="____RadiusAccessPoints____" value="____">
		'$"Access Points"'
	</button>

	<button formaction="/cgi-bin/admin/radius_access_controls.cgi" class="button" name="____RadiusAccessPoints____" value="____ACTION____allowall____GROUP____all____">
		'$"Allow all"'
	</button>

	<button formaction="/cgi-bin/admin/radius_access_controls.cgi" class="button" name="____RadiusAccessPoints____" value="____ACTION____denyall____GROUP____all____">
		'$"Deny all"'
	</button>'

	if [ -f /opt/karoshi/server_network/radius/activate_changes ] 
	then
		echo '

	<button formaction="/cgi-bin/admin/radius_access_controls.cgi" class="button" name="____ActivateChanges____" value="____ACTION____activatechanges____GROUP____all____">
		'$"Activate changes"'
	</button>'
	fi

echo '
</form>
<form action="/cgi-bin/admin/radius_access_controls.cgi" method="post">
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th><b>'$"Group"'</b></th>
				<th><b>'$"Access"'</b></th>
				<th></th>
			</tr>
		</thead>
		<tbody>'

for PRI_GROUP in $(ls -1 /opt/karoshi/server_network/group_information)
do
	ACTION="deny"
	COLOUR="#6EDE60"
	STATUSICON="$ICON1"
	STATUSMSG=$"Allowed"
	LONGMSG=$"Deny access for this group of users."
	
	if [ -f /opt/karoshi/server_network/radius/denied_groups/"$PRI_GROUP" ]
	then
		ACTION="allow"
		COLOUR="#FF0004"
		STATUSICON="$ICON2"
		STATUSMSG=$"Denied"
		LONGMSG=$"Allow access for this group of users."
	fi
	
	echo '
			<tr>
				<td>'"$PRI_GROUP"'</td>
				<td style="color:'"$COLOUR"'"><b>'"$STATUSMSG"'</b></td>
				<td>
					<button class="info infonavbutton" name="____DoAction____" value="____ACTION____'"$ACTION"'____GROUP____'"$PRI_GROUP"'____">
						<img src="'"$STATUSICON"'" alt="'"$STATUSMSG"'">
						<span>'"$LONGMSG"'</span>
					</button>
				</td>
			</tr>'
done
echo '
		</tbody>
	</table>
</form>
</display-karoshicontent>
</body>
</html>'
exit
