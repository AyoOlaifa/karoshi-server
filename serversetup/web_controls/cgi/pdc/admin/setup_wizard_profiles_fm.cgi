#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Profiles"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<br>
<b>'$"Windows Clients"'</b>
<br>
<br>'$"By default windows clients use mandatory profiles."'
<br>'$"New profiles are created using the profileuser account."'
<br>
<br>'$"The profileuser account uses a roaming profile that you can customise. "'
<br>'$"Copy the profileuser profile by right clicking on My Computer and using the profile management tools."'
<br>'$"It is essential that the permissions are changed on the profile to allow all users to use it."'
<br>
<br>'$"The copied profile can be zipped up and uploaded to the web management."'
<br>
<br>
<a href="windows_client_profile_upload_fm.cgi"><img src="/images/submenus/client/upload_skel.png" border="0" alt="">'$"Upload Windows Profile"'</a>
<br>
<br>
<br>
<b>'$"Karoshi Linux Clients"'</b>
<br>
<br>'$"The Karoshi Linux clients download an updated skel from the main server each time that the client is booted. "'
<br>
'$"Users logging onto the client then use the skel from the local machines."'
<br>
<br>
<a href="linux_client_download_skel.cgi"><img src="/images/submenus/client/upload_skel.png" border="0" alt="" />'$"Download Karoshi Linux Profile"'</a>
<br>
<br>
<a href="linux_client_upload_skel_fm.cgi"><img src="/images/submenus/client/download_skel.png" border="0" alt="" />'$"Upload Karoshi Linux Profile"'</a>
</display-karoshicontent>
</body>
</html>'
exit
