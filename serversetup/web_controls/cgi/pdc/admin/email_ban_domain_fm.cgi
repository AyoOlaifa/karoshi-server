#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Allow"' - '$"Ban E-Mail Domain"
TITLEHELP=$"Enter in the E-mail domain that you want to allow or ban."' '$"Banning will stop emails from being delivered from the domain."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Banned_E-Mail_Domains"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
<form action="/cgi-bin/admin/email_view_banned_domains_fm.cgi" method="post">
	<button class="button" formaction="email_view_banned_domains_fm.cgi" name="_ViewBannedDomains_" value="_">
		'$"View Domains"'
	</button>
	</td>
</form>
<form name="myform" action="/cgi-bin/admin/email_ban_domain.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"E-Mail Domain"'</td>
				<td class="karoshi-input"><input class="karoshi-input" tabindex= "1" name="_EMAILDOMAIN_" size="20" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the E-mail domain that you want to allow or ban."'<br><br>'$"Banning will stop emails from being delivered from the domain."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Allow"'</td>
				<td><input id="allowURL" type="radio" name="_DROPTYPE_" value="OK" checked><label for="allowURL"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This option will remove dns and spam blocklist checks from the domain."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Reject"'</td>
				<td><input id="rejectURL" type="radio" name="_DROPTYPE_" value="REJECT" checked><label for="rejectURL"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This option will send a rejected message back to the sender."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Discard"'</td>
				<td><input id="discardURL" type="radio" name="_DROPTYPE_" value="DISCARD"><label for="discardURL"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This option will silently drop messages without delivering them or notifying the sender."'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>'
exit

