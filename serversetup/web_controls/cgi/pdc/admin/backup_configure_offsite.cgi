#!/bin/bash
#Copyright (C) 20016 Paul Sharrad

#This file is part of Karoshi SERVERNAME.
#
#Karoshi SERVERNAME is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi SERVERNAME is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi SERVERNAME.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Off-Site Backup"
TITLEHELP=$"Choose the server that you want to configure the offsite backup for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Off-Site_Backup"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/backup_configure_offsite_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%+' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/12345UNDERSCORE12345/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=26
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

if [ ! -z "$SERVERNAME" ]
then
	ShortServerName=$(echo "$SERVERNAME" | cut -d. -f1)
	TITLE="$TITLE - $ShortServerName"
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" != view ]
then
	#Assign BACKUPDESTINATION
	DATANAME=BACKUPDESTINATION
	get_data
	BACKUPDESTINATION=${DATAENTRY//12345UNDERSCORE12345/_}	
fi

if [ "$ACTION" = viewarchives ] || [ "$ACTION" = viewarchivefiles ] || [ "$ACTION" = restorefiles ] || [ "$ACTION" = reallyrestorefiles ]
then
	#Assign ARCHIVENAME
	DATANAME=ARCHIVENAME
	get_data
	ARCHIVENAME=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign FOLDERPATH
	DATANAME=FOLDERPATH
	get_data
	FOLDERPATH=${DATAENTRY//12345UNDERSCORE12345/_}
fi

if [ "$ACTION" = reallyviewlogs ] 
then
	#Assign BACKUPLOGDATE
	DATANAME=BACKUPLOGDATE
	get_data
	BACKUPLOGDATE=${DATAENTRY//12345UNDERSCORE12345/_}
fi

if [ "$ACTION" = editbackupfolder ] || [ "$ACTION" = deletebackupfolder ] || [ "$ACTION" = reallydeletebackupfolder ]
then
	#Assign BACKUPFOLDER
	DATANAME=BACKUPFOLDER
	get_data
	BACKUPFOLDER=${DATAENTRY//12345UNDERSCORE12345/_}
fi


if [ "$ACTION" = reallyaddbackupfolder ]
then
	#Assign ARCHIVENAME
	DATANAME=ARCHIVENAME
	get_data
	ARCHIVENAME=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign BACKUPFOLDER
	DATANAME=BACKUPFOLDER
	get_data
	BACKUPFOLDER=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign DURATION
	DATANAME=DURATION
	get_data
	DURATION=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign FULLBACKUP
	DATANAME=FULLBACKUP
	get_data
	FULLBACKUP=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign HOURLYARCHIVES
	DATANAME=HOURLYARCHIVES
	get_data
	HOURLYARCHIVES=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign DAILYARCHIVES
	DATANAME=DAILYARCHIVES
	get_data
	DAILYARCHIVES=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign WEEKLYARCHIVES
	DATANAME=WEEKLYARCHIVES
	get_data
	WEEKLYARCHIVES=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign MONTHLYARCHIVES
	DATANAME=MONTHLYARCHIVES
	get_data
	MONTHLYARCHIVES=${DATAENTRY//12345UNDERSCORE12345/_}
fi 

if [ "$ACTION" = reallyadd ]
then
	#Assign BACKUPUSERNAME
	DATANAME=BACKUPUSERNAME
	get_data
	BACKUPUSERNAME=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign BACKUPSERVERNAME
	DATANAME=BACKUPSERVERNAME
	get_data
	BACKUPSERVERNAME=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign BACKUPPASSWORD
	DATANAME=BACKUPPASSWORD
	get_data
	BACKUPPASSWORD=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign BACKUPTYPE
	DATANAME=BACKUPTYPE
	get_data
	BACKUPTYPE=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign PASSPHRASE
	DATANAME=PASSPHRASE
	get_data
	PASSPHRASE=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign HOURS
	DATANAME=HOURS
	get_data
	HOURS=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign MINUTES
	DATANAME=MINUTES
	get_data
	MINUTES=${DATAENTRY//12345UNDERSCORE12345/_}

	#Assign STORAGEPATH
	DATANAME=STORAGEPATH
	get_data
	STORAGEPATH=${DATAENTRY//12345UNDERSCORE12345/_}


	if [ "$BACKUPTYPE" = local ]
	then
		#Assign LABEL
		DATANAME=LABEL
		get_data
		LABEL=${DATAENTRY//12345UNDERSCORE12345/_}
	fi

	#Assign ENCMODE
	DATANAME=ENCMODE
	get_data
	ENCMODE=${DATAENTRY//12345UNDERSCORE12345/_}
fi

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that a SERVERNAME has been chosen
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"You must choose a server."
	show_status
fi

if [ "$ACTION" = reallyaddbackupfolder ]
then
	if [ -z "$BACKUPFOLDER" ]
	then
		MESSAGE=$"You have not entered a backup folder."
		show_status
	fi

	if [ -z "$BACKUPDESTINATION" ]
	then
		MESSAGE=$"The backup destination cannot be blank."
		show_status
	fi
	if [ -z "$ARCHIVENAME" ]
	then
		MESSAGE=$"The archive name cannot be blank."
		show_status
	fi
fi

if [ "$ACTION" = viewarchives ] || [ "$ACTION" = viewarchivefiles ]
then
	if [ -z "$ARCHIVENAME" ]
	then
		MESSAGE=$"You have not entered an archive name."
		show_status
	fi

	if [ -z "$BACKUPDESTINATION" ]
	then
		MESSAGE=$"The backup destination cannot be blank."
		show_status
	fi	
fi

if [ "$ACTION" = reallyadd ]
then
	if [ -z "$BACKUPTYPE" ]
	then
		MESSAGE=$"You have not chosen a backup type."
		show_status
	fi

	if [ "$BACKUPTYPE" = ssh ]
	then
		if [ -z "$BACKUPSERVERNAME" ]
		then
			MESSAGE=$"You have not entered a backup server name."
			show_status
		fi

		if [ -z "$BACKUPUSERNAME" ]
		then
			MESSAGE=$"You have not entered a backup user name."
			show_status
		fi
	fi

	if [ -z "$PASSPHRASE" ]
	then
		MESSAGE=$"You have not entered an encryption key."
		show_status
	fi

	if [ -z "$STORAGEPATH" ]
	then
		MESSAGE=$"You have not entered a storage path."
		show_status
	fi
fi
if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyaddbackupfolder ]
then

	if [ -z "$HOURLYARCHIVES" ]
	then
		HOURLYARCHIVES=6
	fi

	if [ -z "$DAILYARCHIVES" ]
	then
		DAILYARCHIVES=7
	fi

	if [ -z "$WEEKLYARCHIVES" ]
	then
		WEEKLYARCHIVES=4
	fi

	if [ -z "$MONTHLYARCHIVES" ]
	then
		MONTHLYARCHIVES=6
	fi
fi

echo '
	<form action="/cgi-bin/admin/backup_configure_offsite_fm.cgi" method="post">
		<button class="button" name="SelectServer" value="_">
			'$"Select Server"'
		</button>
'

if [ "$ACTION" = view ] || [ "$ACTION" = reallyaddbackupfolder ] || [ "$ACTION" = reallydeletebackupfolder ] || [ "$ACTION" = assignbackupserver ] || [ "$ACTION" = setbackupstatus ]
then
	echo '
		<button formaction="/cgi-bin/admin/backup_configure_offsite.cgi" class="button" name="____AddOffsiteBackup____" value="____ACTION____add____SERVERNAME____'"$SERVERNAME"'____">
			'$"Add Offsite Backup"'
		</button>
		<button formaction="/cgi-bin/admin/backup_configure_offsite.cgi" class="button" name="____Restore____" value="____ACTION____restore____SERVERNAME____'"$SERVERNAME"'____">
			'$"Restore Files"'
		</button>
	'
fi

if [ "$ACTION" != view ]
then
	echo '
		<button formaction="/cgi-bin/admin/backup_configure_offsite.cgi" class="button" name="____ViewOffsiteBackup____" value="____ACTION____view________SERVERNAME____'"$SERVERNAME"'____">
			'$"View Offsite Backups"'
		</button>
	'
fi

echo '</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/backup_configure_offsite.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$SERVERNAME:$BACKUPSERVERNAME:$BACKUPUSERNAME:$BACKUPTYPE:$BACKUPPASSWORD:$PASSPHRASE:$HOURS:$MINUTES:$BACKUPDESTINATION:$BACKUPFOLDER:$STORAGEPATH:$FULLBACKUP:$HOURLYARCHIVES:$DAILYARCHIVES:$WEEKLYARCHIVES:$MONTHLYARCHIVES:$LABEL:$ENCMODE:$ARCHIVENAME:$BACKUPLOGDATE:$FOLDERPATH:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/backup_configure_offsite
echo "</display-karoshicontent></body></html>"
exit
