#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Web Management Global Language"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/remote_management_change_global_language2.cgi" method="post">

	<button formaction="/cgi-bin/admin/remote_management_change_language.cgi" class="button" name="_ChangeLanguage_" value="_">
		'$"Change Language"'
	</button>
	<br>
	<br>

	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Language"'</td>
				<td><select name="___LANGCHOICE___" class="karoshi-input"><option label="blank" ></option>'

#Get currently set global language.
[ -f /opt/karoshi/web_controls/global_prefs ] && source /opt/karoshi/web_controls/global_prefs
[ -z "$LANG" ] && LANG="en.UTF-8"

#Generate dropdown list of langauges
source /opt/karoshi/serversetup/variables/langlist
for LANGDATA in $LANGLIST
do
	LANGCODE=$(echo "$LANGDATA" | cut -d"," -f1)
	LANGTITLE=$(echo "$LANGDATA" | cut -d"," -f2)
	SELECTED=""
	if [ "$LANGCODE" = "$LANG" ]
	then
		SELECTED='selected="selected"'
	fi
echo '
					<option value="'"$LANGCODE"'" '$SELECTED'>'"$LANGTITLE"'</option>'
done



echo '
				</select></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the language that you want for the Web Management."'<br><br>'$"This will affect all general web management pages."'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>'
exit

