#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Roaming Profiles"
TITLEHELP=$"This shows if users have been set to use roaming or mandatory profiles. It allows you to change the type of profile used for each user or a group of users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

if [ "$MOBILE" = no ]
then
	ICON1=/images/submenus/user/users.png
	TABLECLASS=standard
	WIDTH=100
	WIDTH1=180
	WIDTH2=200
	HEIGHT1=24
else
	ICON1=/images/submenus/user/usersm.png
	TABLECLASS=mobilestandard
	WIDTH=90
	WIDTH1=120
	WIDTH2=160
	HEIGHT1=30
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
	<display-karoshicontent>
	<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+')
#########################
#Assign data to variables
#########################
END_POINT=17
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign GROUP
DATANAME=GROUP
get_data
GROUP="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign EXCEPTIONLIST
DATANAME=EXCEPTIONLIST
get_data
EXCEPTIONLIST="$DATAENTRY"

#Assign MODCODE
DATANAME=MODCODE
get_data
MODCODE="$DATAENTRY"

#Assign FORMCODE
DATANAME=FORMCODE
get_data
FORMCODE="$DATAENTRY"

if [ -z "$ACTION" ]
then
	ACTION=showmenu
fi

if [ "$ACTION" = status ]
then
	echo '
	<form name="ChooseGroup" action="/cgi-bin/admin/windows_client_roaming_profiles.cgi" method="post">
		<button class="button" name="_Menu_" value="_">
			'$"Show Menu"'
		</button>
	</form>
	'
fi

echo '<form name="profileform" action="/cgi-bin/admin/windows_client_roaming_profiles.cgi" method="post">'


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/windows_client_roaming_profiles.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################

if [ "$ACTION" = status ] || [ "$ACTION" = roaming ] || [ "$ACTION" = mandatory ]
then
	#Check to see that we have either a username or a group
	if [ -z "$USERNAME" ] && [ -z "$GROUP" ]
	then
		MESSAGE=$"You have not entered in a username or a group."
		show_status	
	fi

	if [ ! -z "$USERNAME" ]
	then
		#Check to see if the user exists
		getent passwd "$USERNAME" 1>/dev/null 
		if [ "$?" != 0 ]
		then
			MESSAGE=$"This user does not exist."
			show_status
		fi

		#Check that the username is not for a system account
		if [[ $(id -u "$USERNAME") -lt 500 ]]
		then
			MESSAGE=$"You cannot use a roaming profile with a system account."
			show_status
		fi

		#Check that the username is not the karoshi user
		if [ "$USERNAME" = karoshi ]
		then
			MESSAGE=$"You cannot use a roaming profile for the karoshi user."
			show_status
		fi
	fi
fi

#Check to see that username is not blank
if [ "$ACTION" = roaming ] || [ "$ACTION" = mandatory ]
then

	#Check to see that MODCODE is not blank
	if [ -z "$MODCODE" ]
	then
		MESSAGE=$"The modify code must not be blank."
		show_status
	fi
	#Check to see that FORMCODE is not blank
	if [ -z "$FORMCODE" ]
	then
		MESSAGE=$"The form code must not be blank."
		show_status
	fi

	#Check that modcode matches formcode
	if [ "$MODCODE" != "$FORMCODE" ]
	then
		MESSAGE=$"Incorrect modify code."
		show_status	
	fi
fi

if [ "$ACTION" = roaming ] || [ "$ACTION" = mandatory ] || [ "$ACTION" = status ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/windows_client_roaming_profiles.cgi | cut -d' ' -f1)
	#Enable roaming profile
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$USERNAME:$GROUP:$EXCEPTIONLIST:$ACTION:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/windows_client_roaming_profiles
fi

#Show menu options for the page
if [ "$ACTION" = showmenu ]
then
	MOD_CODE="${RANDOM:0:3}"
	echo '<input name="_FORMCODE_" value="'"$MOD_CODE"'" type="hidden">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Option"'</td><td>
					<select name="_ACTION_" class="karoshi-input">
						<option value="status">'$"View Status"'</option>
						<option value="mandatory">'$"Mandatory Profile"'</option>
						<option value="roaming">'$"Roaming Profile"'</option>
					</select></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"><span class="icon-large-tooltip">'$"Choose the profile action you want to carry out."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Primary Group"'</td>
				<td>'
	/opt/karoshi/web_controls/group_dropdown_list
	echo '
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"><span class="icon-large-tooltip">'$"All users in the group you select will be affected by the action you choose from this menu." $"Leave this blank if you want to enter in a username."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Username"'</td>
				<td><input tabindex= "1" name="_USERNAME_" class="karoshi-input" size="20" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"><span class="icon-large-tooltip">'$"Enter in a username or leave this blank if you want to choose an entire group."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Exceptions"'</td>
				<td><input tabindex= "1" name="_EXCEPTIONLIST_" class="karoshi-input" size="20" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"><span class="icon-large-tooltip">'$"Enter in any user accounts that you do not want to modify separated by spaces."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Modify Code"'</td>
				<td><b>'"$MOD_CODE"'</b></td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Confirm"'</td>
				<td><input name="_MODCODE_" maxlength="3" size="3" type="text" class="karoshi-input"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Roaming_Profiles"><span class="icon-large-tooltip">'$"Enter in the code displayed on the page to confirm this action."'</span></a></td>
			</tr>
		</tbody>
	</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">'


fi

echo '</form></display-karoshicontent></body></html>'

exit

