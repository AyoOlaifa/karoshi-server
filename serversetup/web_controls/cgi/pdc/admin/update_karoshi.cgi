#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Update Web Management"
TITLEHELP=$"This shows any Karoshi Server patches that are available."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Linux_Schools_Server_System"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/update_karoshi_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

function completed {
echo '
<script>
	window.location = "/cgi-bin/admin/update_karoshi_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################

#Assign _PATCHNAME_

END_POINT=19
#Assign PATCHNAME
COUNTER=2
while [ "$COUNTER" -le "$END_POINT" ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = PATCHNAME ]]
	then
		let COUNTER="$COUNTER"+1
		PATCHNAME=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER="$COUNTER"+1
done

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that patchname is not blank
if [ -z "$PATCHNAME" ]
then
	MESSAGE=$"You need to choose an update."
	show_status
fi

if [ "$PATCHNAME" = applyallpatches ]
then
	sudo -H /opt/karoshi/web_controls/exec/apply_all_karoshi_patches "$MOBILE":
else
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/update_karoshi.cgi | cut -d' ' -f1)
	sudo -H /opt/karoshi/web_controls/exec/update_karoshi "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$PATCHNAME:$MOBILE:"
fi
EXEC_STATUS="$?"

echo '</display-karoshicontent>'

if [ "$EXEC_STATUS" = 102 ]
then
	MESSAGE=''$"The updates have been completed."' '$"Please check the karoshi web administration logs for more details."''
	show_status
	exit
fi

if [ "$EXEC_STATUS" = 103 ]
then
	echo '<br><b>'$"Showing available updates completed."'</b><br>'
	show_status
	exit
fi
completed
echo '</display-karoshicontent></body></html>'
