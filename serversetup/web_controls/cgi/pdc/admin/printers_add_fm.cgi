#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add Network Printer"
TITLEHELP=$"Add a network printer for your client computers."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'$"A print server has not yet been set up."'")';
echo 'window.location = "karoshi_servers_view.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}

[ ! -f /opt/karoshi/server_network/printserver ] && show_status

echo '
			<form action="/cgi-bin/admin/printers.cgi" name="printers" method="post">
				<button class="button" name="_ShowPrinters_" value="_">
					'$"Show Printers"'
				</button>

				<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinters_" value="_">
					'$"Delete Printers"'
				</button>

				<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
					'$"Locations"'
				</button>
			</form>
'

echo '<form action="/cgi-bin/admin/printers_add.cgi" method="post">'
#Check that a print server has been assigned
if [ ! -f /opt/karoshi/server_network/printserver ]
then
	echo $"A print server has not yet been set up."
fi

echo '
				<table>
				    <tbody>
					<tr>
						<td class="karoshi-input">'$"Printer Name"'</td>
						<td><input tabindex= "1" name="____PRINTERNAME____" maxlength="15" class="karoshi-input" size="20" type="text"></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxgfx.co.uk/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"><span class="icon-large-tooltip">'$"Enter in the name that you want for this printer."'</span></a></td>
					</tr>
				 	<tr>
						<td>'$"Printer Address"'</td><td><input tabindex= "2" name="____PRINTERADDRESS____" maxlength="15" class="karoshi-input" size="20" type="text"></td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"><span class="icon-large-tooltip">'$"Enter in the tcpip number of the printer."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Queue - LPD only"'</td><td><input tabindex= "3" name="____PRINTERQUEUE____" class="karoshi-input" size="20" type="text"></td><td>
				<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"><span class="icon-large-tooltip">'$"Enter in the network queue."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Description"'</td><td><input tabindex= "4" name="____PRINTERDESC____" class="karoshi-input" size="20" type="text"></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"><span class="icon-large-tooltip">'$"Enter in a description of the printer hardware."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Location"'</td>
						<td>'

###############################
#Location
###############################
if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi
#Show current rooms
echo '
						<select tabindex= "5" name="____LOCATION____" class="karoshi-input">
							<option label="defaultlocation" value="'"$NO_LOCATION"'">'"$NO_LOCATION"'</option>'
COUNTER=1
while [ "$COUNTER" -lt "$LOCATION_COUNT" ]
do
	LOCATION=$(sed -n "$COUNTER,$COUNTER""p" /var/lib/samba/netlogon/locations.txt)
	echo '
							<option value="'"$LOCATION"'">'"$LOCATION"'</option>'
	let COUNTER="$COUNTER"+1
done
echo '
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"><span class="icon-large-tooltip">'$"Choose the location of this printer. Click on the add locations icon above to add locations."'</span></a></td>
					</tr>
			     		 <tr>
						<td>'$"Network Type"'</td>
						<td><select tabindex= "6" name="____PRINTERTYPE____" class="karoshi-input">
							<option>'$"Network Printer TCP"'</option>
							<option>'$"Network Printer IPP"'</option>
							<option>'$"Remote LPD queue"'</option>
							</select></td>
						<td></td>
					</tr>
					<tr>
						<td>'$"Port"'</td>
						<td>
							<select tabindex= "7" name="____PRINTERPORT____" class="karoshi-input">
							<option>9100</option>
							<option>9101</option>
							<option>9102</option>
							<option>631</option>
							</select>
						</td>
						<td></td>
			      		</tr>
			   	 </tbody>
			 	 </table>
				<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
			</form>
		</display-karoshicontent>
	</body>
</html>'
exit
