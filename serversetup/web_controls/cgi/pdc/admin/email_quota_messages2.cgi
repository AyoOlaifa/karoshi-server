#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Quota Warning Messages"
TITLEHELP=$"These are the messages that are sent to users who have reached a certain level on their mailbox."
HELPURL="#"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin
#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\+%')
#########################
#Assign data to variables
#########################
END_POINT=8
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign LEVEL1
DATANAME=LEVEL1
get_data
LEVEL1="$DATAENTRY"

#Assign LEVEL2
DATANAME=LEVEL2
get_data
LEVEL2="$DATAENTRY"

#Assign LEVEL3
DATANAME=LEVEL3
get_data
LEVEL3="$DATAENTRY"

#Assign LEVEL4
DATANAME=LEVEL4
get_data
LEVEL4="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "email_quota_messages.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that LEVEL1 to 4 are not blank
if [ -z "$LEVEL1" ] || [ -z "$LEVEL2" ] || [ -z "$LEVEL3" ] || [ -z "$LEVEL4" ] 
then
	MESSAGE=$"The level description is blank."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_quota_messages2.cgi | cut -d' ' -f1)
#Apply changes
sudo -H /opt/karoshi/web_controls/exec/email_quota_messages_apply "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$LEVEL1:$LEVEL2:$LEVEL3:$LEVEL4:"
MESSAGE=$"E-Mail Quota Warning messages have been saved."
show_status
