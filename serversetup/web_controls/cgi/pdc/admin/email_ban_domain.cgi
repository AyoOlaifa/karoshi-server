#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Allow"' - '$"Ban E-Mail Domain"
TITLEHELP=$"Enter in the E-mail domain that you want to allow or ban."' '$"Banning will stop emails from being delivered from the domain."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Banned_E-Mail_Domains"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=11
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign EMAILDOMAIN
DATANAME=EMAILDOMAIN
get_data
EMAILDOMAIN="$DATAENTRY"

#Assign DROPTYPE
DATANAME=DROPTYPE
get_data
DROPTYPE="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_ban_domain_fm.cgi";
</script>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that EMAILDOMAIN is not blank
if [ -z "$EMAILDOMAIN" ]
then
	MESSAGE=$"The action cannot be blank."
	show_status
fi

#Check that DROPTYPE has been set to the correct value
if [ "$DROPTYPE" != OK ] && [ "$DROPTYPE" != REJECT ] && [ "$DROPTYPE" != DISCARD ]
then
	MESSAGE=$"Incorrect droptype"
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_ban_domain.cgi | cut -d' ' -f1)
#Add user
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$EMAILDOMAIN:$DROPTYPE:" | sudo -H /opt/karoshi/web_controls/exec/email_ban_domain
echo '
<script>
	window.location = "/cgi-bin/admin/email_view_banned_domains_fm.cgi";
</script>
</body>
</html>
'
exit
