#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Default User Settings"
TITLEHELP=$"Sets the default settings for user accounts."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Default_User_Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=28
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}


#Assign LOCKOUTDURATION
DATANAME=LOCKOUTDURATION
get_data
LOCKOUTDURATION="$DATAENTRY"

#Assign LOCKOUTTHRESHOLD
DATANAME=LOCKOUTTHRESHOLD
get_data
LOCKOUTTHRESHOLD="$DATAENTRY"

#Assign LOCKOUTOBS
DATANAME=LOCKOUTOBS
get_data
LOCKOUTOBS="$DATAENTRY"

#Assign MAXIMUMPASSWORDAGE
DATANAME=MAXIMUMPASSWORDAGE
get_data
MAXIMUMPASSWORDAGE="$DATAENTRY"

#Assign USERNAMESTYLE
DATANAME=USERNAMESTYLE
get_data
USERNAMESTYLE="$DATAENTRY"

#Assign PASSWORDCOMPLEXITY
DATANAME=PASSWORDCOMPLEXITY
get_data
PASSWORDCOMPLEXITY="$DATAENTRY"

#Assign MINPASSWORDLENGTH
DATANAME=MINPASSWORDLENGTH
get_data
MINPASSWORDLENGTH="$DATAENTRY"

#Assign PASSWORDHISTORYLENGTH
DATANAME=PASSWORDHISTORYLENGTH
get_data
PASSWORDHISTORYLENGTH="$DATAENTRY"

#Assign CHANGEPASSFIRSTLOGIN
DATANAME=CHANGEPASSFIRSTLOGIN
get_data
CHANGEPASSFIRSTLOGIN="$DATAENTRY"

#Assign PASSWORDEXPIRY
DATANAME=PASSWORDEXPIRY
get_data
PASSWORDEXPIRY="$DATAENTRY"

#Assign HOMEDRIVE
DATANAME=HOMEDRIVE
get_data
HOMEDRIVE="$DATAENTRY"

#Make sure that only numbers are entered
LOCKOUTDURATION=$(echo "$LOCKOUTDURATION" | tr -cd '0-9\n')
LOCKOUTTHRESHOLD=$(echo "$LOCKOUTTHRESHOLD" | tr -cd '0-9\n')
LOCKOUTOBS=$(echo "$LOCKOUTOBS" | tr -cd '0-9\n')
MAXIMUMPASSWORDAGE=$(echo "$MAXIMUMPASSWORDAGE" | tr -cd '0-9\n')

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/default_user_settings_fm.cgi";
</script>
</body></html>'
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################

#Check to see that LOCKOUTDURATION is not blank
if [ -z "$LOCKOUTDURATION" ]
then
	MESSAGE=$"The lockout duration cannot be blank."
	show_status
fi
#Check to see that LOCKOUTTHRESHOLD is not blank
if [ -z "$LOCKOUTTHRESHOLD" ]
then
	MESSAGE=$"The lockout threshold cannot be blank."
	show_status
fi
#Check to see that LOCKOUTOBS is not blank
if [ -z "$LOCKOUTOBS" ]
then
	MESSAGE=$"The lockout observation period cannot be blank."
	show_status
fi

#Check to see that PASSWORDHISTORYLENGTH is not blank
if [ -z "$PASSWORDHISTORYLENGTH" ]
then
	PASSWORDHISTORYLENGTH=24
fi

#Check that PASSWORDHISTORYLENGTH is between 0 and 24
if [ "$PASSWORDHISTORYLENGTH" -gt 24 ] || [ "$PASSWORDHISTORYLENGTH" -lt 0 ]
then
	MESSAGE=$"The password history length must be between 0 and 24."
	show_status
fi

#Check to see that PASSWORDCOMPLEXITYis not blank
if [ -z "$PASSWORDCOMPLEXITY" ]
then
	PASSWORDCOMPLEXITY=off
fi

#Make sure that password complexity is either on or off
if [ "$PASSWORDCOMPLEXITY" != on ] && [ "$PASSWORDCOMPLEXITY" != off ]
then
	MESSAGE=$"The password complexity must be either on or off."
	show_status
fi

#Check to see that PASSWORDEXPIRY is not blank
if [ -z "$PASSWORDEXPIRY" ]
then
	PASSWORDEXPIRY=yes
fi

#Make sure that PASSWORDEXPIRY is either yes or no
if [ "$PASSWORDEXPIRY" != yes ] && [ "$PASSWORDEXPIRY" != no ]
then
	MESSAGE=$"The password expiry must be either yes or no."
	show_status
fi

#Check to see that CHANGEPASSFIRSTLOGIN is not blank
if [ -z "$CHANGEPASSFIRSTLOGIN" ]
then
	CHANGEPASSFIRSTLOGIN=no
fi

#Check to see that MINPASSWORDLENGTH is not blank
if [ -z "$MINPASSWORDLENGTH" ]
then
	MINPASSWORDLENGTH=5
fi

#Check that MINPASSWORDLENGTH is between 0 and 14
if [ "$MINPASSWORDLENGTH" -gt 14 ] || [ "$PASSWORDHISTORYLENGTH" -lt 0 ]
then
	MESSAGE=$"The minimum password length must be between 0 and 14."
	show_status
fi

#Check to see that MAXIMUMPASSWORDAGE is not blank
if [ -z "$MAXIMUMPASSWORDAGE" ]
then
	MAXIMUMPASSWORDAGE=999
fi

#Check to see that USERNAMESTYLE is not blank
if [ -z "$USERNAMESTYLE" ]
then
	MESSAGE=$"The username style cannot be blank."
	show_status
fi

#Make sure that HOMEDRIVE is not already in use
if [ -z "$HOMEDRIVE" ]
then
	MESSAGE=$"The homedrive cannot be blank."
	show_status
fi

if [[ $(grep -r 'DRIVELETTER=""'$HOMEDRIVE2'"' /opt/karoshi/server_network/network_shares | wc -l) -gt 0 ]]
then
	MESSAGE=''$"Home Drive"': '"$HOMEDRIVE"' - '$"This drive letter is already in use."''
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/default_user_settings.cgi | cut -d' ' -f1)
#Modify settings
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:SETDATA:$LOCKOUTDURATION:$LOCKOUTTHRESHOLD:$LOCKOUTOBS:$MAXIMUMPASSWORDAGE:$USERNAMESTYLE:$MINPASSWORDLENGTH:$PASSWORDCOMPLEXITY:$PASSWORDHISTORYLENGTH:$CHANGEPASSFIRSTLOGIN:$PASSWORDEXPIRY:$HOMEDRIVE:" | sudo -H /opt/karoshi/web_controls/exec/default_user_settings
if [ "$?" != 0 ]
then
	MESSAGE=''$"There was a problem with this action."' '$"Please check the karoshi web administration logs for more details."''
	show_status
fi

echo '<script>
window.location = "/cgi-bin/admin/default_user_settings_fm.cgi";
</script>'

exit
