#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi SERVERNAME.
#
#Karoshi SERVERNAME is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi SERVERNAME is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi SERVERNAME.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser

source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Configure Backup"
TITLEHELP=$"Choose the folders you want to backup."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Configure_Backup"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/backup_configure_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/12345UNDERSCORE12345/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=22
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

TITLE="$TITLE - $SERVERNAME"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = delete ] || [ "$ACTION" = edit ] || [ "$ACTION" = reallyedit ] || [ "$ACTION" = reallydelete ]
then
	#Assign BACKUPNAME
	DATANAME=BACKUPNAME
	get_data
	BACKUPNAME="${DATAENTRY//12345UNDERSCORE12345/_}"

	if [ "$ACTION" = reallyedit ]
	then
		#Assign BACKUPFOLDER
		DATANAME=BACKUPFOLDER
		get_data
		BACKUPFOLDER="${DATAENTRY//12345UNDERSCORE12345/_}"

		#Assign DURATION
		DATANAME=DURATION
		get_data
		DURATION=$(echo "$DATAENTRY" | tr -cd '0-9')
	fi
fi

if [ "$ACTION" = reallyschedule ]
then
	#Assign HOURS
	DATANAME=HOURS
	get_data
	HOURS=$(echo "$DATAENTRY" | tr -cd '0-9')


	#Assign MINUTES
	DATANAME=MINUTES
	get_data
	MINUTES=$(echo "$DATAENTRY" | tr -cd '0-9')
fi

if [ "$ACTION" = assignbackupserver ]
then
	#Assign BACKUPSERVER
	DATANAME=BACKUPSERVER
	get_data
	BACKUPSERVER="${DATAENTRY//12345UNDERSCORE12345/_}"
fi

if [ "$ACTION" = setbackupstatus ]
then
	#Assign BACKUPSTATUS
	DATANAME=BACKUPSTATUS
	get_data
	BACKUPSTATUS="${DATAENTRY//12345UNDERSCORE12345/_}"
fi

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that a SERVERNAME has been chosen
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"You must choose a server."
	show_status
fi

if [ "$ACTION" = delete ] || [ "$ACTION" = edit ] || [ "$ACTION" = reallyedit ] || [ "$ACTION" = reallydelete ]
then
	
	if [ -z "$BACKUPNAME" ]
	then
		MESSAGE=$"Blank backup name."
		show_status
	fi
	if [ "$ACTION" = reallyedit ]
	then
		if [ -z "$BACKUPFOLDER" ]
		then
			MESSAGE=$"Blank backup folder."
			show_status
		fi

		if [ -z "$DURATION" ]
		then
			MESSAGE=$"Blank duration."
			show_status
		fi
	fi
fi

if [ "$ACTION" = assignbackupserver ]
then
	if [ -z "$BACKUPSERVER" ]
	then
		MESSAGE=$"Blank backup servername."
		show_status
	fi
fi

if [ "$ACTION" = setbackupstatus ]
then
	if [ -z "$BACKUPSTATUS" ]
	then
		MESSAGE=$"Blank backup status."
		show_status
	fi
fi



echo '
<form action="/cgi-bin/admin/backup_configure_fm.cgi" method="post">
	<button class="button" name="SelectServer" value="_">
		'$"Select Server"'
	</button>
'

if [ "$ACTION" = edit ] || [ "$ACTION" = add ] || [ "$ACTION" = schedule ]
then
	echo '
	<button formaction="/cgi-bin/admin/backup_configure.cgi" class="button" name="____ViewBackups____" value="____SERVERNAME____'"$SERVERNAME"'____">
		'$"View Backups"'
	</button>'
fi
echo '</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/backup_configure.cgi | cut -d' ' -f1)
#View logs
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVERNAME:$ACTION:$BACKUPNAME:$BACKUPFOLDER:$DURATION:$BACKUPSERVER:$BACKUPSTATUS:$HOURS:$MINUTES:" | sudo -H /opt/karoshi/web_controls/exec/backup_configure
echo '
</display-karoshicontent>
</body>
</html>'
exit
