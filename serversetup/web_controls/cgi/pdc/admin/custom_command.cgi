#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

TITLE=$"Custom Command"
TITLEHELP=$"Please use this feature with care."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Custom_Commands"


#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%/+-')

#########################
#Assign data to variables
#########################
END_POINT=26
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign SERVERMASTER
DATANAME=SERVERMASTER
get_data
SERVERMASTER="$DATAENTRY"

#Assign CUSTOMCOMMAND
DATANAME=CUSTOMCOMMAND
get_data
CUSTOMCOMMAND="$DATAENTRY"

if [ ! -z "$SERVERNAME" ]
then
	ShortServerName=$(echo "$SERVERNAME" | cut -d. -f1)
	TITLE="$TITLE - $ShortServerName"
fi

#Generate page layout later than normal so that we have caputured the servername for display at the top.
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/custom_command_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ $REMOTE_USER'null' = null ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"You need to choose at least one server."
	show_status
fi

#Check to see that CUSTOMCOMMAND is not blank
if [ -z "$CUSTOMCOMMAND" ]
then
	MESSAGE=$"The custom command must not be blank."
	show_status
fi

#Check to see that SERVERTYPE is not blank
if [ -z "$SERVERTYPE" ]
then
	MESSAGE=$"The server type must not be blank."
	show_status
fi


Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/custom_command.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/custom_command "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$CUSTOMCOMMAND:$SERVERNAME:$SERVERTYPE:$SERVERMASTER:"
CUSTOM_COMMAND_STATUS="$?"

if [ "$CUSTOM_COMMAND_STATUS" = 102 ]
then
	MESSAGE=$"This custom command is not in the allowed custom command list."
	show_status
fi
echo '</display-karoshicontent></body></html>'
exit
