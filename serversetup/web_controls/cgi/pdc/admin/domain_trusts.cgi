#!/bin/bash
#Copyright (C) 2017  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Domain Trusts"
TITLEHELP=$"This allows you to add a domain trust so that users from other domains can log into this domain."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Domain_Trusts"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	0: { sorter: "ipAddress" },
	1: { sorter: "MAC" }
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\/*%+"-' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/UNDERSCORE/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=27
#Assign ACTION
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = ACTIONcheck ]
	then
		let COUNTER=$COUNTER+1
		ACTION=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = add ] || [ "$ACTION" = reallyadd ] || [ "$ACTION" = remove ] || [ "$ACTION" = reallyremove ] || [ "$ACTION" = edit ] || [ "$ACTION" = reallyedit ]
then
	#Assign DOMAIN
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = DOMAINcheck ]
		then
			let COUNTER=$COUNTER+1
			DOMAIN=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyremove ]
then
	#Assign USERNAME
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = USERNAMEcheck ]
		then
			let COUNTER=$COUNTER+1
			USERNAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done

	#Assign PASSWORD
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = PASSWORDcheck ]
		then
			let COUNTER=$COUNTER+1
			PASSWORD=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = reallyadd ]
then
	#Assign IPADDRESS1
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = IPADDRESS1check ]
		then
			let COUNTER=$COUNTER+1
			IPADDRESS1=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done

	#Assign IPADDRESS2
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = IPADDRESS2check ]
		then
			let COUNTER=$COUNTER+1
			IPADDRESS2=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi
function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/arp_control.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

if [ -z "$ACTION" ]
then
	ACTION=view
fi

#Check data

if [ "$ACTION" = reallyadd2 ]
then
	if [ -z "$DEVICE" ]
	then
		MESSAGE=$"No TCPIP information has been received."
		show_status
	fi

	#Check that tcpip number is correct
	#Check that we have 4 dots
	if [ `echo $DEVICE | sed 's/\./\n /g'  | sed /^$/d | wc -l` != 4 ]
	then
		MESSAGE=$"The TCPIP number is not corrrect."
		show_status
	fi
	#Check that no number is greater than 255
	HIGHESTNUMBER=`echo $DEVICE | sed 's/\./\n /g'  | sed /^$/d | sort -g -r | sed -n 1,1p`
	if [ $HIGHESTNUMBER -gt 255 ]
	then
		MESSAGE=$"The TCPIP number is not corrrect."
		show_status
	fi
fi


echo '<form action="/cgi-bin/admin/domain_trusts.cgi" method="post">'

if [ "$ACTION" = view ] || [ "$ACTION" = reallyadd ]
then
	echo '
	<button class="button" name="____ACTION____add____">
		'$"Add a domain trust"'
	</button>'
else
	echo '
	<button class="button" name="____ACTION____view____">
		'$"View domain trusts"'
	</button>'
fi
echo '
</form>
<form action="/cgi-bin/admin/domain_trusts.cgi" method="post">'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/domain_trusts.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$DOMAIN:$USERNAME:$PASSWORD:$IPADDRESS1:$IPADDRESS2:" | sudo -H /opt/karoshi/web_controls/exec/domain_trusts

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
