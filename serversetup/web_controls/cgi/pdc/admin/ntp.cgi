#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"NTP"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script>
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/update_servers_fm.cgi";
</script>
</body>
</html>'
exit
}

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Get current ntp servers from the main server.
NTPSERVERS=$(grep ^server /etc/ntp.conf | grep -v 127.127.1.0 | sed "s/^server//g" | sed "s/^ //g" | cut -d" " -f1 | sort)
NTPSERVER1=$(echo $NTPSERVERS | cut -d" " -f1)
NTPSERVER2=$(echo $NTPSERVERS | cut -d" " -f2)
NTPSERVER3=$(echo $NTPSERVERS | cut -d" " -f3)
NTPSERVER4=$(echo $NTPSERVERS | cut -d" " -f4)
echo '
<form action="/cgi-bin/admin/ntp2.cgi" name="selectservers" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"NTP Server"' 1</td>
				<td><input <input class="karoshi-input" tabindex= "1" name="_NTPSERVER1_" size="25" type="text" value="'"$NTPSERVER1"'"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxgfx.co.uk/karoshi/documentation/wiki/index.php?title=Configure_NTP"><span class="icon-large-tooltip">'$"Enter in the NTP server addresses that you want the servers to get the time from."'</span></a></td>
			</tr>
			<tr>
				<td>'$"NTP Server"' 2</td>
				<td><input class="karoshi-input" required="required" tabindex= "2" name="_NTPSERVER2_" size="25" type="text" value="'"$NTPSERVER2"'"></td>
				<td></td>
			</tr>
			<tr>
				<td>'$"NTP Server"' 3</td>
				<td><input class="karoshi-input" tabindex= "3" name="_NTPSERVER3_" size="25" type="text" value="'"$NTPSERVER3"'"></td>
				<td></td>
			</tr>
			<tr>
				<td>'$"NTP Server"' 4</td>
				<td><input class="karoshi-input" tabindex= "4" name="_NTPSERVER4_" size="25" type="text" value="'$NTPSERVER4'"></td>
				<td></td>
			</tr>
		</tbody>
	</table>'

#Show list of servers 
/opt/karoshi/web_controls/show_servers $MOBILE servers $"Set NTP Server" "notset" showtime

echo '
</display-karoshicontent>
</body>
</html>'
exit
