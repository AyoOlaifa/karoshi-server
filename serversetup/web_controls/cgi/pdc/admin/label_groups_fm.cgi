#!/bin/bash
#Copyright (C) 2008  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Label Groups"
TITLEHELP=$"This will let you add labels to your groups. The labels are shown to help you choose the correct group when adding new users to the system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Group_Management#Labelling_groups"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/label_groups.cgi" method="post">

	<button formaction="groups.cgi" name="_ViewNewPasswords_" value="_">
		'$"Group Management"'
	</button>
	<br><br>
'

#groups
COUNTER=1
echo '
<table id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th class="karoshi-input">'$"Group"'</th>
			<th class="karoshi-input">'$"Label"'</th>'
		
if [ "$MOBILE" = no ]
then
	echo '
			<th class="karoshi-input">'$"Group"'</th>
			<th class="karoshi-input">'$"Label"'</th>'
	MAXCOLS=2
else
	MAXCOLS=1
fi
echo '		
		</tr>
	</thead>
	<tbody>
		<tr>'
for GROUPNAMES in /opt/karoshi/server_network/group_information/*
do
	GROUPNAME=$(basename "$GROUPNAMES")
	unset LABEL
	source /opt/karoshi/server_network/group_information/"$GROUPNAME"
	echo '
			<td>'"$GROUPNAME"'</td><td><input class="karoshi-input" maxlength="20" size="20" name="____'"$GROUPNAME"':" value="'"$LABEL"'" type="text"></td>'

	if [ $COUNTER = $MAXCOLS ]
	then
		echo "
		</tr><tr>"
		COUNTER=0
	fi
	let COUNTER="$COUNTER"+1
done


echo '
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</display-karoshicontent>
</body>
</html>
'
exit

