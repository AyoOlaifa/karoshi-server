#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Desktop Icons"
TITLEHELP=$"Choose the profiles that you want to upload the desktop icons to."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_Desktop_Icons"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script>
<!--
function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}
// -->
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/windows_client_icon_select.cgi" name="selectgroups" method="post">'


#Check to see if any files have been uploaded
FILECOUNT=0
if [ -d /var/www/karoshi/win_icon_upload/ ]
then
	FILECOUNT=$(ls -1 /var/www/karoshi/win_icon_upload/ | wc -l)
fi

if [ "$FILECOUNT" -gt 8 ]
then
	echo ''$"An incorrect number of files have been uploaded."'</display-karoshicontent></body></html>'
	exit
fi
if [ "$FILECOUNT" -lt 1 ]
then
	echo ''$"You have not uploaded a file."'</div></div></body></html>'
	exit
fi

#Show list of profiles to choose from

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Uploaded file"'</td><td>'$FILENAME'</td></tr>
		<tr>
			<td>'$"Windows Version"'</td>
			<td>
				<select name="___WINDOWSVER___" class="karoshi-input">'
for WINDOWSVERSION in $(ls -1 /opt/karoshi/server_network/clients/windows_client_versions | sort -V)
do
	source /opt/karoshi/server_network/clients/windows_client_versions/"$WINDOWSVERSION"
	echo '
					<option value="'"$WINDOWS_VERSION"'">'"$WINDOWS_NAME"'</option>'
done
echo '
				</select>
			</td>
		</tr>
	</tbody>
</table>
'

echo '<br>'$"Select the groups that you want to copy the icons to."'<br><br>
<table>
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Group"'</b></th>
			<th></th>
		</tr>
	</thead>
	<tbody>'

for GROUPNAMES in /opt/karoshi/server_network/group_information/*
do
	GROUPNAME=$(basename "$GROUPNAMES")
	if [ $GROUPNAME != optional_groups ]
	then
		echo '
		<tr>
			<td>'"$GROUPNAME"'</td>
			<td style="text-align:right"><input id="PriGroup'"$GROUPNAME"'" name="___PRIGROUP___" value="'"$GROUPNAME"'" type="checkbox"><label for="PriGroup'"$GROUPNAME"'"> </label></td>
		</tr>'
	fi
done

echo '
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"> <input type="button" class="button" onclick="SetAllCheckBoxes('\'selectgroups\'', '\'___PRIGROUP___\'', true);" value="'$"Select all"'">
</form>
</body>
</html>
'
exit
