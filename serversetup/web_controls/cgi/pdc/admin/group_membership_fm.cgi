#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Group Membership"
TITLEHELP=$"This shows the groups that the user is a member of."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Group_Membership"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/script.js"></script>'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/group_membership.cgi" method="post">
<table>
   <tbody>
	<tr>
		<td class="karoshi-input">'$"Username"'</td><td style="vertical-align: top;"><div id="suggestions"></div>
			<input tabindex= "1" class="karoshi-input" name="____USERNAME____" value="'"$USERNAME"'" size="20" type="text" id="inputString" onkeyup="lookup(this.value);">
		</td>
		<td style="vertical-align: top;">
			<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Group_Membership"><span>'$"Enter in the username you want to change the groups for."'</span></a>
		</td>
	</tr>
	<tr>
		<td>'$"User Photo"'</td>
        	<td>
	 		<div id="photobox"><img src="/images/blank_user_image.jpg" width="120" height="150"></div>
		</td>
	</tr>
   </tbody>
</table><br><br>

<input value="'$"Submit"'" class="button" type="submit">

</display-karoshicontent>
</body></html>'
exit

