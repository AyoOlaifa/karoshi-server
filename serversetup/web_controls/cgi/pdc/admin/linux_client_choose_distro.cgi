#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Client - Choose Distribution"
TITLEHELP=$"Choose the linux client iso that you want to distribute to your linux clients over the network."
HELPURL="https://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Distribution_Server"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
DATA=$(echo "$DATA" | sed 's/___/TRIPLEUNDERSCORE/g' | sed 's/_/UNDERSCORE/g' | sed 's/TRIPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=15
#Assign DISTROCHOICE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = DISTROCHOICEcheck ]
	then
		let COUNTER=$COUNTER+1
		DISTROCHOICE=`echo $DATA | cut -s -d'_' -f$COUNTER`
		DISTROCHOICE=`echo $DISTROCHOICE | sed 's/UNDERSCORE/_/g'`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign CONTROL
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = CONTROLcheck ]
	then
		let COUNTER=$COUNTER+1
		CONTROL=`echo $DATA | cut -s -d'_' -f$COUNTER`
		CONTROL=`echo $CONTROL | sed 's/UNDERSCORE/_/g'`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign NETBOOT
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = NETBOOTcheck ]
	then
		let COUNTER=$COUNTER+1
		NETBOOT=`echo $DATA | cut -s -d'_' -f$COUNTER`
		NETBOOT=`echo $NETBOOT | sed 's/UNDERSCORE/_/g'`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/linux_client_choose_distro_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that DISTROCHOICE is not blank
if [ -z "$DISTROCHOICE" ]
then
	MESSAGE=$"This folder does not contain any iso images."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/linux_client_choose_distro.cgi | cut -d' ' -f1)
#Copy iso
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DISTROCHOICE:$CONTROL:$NETBOOT:" | sudo -H /opt/karoshi/web_controls/exec/linux_client_choose_distro2

echo '
<script>
	window.location = "/cgi-bin/admin/linux_client_choose_distro_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
