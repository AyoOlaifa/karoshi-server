#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Set Default Page"
TITLEHELP=$"Choose the default page that you want to have for this section of the web management."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<form action="/cgi-bin/admin/set_default_page.cgi" method="post">
				<table>
					<tbody>
						<tr>
			 				<td class="karoshi-input">'$"Default Page"'</td>
							<td>
								<select required="required" name="_DEFAULTPAGE_" class="karoshi-input">'

for SEARCHRESULT in $(/opt/karoshi/web_controls/generate_sidebar_admin | grep "href=" | grep -i \"*"$SEARCH" | grep -v 'class="mid"' | grep -v warnings | grep -v 'class="top"' | sed 's% %SPACE%g' | sort)
do 
	URL=$(echo "$SEARCHRESULT" | cut -d= -f2 | cut -d'"' -f2)
	URL=$(basename "$URL")
	TITLE=$(echo "$SEARCHRESULT" | cut -d">" -f3 | cut -d"<" -f1 | sed 's%SPACE% %g')
	#Try and get some help info for the page
	HELPINFO=$(echo "$SEARCHRESULT" | sed 's/^<!--//g' | cut -s -d"!" -f2 | sed 's%SPACE% %g' | sed 's%--%%g' | sed 's%>%%g')

	echo '
								<option value="'"$URL"'">'"$TITLE"'</option>'
done

echo '
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">'

echo '
			</form>
		</display-karoshicontent>
	</body>
</html>'
exit
