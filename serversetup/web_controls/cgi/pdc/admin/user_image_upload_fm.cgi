#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Import User Images"
TITLEHELP=$"This allows you to upload user images that can be used to identify your users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=User_Images"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
		<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/user_image_upload.cgi" METHOD="POST">

			<button class="button" formaction="bulk_user_creation_upload_fm.cgi" name="_BulkUserCreation_" value="_">
				'$"Bulk User Creation"'
			</button>

			<button class="button" formaction="bulk_user_creation_view_passwords_fm.cgi" name="_ViewNewPasswords_" value="_">
				'$"View New Passwords"'
			</button>

			<button class="button" formaction="bulk_user_creation_import_enrollment_numbers_fm.cgi" name="_ImportEnrollmentNumbers_" value="_">
				'$"Import Enrollment Numbers"'
			</button>

			<button class="button" formaction="csv_set_passwords_fm.cgi" name="_SetUserPasswords_" value="_">
				'$"Set User Passwords"'
			</button>

			<br>
			<br>
		
			<table>
				<tr>
					<td class="karoshi-input">'$"User Image or compressed file"':</td>
			    		<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=User_Images"><span class="icon-large-tooltip">'$"Choose a jpg image which corresponds to a user name or enrollment number or choose a zip or tar.gz file that contains multiple images."'<br><br>'$"Example"' - jjones.jpg - 10111.jpg.<br><br>'$"User images default to width 120px height 150px."'</span></a></td>
				</tr>
			</table>
			<input value="'$"Submit"'" class="button primary" type="submit">
	
		</form>
	</body>
</html>
'
exit
