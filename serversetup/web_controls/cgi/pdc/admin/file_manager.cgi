#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"File Manager"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script>
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	4: { sorter: false}
    		}
		});
    } 
);
</script>
'

DATA=$(cat | tr -cd 'A-Za-z0-9\._:&%\-+*')
#CONVERT STAR
DATA=$(echo "$DATA" | sed 's/*/%99/g')
#echo $DATA"<br>"
#########################
#Assign data to variables
#########################
END_POINT=45
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVER
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign LOCATION
DATANAME=LOCATION
get_data
LOCATION=$(echo "$DATAENTRY" |sed 's/%2F/\//g' | sed "s/Z%25%25%25%25%25Z/_/g")

function show_status {
echo '<script>
alert("'"$MESSAGE"'");
window.location = "/cgi-bin/admin/file_manager.cgi";
</script></div></body></html>'
exit
}

if [ -z "$ACTION" ]
then
	ACTION=notset
fi

if [ -z "$SERVERNAME" ]
then
	SERVERNAME=notset
fi
if [ -z "$SERVERTYPE" ]
then
	SERVERTYPE=notset
fi

if [ $SERVERTYPE = federatedslave ]
then
	#Assign SERVERMASTER
	DATANAME=SERVERMASTER
	get_data
	SERVERMASTER="$DATAENTRY"
fi

#########################
#Check data
#########################
if [ "$ACTION" != ENTER ] && [ "$ACTION" != DELETE ] && [ "$ACTION" != REALLYDELETE ] && [ "$ACTION" != SETPERMS ] && [ "$ACTION" != REALLYSETPERMS ] && [ "$ACTION" != MOVE ] && [ "$ACTION" != REALLYMOVE ] && [ "$ACTION" != REALLYCOPY ] && [ "$ACTION" != CANCELCOPY ] && [ "$ACTION" != RENAME ] && [ "$ACTION" != REALLYRENAME ] && [ "$ACTION" != EDIT ] && [ "$ACTION" != REALLYEDIT ] && [ "$ACTION" != CREATEDIR ] && [ "$ACTION" != REALLYCREATEDIR ] && [ "$ACTION" != CREATEFILE ] && [ "$ACTION" != REALLYCREATEFILE ] && [ "$ACTION" != RESTORE ] && [ "$ACTION" != REALLYRESTORE ] && [ "$ACTION" != SEARCHBACKUP ]  && [ "$ACTION" != REALLYSEARCHBACKUP ] && [ "$ACTION" != notset ] && [ "$ACTION" != DELETEACLPERMS ] && [ "$ACTION" != REALLYDELETEACLPERMS ] && [ "$ACTION" != ADDACLPERMS ] && [ "$ACTION" != REALLYADDACLPERMS ] && [ "$ACTION" != SQLRESTORE ] && [ "$ACTION" != UPLOADFILE ] && [ "$ACTION" != REALLYUPLOADFILE ]
then
	MESSAGE=$"You have not entered a correct action."
	show_status
fi

if [ "$ACTION" = REALLYSEARCHBACKUP ]
then
	END_POINT=32
	COUNTER=2
	#Assign SEARCH
	DATANAME=SEARCH
	get_data
	SEARCH="$DATAENTRY"
fi

if [ "$ACTION" = REALLYEDIT ]
then
	END_POINT=32
	COUNTER=2
	#Assign TEXTDATA
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = TEXTDATAcheck ]
		then
			let COUNTER=$COUNTER+1
			TEXTDATA=`echo "$DATA" | cut -s -d'_' -f$COUNTER-`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = REALLYCREATEDIR ] || [ "$ACTION" = REALLYCREATEFILE ]
then
	#Assign NEWFOLDER
	END_POINT=32
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = NEWFOLDERcheck ]
		then
			let COUNTER=$COUNTER+1
			NEWFOLDER=`echo "$DATA" | cut -s -d'_' -f$COUNTER- | cut -d'&' -f1`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = REALLYRENAME ]
then
	#Assign NEWFOLDER
	END_POINT=32
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo "$DATAHEADER"'check'` = NEWFOLDERcheck ]
		then
			let COUNTER=$COUNTER+1
			NEWFOLDER=`echo "$DATA" | cut -s -d'_' -f$COUNTER- | cut -d'&' -f1`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = REALLYADDACLPERMS ]
then
	END_POINT=50

	#Assign ACLOWNER
	DATANAME=ACLOWNER
	get_data
	ACLOWNER="$DATAENTRY"

	#Assign ACLPERMISSIONS
	DATANAME=ACLPERMISSIONS
	get_data
	ACLPERMISSIONS="$DATAENTRY"	
fi

if [ "$ACTION" = DELETEACLPERMS ] || [ "$ACTION" = REALLYDELETEACLPERMS ]
then
	END_POINT=50
	#Assign ACLOWNER
	DATANAME=ACLOWNER
	get_data
	ACLOWNER="$DATAENTRY"

	#Assign ACLGROUP
	DATANAME=ACLGROUP
	get_data
	ACLGROUP="$DATAENTRY"
fi

if [ "$ACTION" = SQLRESTORE ]
then
	#Assign DBNAME
	END_POINT=32
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo "$DATAHEADER"'check'` = DBNAMEcheck ]
		then
			let COUNTER=$COUNTER+1
			DBNAME=`echo "$DATA" | cut -s -d'_' -f$COUNTER- | cut -d'&' -f1`
			break
		fi
		let COUNTER=$COUNTER+1
	done

	#Assign DBUSERNAME
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo "$DATAHEADER"'check'` = DBUSERNAMEcheck ]
		then
			let COUNTER=$COUNTER+1
			DBUSERNAME=`echo "$DATA" | cut -s -d'_' -f$COUNTER- | cut -d'&' -f1`
			break
		fi
		let COUNTER=$COUNTER+1
	done

	#Assign  DBPASSWORD
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo "$DATA" | cut -s -d'_' -f$COUNTER`
		if [ `echo "$DATAHEADER"'check'` = DBPASSWORDcheck ]
		then
			let COUNTER=$COUNTER+1
			 DBPASSWORD=`echo "$DATA" | cut -s -d'_' -f$COUNTER- | cut -d'&' -f1`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

if [ "$ACTION" = REALLYSETPERMS ]
then
	DATA=$(echo "$DATA" | tr -cd 'A-Za-z0-9\._:%\-+*')
	END_POINT=42
	#Assign OWNER
	DATANAME=OWNER
	get_data
	OWNER="$DATAENTRY"

	#Assign GROUP
	DATANAME=GROUP
	get_data
	GROUP="$DATAENTRY"

	#Assign USERREAD
	DATANAME=USERREAD
	get_data
	USERREAD="$DATAENTRY"
	[ "$USERREAD" = 1 ] && PERMISSIONS=USERREAD

	#Assign USERWRITE
	DATANAME=USERWRITE
	get_data
	USERWRITE="$DATAENTRY"
	[ "$USERWRITE" = 1 ] && PERMISSIONS="$PERMISSIONS,USERWRITE"

	#Assign USEREXEC
	DATANAME=USEREXEC
	get_data
	USEREXEC="$DATAENTRY"
	[ "$USEREXEC" = 1 ] && PERMISSIONS="$PERMISSIONS,USEREXEC"

	#Assign GROUPREAD
	DATANAME=GROUPREAD
	get_data
	GROUPREAD="$DATAENTRY"
	[ "$GROUPREAD" = 1 ] && PERMISSIONS="$PERMISSIONS,GROUPREAD"

	#Assign GROUPWRITE
	DATANAME=GROUPWRITE
	get_data
	GROUPWRITE="$DATAENTRY"
	[ "$GROUPWRITE" = 1 ] && PERMISSIONS="$PERMISSIONS,GROUPWRITE"

	#Assign GROUPEXEC
	DATANAME=GROUPEXEC
	get_data
	GROUPEXEC="$DATAENTRY"
	[ "$GROUPEXEC" = 1 ] && PERMISSIONS="$PERMISSIONS,GROUPEXEC"

	#Assign OTHERREAD
	DATANAME=OTHERREAD
	get_data
	OTHERREAD="$DATAENTRY"
	[ "$OTHERREAD" = 1 ] && PERMISSIONS="$PERMISSIONS,OTHERREAD"

	#Assign OTHERWRITE
	DATANAME=OTHERWRITE
	get_data
	OTHERWRITE="$DATAENTRY"
	[ "$OTHERWRITE" = 1 ] && PERMISSIONS="$PERMISSIONS,OTHERWRITE"

	#Assign OTHEREXEC
	DATANAME=OTHEREXEC
	get_data
	OTHEREXEC="$DATAENTRY"
	[ "$OTHEREXEC" = 1 ] && PERMISSIONS="$PERMISSIONS,OTHEREXEC"

	#Assign SETUID
	DATANAME=SETUID
	get_data
	SETUID="$DATAENTRY"
	[ "$SETUID" = 1 ] && PERMISSIONS="$PERMISSIONS,SETUID"

	#Assign SETGID
	DATANAME=SETGID
	get_data
	SETGID="$DATAENTRY"
	[ "$SETGID" = 1 ] && PERMISSIONS="$PERMISSIONS,SETGID"

	#Assign STICKY
	DATANAME=STICKY
	get_data
	STICKY="$DATAENTRY"
	[ "$STICKY" = 1 ] && PERMISSIONS="$PERMISSIONS,STICKY"

	#Assign RECURSIVE
	DATANAME=RECURSIVE
	get_data
	RECURSIVE="$DATAENTRY"
	[ "$RECURSIVE" = 1 ] && PERMISSIONS="$PERMISSIONS,RECURSIVE"

	#Assign EXECRECURSE
	DATANAME=EXECRECURSE
	get_data
	EXECRECURSE="$DATAENTRY"
	[ "$EXECRECURSE" = 1 ] && PERMISSIONS="$PERMISSIONS,EXECRECURSE"

	#Check to see that owner is not blank
	if [ -z "$OWNER" ]
	then
		MESSAGE=$"The owner cannot be blank."
		show_status
	fi

	#Check to see that group is not blank
	if [ -z "$GROUP" ]
	then
		MESSAGE=$"The group cannot be blank."
		show_status
	fi
fi

END_POINT=12
#Assign ITEMMOVE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [ $(echo "$DATAHEADER"'check') = ITEMMOVEcheck ]
	then
		let COUNTER="$COUNTER"+1
		ITEMMOVE=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER"  | sed 's/%2F/\//g' | sed "s/Z%25%25%25%25%25Z/_/g")
		break
	fi
	let COUNTER="$COUNTER"+1
done

if [ "$ACTION" = notset ]
then
	SERVER2=""
else
	SERVER2=$(echo "- ${SERVERNAME:0:9}" | cut -d. -f1)
fi

#Add the servername to the title
TITLE=''$"File Manager"' '"$SERVER2"''

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/file_manager.cgi | cut -d' ' -f1)

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"
echo '<form action="/cgi-bin/admin/file_manager.cgi" method="post">'	
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$SERVERNAME:$SERVERTYPE:$SERVERMASTER:$LOCATION:$FILENAME:$ACTION:$PERMISSIONS:$OWNER:$GROUP:$ITEMMOVE:$NEWFOLDER:$SEARCH:$TEXTDATA:$ACLOWNER:$ACLGROUP:$ACLPERMISSIONS:$DBNAME:$DBUSERNAME:$DBPASSWORD:" | sudo -H /opt/karoshi/web_controls/exec/file_manager

echo '</form></display-karoshicontent></body></html>'
			


exit
