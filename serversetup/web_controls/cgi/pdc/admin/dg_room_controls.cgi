#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Internet Controls"
TITLEHELP=$"Choose the location that you want to allow or deny internet access for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Room_Controls"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\+-')
#########################
#Assign data to variables
#########################
END_POINT=6
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign LOCATION
DATANAME=LOCATION
get_data
LOCATION="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/dg_room_controls_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$ERRORMSG7
	show_status
fi

#########################
#Check data
#########################

#Check to see that LOCATION is not blank
if [ -z "$LOCATION" ]
then
	MESSAGE=$"You have not chosen a location."
	show_status
fi


#Generate navigation bar
if [ "$MOBILE" = no ]
then
	WIDTH=70
	WIDTH2=150
	WIDTH3=175
	WIDTH4=100
	ICON1=/images/assets/curriculum_computer.png
	ICON2=/images/submenus/system/clock.png
	ICON3=/images/submenus/internet/client_allowed.png
	ICON4=/images/submenus/internet/client_denied.png
	ICON5=/images/submenus/internet/client_allowed.png
	ICON6=/images/submenus/internet/client_denied.png
	TITLE=''$"Client Internet Controls"' - '$LOCATION''
else
	WIDTH=70
	WIDTH2=90
	WIDTH3=110
	WIDTH4=110
	ICON1=/images/assets/curriculum_computerm.png
	ICON2=/images/submenus/system/clockm.png
	ICON3=/images/submenus/internet/client_allowedm.png
	ICON4=/images/submenus/internet/client_deniedm.png
	ICON5=/images/submenus/internet/client_allowedm.png
	ICON6=/images/submenus/internet/client_deniedm.png
	TITLE="$LOCATION"
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	2: { sorter: "ipAddress" },
	3: { sorter: "MAC" }
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="dg_room_controls_fm.cgi" method="post">
	<button class="button" name="_Location_" value="_">
		'$"Location"'
	</button>

	<button formaction="dg_room_controls2.cgi" class="button" name="_AllowAll_" value="_LOCATION_'"$LOCATION"'_ACTION_allowall_ASSET_na_">
		'$"Allow All"'
	</button>

	<button formaction="dg_room_controls2.cgi" class="button" name="_DenyAll_" value="_LOCATION_'"$LOCATION"'_ACTION_denyall_ASSET_na_">
		'$"Deny All"'
	</button>

	<button formaction="dg_room_controls2.cgi"  class="button" name="_AllowAllMedia_" value="_LOCATION_'"$LOCATION"'_ACTION_allowallmedia_ASSET_na_">
		'$"Allow All Media"'
	</button>

	<button formaction="dg_room_controls2.cgi" class="button" name="_DenyAllMedia_" value="_LOCATION_'"$LOCATION"'_ACTION_denyallmedia_ASSET_na_">
		'$"Deny All Media"'
	</button>
</form>
'

if [ -d /opt/karoshi/asset_register/locations/"$LOCATION"/ ]
then
	if [[ $(ls -1 /opt/karoshi/asset_register/locations/"$LOCATION"/ | wc -l) -gt 0 ]]
	then
		echo '
			<style>
			form {
				margin: 0;
			}
			</style><br>
			<table id="myTable" class="tablesorter">
				<thead><tr><th style="width: '"$WIDTH"'px;"><b>'$"Asset"'</b></th>'

		[ "$MOBILE" = no ] && echo '<th style="width: '"$WIDTH3"'px;"><b>'$"Mac-address"'</b></th><th style="width: '"$WIDTH2"'px;"><b>'$"Tcpip"'</b></th>'

		echo '<th><b>'$"Access"'</b></th><th><b>'$"Media Access"'</b></th></tr></thead><tbody>'

		for ASSETS in "/opt/karoshi/asset_register/locations/$LOCATION/"*
		do
			ASSET=$(basename "$ASSETS")
			source /opt/karoshi/asset_register/locations/"$LOCATION/$ASSET"
			#Only show certain asset types
			if [ "$ASSETTYPE" = 1 ] || [ "$ASSETTYPE" = 3 ] || [ "$ASSETTYPE" = 5 ] || [ "$ASSETTYPE" = 7 ] || [ "$ASSETTYPE" = 9 ]
			then
				CONTROLMSG=$"Deny access"
				COLOUR=#096F16
				ACTION=deny
			if [ -f /opt/karoshi/server_network/internet_room_controls/"$LOCATION/$ASSET" ]
			then
				CONTROLMSG=$"Allow access"
				COLOUR=#FF0000
				ACTION=allow
			fi

			CONTROLMSG2=$"Deny media access"
			COLOUR2=#096F16
			ACTION2=denymedia
			if [ -f /opt/karoshi/server_network/internet_media_room_controls/"$LOCATION/$ASSET"_media ]
			then
				CONTROLMSG2=$"Allow media access"
				COLOUR2=#FF0000
				ACTION2=allowmedia
			fi


			echo '<tr><td>'"$ASSET"'</td>'

			[ "$MOBILE" = no ] && echo '<td>'"$MAC1"'</td><td>'"$TCPIP1"'</td>'

			echo '<td><form action="/cgi-bin/admin/dg_room_controls2.cgi" method="post">
			<input name="_ACTION_'"$ACTION"'_LOCATION_'"$LOCATION"'_ASSET_'"$ASSET"'_" type="submit" class="button" style="color:'"$COLOUR"';" value="'"$CONTROLMSG"'"></form></td>

			<td><form action="/cgi-bin/admin/dg_room_controls2.cgi" method="post"><input name="_ACTION_'"$ACTION2"'_LOCATION_'"$LOCATION"'_ASSET_'"$ASSET"'_" type="submit" class="button" style="color:'"$COLOUR2"';" value="'"$CONTROLMSG2"'"></form></td>
			</tr>
			'
			fi
		done
	fi
fi
echo '</tbody></table>
</display-karoshicontent>
</body>
</html>'
exit
