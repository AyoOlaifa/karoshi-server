#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Staff Restrictions"
TITLEHELP=$"Enter in the usernames of any members of staff that you do not want to be able to access the staff section of the web management."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign delete
DATANAME=DELETE
get_data
DELETE="$DATAENTRY"

#Assign staffname
DATANAME=STAFFNAME
get_data
STAFFNAME="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/staff_restrictions.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that delete is not blank
if [ -z "$DELETE" ] && [ -z "$STAFFNAME" ]
then
	MESSAGE=$"You have not selected an action."
	show_status
fi
#Check to see that username exists
getent passwd "$STAFFNAME" 1>/dev/null 2>/dev/null
if [ "$?" != 0 ]
then
	MESSAGE=$"This user does not exist."
	show_status
fi

#Check to see that the username is a member of staff
PRIGROUP=$(id -g -n "$STAFFNAME")
if [ "$PRIGROUP" != staff ] && [ "$PRIGROUP" != officestaff ]
then
	MESSAGE=$"This user is not a member of staff."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/staff_restrictions2.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/staff_restrictions "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DELETE:$STAFFNAME"
echo '
<script>
	window.location = "staff_restrictions.cgi";
</script>
'
exit
