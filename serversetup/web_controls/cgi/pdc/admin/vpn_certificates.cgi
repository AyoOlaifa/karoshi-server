#!/bin/bash
#Copyright (C) 2015 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"VPN Certificates"
TITLEHELP=$"Create VPN certificates for client computers to connect to your VPN server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=OpenVPN_Server#Creating_Client_Certificates"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+-')
#########################
#Assign data to variables
#########################
END_POINT=11
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

if [ -z "$ACTION" ]
then
	ACTION=view
fi

if [ "$ACTION" = reallyaddcomment ]
then
	#Assign COMMENT
	DATANAME=COMMENT
	get_data
	COMMENT="$DATAENTRY"
fi

if [ "$ACTION" = view ] || [ "$ACTION" = reallyaddcomment ]
then
	TITLEMSG=$"VPN Certificates"
	ACTION2=add
	ACTIONMSG=$"Add Certificate"
	ACTIONMSG2=$"Add"
	ICON1=/images/submenus/system/add.png
fi

if [ "$ACTION" = add ] || [ "$ACTION" = reallyadd ] || [ "$ACTION" = unrevoke ] || [ "$ACTION" = revoke ] || [ "$ACTION" = reallyunrevoke ]  || [ "$ACTION" = reallyrevoke ] || [ "$ACTION" = downloadcert ] || [ "$ACTION" = addcomment ]
then
	ACTION2=view
	ACTIONMSG=$"View Certificates"
	ACTIONMSG2=$"View"
	ICON1=/images/submenus/system/vpn.png
fi

if [ "$ACTION" = add ] || [ "$ACTION" = reallyadd ]
then
	TITLEMSG=$"Create Client VPN Certificate"
fi

if [ "$ACTION" = revoke ] || [ "$ACTION" = reallyrevoke ]
then
	TITLEMSG=$"Revoke Client VPN Certificate"
fi

if [ "$ACTION" = unrevoke ] || [ "$ACTION" = reallyunrevoke ]
then
	TITLEMSG=$"Un-Revoke Client VPN Certificate"
fi

if [ "$ACTION" = addcomment ]
then
	TITLEMSG=$"Add a comment to a VPN account"
fi


if [ "$ACTION" = downloadcert ]
then
	TITLEMSG=$"Download Client VPN Certificate"
fi

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/vpn_certificates.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi


#########################
#Check data
#########################

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyrevoke ] || [ "$ACTION" = reallyunrevoke ]
then
	#Make sure that a username has been entered.
	if [ -z "$USERNAME" ]
	then
		MESSAGE=$"You must enter in a username."
		show_status
	fi

	if [ "$ACTION" = reallyadd ]
	then
		#Check that the username exists
		getent passwd "$USERNAME" 1>/dev/null 2>/dev/null
		if [ "$?" != 0 ]
		then
			MESSAGE=$"This username does not exist."
			show_status
		fi
	fi
fi

echo '
<form action="/cgi-bin/admin/vpn_certificates.cgi" method="post">
	<button class="button" name="_DoAction_" value="_ACTION_'$ACTION2'_">
		'"$ACTIONMSG2"'
	</button>
</form>'


Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/vpn_certificates.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$USERNAME:$COMMENT:" | sudo -H /opt/karoshi/web_controls/exec/vpn_certificates
echo '
</display-karoshicontent>
</body>
</html>'
exit

