#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Exam Accounts"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts"
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/exam_accounts.cgi | cut -d' ' -f1)

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin
source /opt/karoshi/server_network/security/password_settings

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%*+-' | sed 's/*/%1123/g' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
ACTION2=notset
DeleteCode="${RANDOM:0:3}"
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=ChooseAction


if [ "$ACTION" = CleanExamHomeAreas ]
then
	#Assign DELETECODE
	DATANAME=DELETECODE
	get_data
	DELETECODE="$DATAENTRY"

	#Assign CONFIRMDELETECODE
	DATANAME=CONFIRMDELETECODE
	get_data
	CONFIRMDELETECODE="$DATAENTRY"
fi

if [ "$ACTION" = ResetPasswords ] || [ "$ACTION" = ChangePasswords ] || [ "$ACTION" = ExamAccountAccess ] || [ "$ACTION" = CleanExamHomeAreas ]
then
	#Assign EXCEPTIONLIST
	DATANAME=EXCEPTIONLIST
	get_data
	EXCEPTIONLIST="$DATAENTRY"
fi

if [ "$ACTION" = ChangePasswords ]
then
	#Assign PASSWORD1
	DATANAME=PASSWORD1
	get_data
	PASSWORD1="$DATAENTRY"

	#Assign PASSWORD2
	DATANAME=PASSWORD2
	get_data
	PASSWORD2="$DATAENTRY"
fi

if [ "$ACTION" = CreateExtraExamAccounts ]
then

	#Assign NEWEXAMACCOUNTS
	DATANAME=NEWEXAMACCOUNTS
	get_data
	NEWEXAMACCOUNTS=$(echo "$DATAENTRY" | tr -cd 0-9)
fi

if [ "$ACTION" = ExamAccountAccess ] || [ "$ACTION" = DownloadExamAccounts ] || [ "$ACTION" = ResetPasswords ]
then
	#Assign ACTION2
	DATANAME=ACTION2
	get_data
	ACTION2="$DATAENTRY"
fi

if [ "$ACTION" = ArchiveExamAccounts ]
then
	#Assign USERNAME
	DATANAME=USERNAME
	get_data
	USERNAME="$DATAENTRY"

	#Assign GROUP
	DATANAME=GROUP
	get_data
	GROUP="$DATAENTRY"

	#Assign SERVER
	DATANAME=SERVER
	get_data
	SERVER="$DATAENTRY"

	#Assign SHARE
	DATANAME=SHARE
	get_data
	SHARE="$DATAENTRY"
fi

function show_status {
echo '
<script language="Javascript">
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/exam_accounts.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check data
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ "$ACTION" = CleanExamHomeAreas ]
then
	if [ -z "$DELETECODE" ]
	then
		MESSAGE=$"You have not entered a delete code."
		show_status
	fi

	if [ -z "$CONFIRMDELETECODE" ]
	then
		MESSAGE=$"You have not entered the confirm delete code."
		show_status
	fi

	if [ "$DELETECODE" != "$CONFIRMDELETECODE" ] 
	then
		MESSAGE=$"You have not entered the correct delete code."
		show_status
	fi
fi

if [ "$ACTION" = CreateExtraExamAccounts ]
then
	if [ -z "$NEWEXAMACCOUNTS" ]
	then
		MESSAGE=$"You have not entered how many new exam accounts you want."
		show_status
	fi	
fi

if [ "$ACTION" = ExamAccountAccess ]
then
	if [ -z "$ACTION2" ]
	then
		MESSAGE=$"You have not chosen to enable or disable the exam accounts."
		show_status		
	fi
fi

if [ "$ACTION" = ResetPasswords ]
then
	if [ -z "$ACTION2" ]
	then
		MESSAGE=$"You have not chosen to reset or view the exam account passwords."
		show_status		
	fi
fi

if [ "$ACTION" = ChangePasswords ]
then
	#Check password settings
	if [ -z "$PASSWORD1" ] || [ -z "$PASSWORD2" ]
	then
		MESSAGE=$"You have not entered a password."
		show_status	
	fi

	#Check that password has been entered correctly
	if [ "$PASSWORD1" != "$PASSWORD2" ]
	then
		MESSAGE=$"The passwords do not match."
		show_status
	fi

	#Convert special characters back for new password to check password strength
	NEW_PASSWORD=$(echo "$PASSWORD1" | sed 's/+/ /g; s/%21/!/g; s/%3F/?/g; s/%2C/,/g; s/%3A/:/g; s/%7E/~/g; s/%40/@/g; s/%23/#/g; s/%24/$/g; s/%26/\&/g; s/%2B/+/g; s/%3D/=/g; s/%28/(/g; s/%29/)/g; s/%5E/^/g; s/%7B/{/g; s/%7D/}/g; s/%3C/</g; s/%3E/>/g; s/%5B/[/g; s/%5D/]/g; s/%7C/|/g; s/%22/"/g; s/%1123/*/g' | sed "s/%27/'/g" | sed 's/%3B/;/g' | sed 's/%60/\`/g' | sed 's/%5C/\\/g' | sed 's/%2F/\//g' | sed 's/%25/%/g')

	PASSLENGTH=${#NEW_PASSWORD}

	#Check to see that password has the required number of characters
	if [ "$PASSLENGTH" -lt "$MINPASSLENGTH" ]
	then
		MESSAGE=''$"Your password length"': '$PASSLENGTH'\n'$"Required password length"': '$MINPASSLENGTH''
		show_status
	fi


	if [ "$PASSWORDCOMPLEXITY" = on ]
	then
		CASECHECK=ok
		CHARCHECK=ok

		#Check that the password has a combination of characters and numbers
		if [[ $(echo "$PASSWORD1"'1' | tr -cd '0-9\n') = 1 ]]
		then
			CHARCHECK=fail
		fi
		if [[ $(echo "$PASSWORD1"'A' | tr -cd 'A-Za-z\n') = A ]]
		then
			CHARCHECK=fail
		fi

		if [[ $(echo "$PASSWORD1"'A' | tr -cd 'A-Z\n') = A ]]
		then
			CASECHECK=fail
		fi
		if [[ $(echo "$PASSWORD1"'a' | tr -cd 'a-z\n') = a ]]
		then
			CASECHECK=fail
		fi

		if [ "$CASECHECK" = fail ] || [ "$CHARCHECK" = fail ]
		then
			MESSAGE=$"A combination of upper and lower case characters and numbers is required."
			show_status
		fi
	fi
fi

if [ "$ACTION" = ArchiveExamAccounts ]
then
	if [ -z "$SHARE" ]
	then
		MESSAGE=$"You have not chosen a storage area."
		show_status
	fi

	if [ -z "$SERVER" ]
	then
		MESSAGE=$"The servername cannot be blank."
		show_status
	fi

	if [ -z "$USERNAME" ] && [ -z "$GROUP" ]
	then
		MESSAGE=$"You must choose either a user or a group to set the permissions for the archive."
		show_status
	fi

	if [ ! -f "/opt/karoshi/server_network/network_shares/$SERVER/$SHARE" ]
	then
		MESSAGE=$"The share definition for this share does not exist."
		show_status
	fi

	#Check to see the the user and group can get to the share
	source "/opt/karoshi/server_network/network_shares/$SERVER/$SHARE"

	if [[ $(echo "$GROUPLIST" | grep -c "$GROUP") = 0 ]]
	then
		MESSAGE=$"This group cannot access the network share."
		show_status
	fi

fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"



if [ "$ACTION" != ChooseAction ]
then

	if [ "$ACTION" = CopyData ]
	then
		echo '
		<script>
			window.location = "/cgi-bin/admin/exam_accounts_upload_fm.cgi"
		</script>
		</display-karoshicontent></body></html>'		
		exit
	fi

	if [ "$ACTION2" = ViewResetExamPasswords ]
	then
		echo '
		<form action="/cgi-bin/admin/exam_accounts.cgi" method="post">
			<button class="button">
				'$"Manage Exam Accounts"'
			</button>
		</form>'
	fi


	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$ACTION2:$EXCEPTIONLIST:$PASSWORD1:$NEWEXAMACCOUNTS:$USERNAME:$GROUP:$SERVER:$SHARE:$DELETE:" | sudo -H /opt/karoshi/web_controls/exec/exam_accounts

	if [ "$?" = 105 ]
	then
		MESSAGE=$"The exam accounts have not had their passwords reset."
		show_status
	fi

	#Reload the page
	if [ "$ACTION2" = ViewResetExamPasswords ]
	then
		echo '
		<form action="/cgi-bin/admin/exam_accounts.cgi" method="post">
			<button class="button">
				'$"Manage Exam Accounts"'
			</button>
		</form>'
	else
		echo '
		<script>
			window.location = "/cgi-bin/admin/exam_accounts.cgi"
		</script>'

	fi
	echo '</display-karoshicontent></body></html>'
else

	echo '<script>
	function ChangePasswords() {
	document.getElementById("ChangePasswords").className = "button primary";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Change Exam Passwords"'<input type=\"hidden\" name=\"____ACTION____\" value=\"ChangePasswords\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This will change all of the exam account passwords to the same password."'</span></a>";

	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Password"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input required=\"required\" pattern=\".{'"$MINPASSLENGTH"',128}\" title=\"'$"Password length required:"' '"$MINPASSLENGTH"'\" tabindex= \"1\" name=\"____PASSWORD1____\" class=\"karoshi-input\" size=\"20\" type=\"password\">";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"Enter the password that you want all of the exam accounts changed to."'</span>";
	var el = document.getElementById("extraoptions4");
		el.innerHTML = "'$"Confirm"'";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "<input required=\"required\" pattern=\".{'"$MINPASSLENGTH"',128}\" title=\"'$"Password length required:"' '"$MINPASSLENGTH"'\" tabindex= \"2\" name=\"____PASSWORD2____\" class=\"karoshi-input\" size=\"20\" type=\"password\">";
	var el = document.getElementById("extraoptions7");
		el.innerHTML = "'$"Exceptions"'";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "<input tabindex= \"3\" name=\"____EXCEPTIONLIST____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Reset_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"Enter in any exam accounts that you do not want to have the passwords reset for separated by spaces."'</span></a>";

	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "";
	document.getElementById("advanced3").style.display = "";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function ResetPasswords() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button primary";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Reset Exam Passwords"'<input type=\"hidden\" name=\"____ACTION____\" value=\"ResetPasswords\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"View or reset the exam account passwords."'</span></a>";

	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"View Reset Exam Passwords"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input tabindex= \"1\" id=\"ViewResetExamPasswords\" type=\"radio\" name=\"____ACTION2____\" value=\"ViewResetExamPasswords\" checked=\"checked\"><label for=\"ViewResetExamPasswords\"> </label>";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"View the reset exam passwords."'</span></a>";

	var el = document.getElementById("extraoptions4");
		el.innerHTML = "'$"Reset Exam Passwords"'";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "<input id=\"ResetExamPasswords\" type=\"radio\" name=\"____ACTION2____\" value=\"ResetExamPasswords\"><label for=\"ResetExamPasswords\"> </label>";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This will reset all of the exam accounts to a different random password for each account."'</span></a>";


	var el = document.getElementById("extraoptions7");
		el.innerHTML = "'$"Exceptions"'";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "<input tabindex= \"3\" name=\"____EXCEPTIONLIST____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Reset_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"Enter in any exam accounts that you do not want to have the passwords reset for separated by spaces."'</span></a>";

	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "";
	document.getElementById("advanced3").style.display = "";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function ExamAccountAccess() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button primary";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Exam Account Access"'<input type=\"hidden\" name=\"____ACTION____\" value=\"ExamAccountAccess\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This allows you to enable or disable access to the exam accounts."'</span></a>";
	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Enable"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input tabindex= \"1\" id=\"EnableAccounts\" type=\"radio\" name=\"____ACTION2____\" value=\"enable\" checked=\"checked\"><label for=\"EnableAccounts\"> </label>";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Enable_.2F_Disable_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Enable the exam accounts so that they have be used to log in with."'</span></a>";
	var el = document.getElementById("extraoptions4");
		el.innerHTML = "'$"Disable"'";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "<input id=\"DisableAccounts\" type=\"radio\" name=\"____ACTION2____\" value=\"disable\"><label for=\"DisableAccounts\"> </label>";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Enable_.2F_Disable_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Disable the exam accounts so that users cannot log in with these accounts."'</span></a>";

	var el = document.getElementById("extraoptions7");
		el.innerHTML = "'$"Exceptions"'";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "<input tabindex= \"2\" name=\"____EXCEPTIONLIST____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Enable_.2F_Disable_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Enter in any exam accounts that you do not want to be enabled separated by spaces."'</span></a>";
	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "";
	document.getElementById("advanced3").style.display = "";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function CreateExtraExamAccounts() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button primary";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Create Extra Exam Accounts"'<input type=\"hidden\" name=\"____ACTION____\" value=\"CreateExtraExamAccounts\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This will create additional exam accounts starting from the next exam account in the sequence."'</span></a>";
	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Quantity"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input required=\"required\" tabindex= \"1\" name=\"____NEWEXAMACCOUNTS____\" class=\"karoshi-input\" size=\"20\" value=\"10\" type=\"number\">";
	var el = document.getElementById("extraoptions3");
	el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Create_Extra_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Enter in the number of new exam accounts you want to create."'</span></a>";
	var el = document.getElementById("extraoptions4");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions7");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "";
	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "none";
	document.getElementById("advanced3").style.display = "none";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function CopyData() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button primary";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Copy Data to Exam Accounts"'<input type=\"hidden\" name=\"____ACTION____\" value=\"CopyData\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"Use this to copy data to each of the exam account home areas."'</span></a>";
	var el = document.getElementById("extraoptions1");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions4");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions7");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "";
	document.getElementById("advanced1").style.display = "none";
	document.getElementById("advanced2").style.display = "none";
	document.getElementById("advanced3").style.display = "none";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function CleanExamHomeAreas() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button primary";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Clean Exam Home Areas"'<input type=\"hidden\" name=\"____ACTION____\" value=\"CleanExamHomeAreas\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This delete all files and folders in the exam account home areas ready for re use."'</span>";

	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Exceptions"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input tabindex= \"1\" name=\"____EXCEPTIONLIST____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Enter in any exam accounts that you do not want to delete files from."'</span></a>";
	var el = document.getElementById("extraoptions4");
		el.innerHTML = "'$"Confirm"'";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "<input required tabindex= \"2\" name=\"____CONFIRMDELETECODE____\" class=\"karoshi-input\" size=\"20\" type=\"number\">";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions7");
		el.innerHTML = "'$"Delete Code"'";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "'"$DeleteCode"'<input name=\"____DELETECODE____\" value=\"'"$DeleteCode"'\" type=\"hidden\">";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "";
	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "";
	document.getElementById("advanced3").style.display = "";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'

	echo '<script>
	function ArchiveExamAccounts() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button primary";
	document.getElementById("DownloadExamAccounts").className = "button";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Archive Exam Accounts"'<input type=\"hidden\" name=\"____ACTION____\" value=\"ArchiveExamAccounts\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This will archive all of the exam account home areas to a network share."'</span></a>";

	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Exceptions"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input tabindex= \"1\" name=\"____EXCEPTIONLIST____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Enter in any exam accounts that you do not want to delete files from."'</span></a>";

	var el = document.getElementById("extraoptions4");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions7");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions8");
		el.innerHTML = "";
	var el = document.getElementById("extraoptions9");
		el.innerHTML = "";

	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "none";
	document.getElementById("advanced3").style.display = "none";
	document.getElementById("ArchiveExamAccounts1").style.display = "";
	document.getElementById("ArchiveExamAccounts2").style.display = "";
	document.getElementById("ArchiveExamAccounts3").style.display = "";


	}
	</script>'

	echo '<script>
	function DownloadExamAccounts() {
	document.getElementById("ChangePasswords").className = "button";
	document.getElementById("ResetPasswords").className = "button";
	document.getElementById("ExamAccountAccess").className = "button";
	document.getElementById("CreateExtraExamAccounts").className = "button";
	document.getElementById("CopyData").className = "button";
	document.getElementById("CleanExamHomeAreas").className = "button";
	document.getElementById("ArchiveExamAccounts").className = "button";
	document.getElementById("DownloadExamAccounts").className = "button primary";

	var el = document.getElementById("ActionMessage");
		el.innerHTML = "'$"Download Exam Accounts"'<input type=\"hidden\" name=\"____ACTION____\" value=\"DownloadExamAccounts\">";
	var el = document.getElementById("ActionMessageHelp");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Change_Exam_Passwords\"><span class=\"icon-large-tooltip\">'$"This will create a zip or tar compressed archive of all of the exam account home areas for download."'</span></a>";
	var el = document.getElementById("extraoptions1");
		el.innerHTML = "'$"Download ZIP Archive"'";
	var el = document.getElementById("extraoptions2");
		el.innerHTML = "<input tabindex= \"1\" id=\"ZipArchive\" type=\"radio\" name=\"____ACTION2____\" value=\"ZipArchive\" checked=\"checked\"><label for=\"ZipArchive\"> </label>";
	var el = document.getElementById("extraoptions3");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Enable_.2F_Disable_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Download a Zip archive of all of the exam account home areas."'</span></a>";


	var el = document.getElementById("extraoptions4");
		el.innerHTML = "'$"Download TAR Archive"'";
	var el = document.getElementById("extraoptions5");
		el.innerHTML = "<input id=\"TarArchive\" type=\"radio\" name=\"____ACTION2____\" value=\"TarArchive\"><label for=\"TarArchive\"> </label>";
	var el = document.getElementById("extraoptions6");
		el.innerHTML = "<a class=\"info icon icon-large fa-info-circle\" target=\"_blank\" href=\"http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Enable_.2F_Disable_Exam_Accounts\"><span class=\"icon-large-tooltip\">'$"Download a Tar archive of all of the exam account home areas."'</span></a>";

	document.getElementById("advanced1").style.display = "";
	document.getElementById("advanced2").style.display = "";
	document.getElementById("advanced3").style.display = "none";
	document.getElementById("ArchiveExamAccounts1").style.display = "none";
	document.getElementById("ArchiveExamAccounts2").style.display = "none";
	document.getElementById("ArchiveExamAccounts3").style.display = "none";
	}
	</script>'


	echo '
		<button id="ChangePasswords" onclick="ChangePasswords();" class="button">
			'$"Change Passwords"'
		</button>
		</div>

		<button id="ResetPasswords" onclick="ResetPasswords();" class="button">
			'$"Reset Passwords"'
		</button>

		<button id="ExamAccountAccess" onclick="ExamAccountAccess();" class="button">
			'$"Exam Account Access"'
		</button>

		<button id="CreateExtraExamAccounts" onclick="CreateExtraExamAccounts();" class="button">
			'$"Extra Accounts"'
		</button>

		<button id="CopyData" onclick="CopyData();" class="button">
			'$"Copy Data"'
		</button>
	
		<button id="CleanExamHomeAreas" onclick="CleanExamHomeAreas();" class="button">
			'$"Clean Home Areas"'
		</button>

		<button id="ArchiveExamAccounts" onclick="ArchiveExamAccounts();" class="button">
			'$"Archive Accounts"'
		</button>

		<button id="DownloadExamAccounts" onclick="DownloadExamAccounts();" class="button">
			'$"Download Accounts"'
		</button>

		<br>
		<br>

		<form name="myform" action="/cgi-bin/admin/exam_accounts.cgi" method="post">	
			<table>
				<tbody>
					<tr>
						<td class="karoshi-input">'$"Action"'</td>
						<td id="ActionMessage"></td>
						<td id="ActionMessageHelp"></td>
					</tr>
					<tr id="advanced1" style="display:none;">
						<td><span id="extraoptions1"></span></td>
						<td><span id="extraoptions2"></span></td>
						<td><span id="extraoptions3"></span></td>
					</tr>
					<tr id="advanced2" style="display:none;">
						<td><span id="extraoptions4"></span></td>
						<td><span id="extraoptions5"></span></td>
						<td><span id="extraoptions6"></span></td>
					</tr>
					<tr id="advanced3" style="display:none;">
						<td><span id="extraoptions7"></span></td>
						<td><span id="extraoptions8"></span></td>
						<td><span id="extraoptions9"></span></td>
					</tr>
					<tr id="advanced4" style="display:none;">
						<td><span id="extraoptions10"></span></td>
						<td><span id="extraoptions11"></span></td>
						<td><span id="extraoptions12"></span></td>
					</tr>

					<tr id="ArchiveExamAccounts1" style="display:none;">
						<td class="karoshi-input">'$"Username"'</td>
						<td><div id="suggestions"></div><input tabindex= "4" class="karoshi-input" name="____USERNAME____" value="'"$USERNAME"'" size="20" type="text" id="inputString" onkeyup="lookup(this.value);"></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Archive_Exam_Accounts"><span class="icon-large-tooltip">'$"Enter in the username that will be set as the owner of the archived files."'</span></a></td>
					</tr>
					<tr id="ArchiveExamAccounts2" style="display:none;">
						<td>'$"Group"'</td><td>'
							#Show a dropdown list of all groups
							/opt/karoshi/web_controls/group_dropdown_list | sed 's/select name="_GROUP_"/select name="____GROUP____" tabindex="5"/g'
	echo '
						</td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Archive_Exam_Accounts"><span class="icon-large-tooltip">'$"If you choose a group all users in the group will have full access to the archived files."'</span></a></td>
					</tr>
					<tr id="ArchiveExamAccounts3" style="display:none;">
						<td>'$"Storage Area"'</td>
						<td>
							<select tabindex= "6" name="____SHARE____" class="karoshi-input">
								<option disabled selected value>'$"Select a storage area"'</option>'

	#Show a list of network shares to archive the accounts to
	for SERVERS in /opt/karoshi/server_network/network_shares/*
	do
		SERVER=$(basename "$SERVERS")
		SERVER_SHORT=$(echo "$SERVER" | cut -d. -f1)
		if [[ $(ls -1 /opt/karoshi/server_network/network_shares/"$SERVER" | wc -l) -gt 0 ]]
		then
			for NETSHARES in /opt/karoshi/server_network/network_shares/"$SERVER"/*
			do
				NETSHARE=$(basename "$NETSHARES")
				source /opt/karoshi/server_network/network_shares/"$SERVER"/"$NETSHARE"
				if [ "$NETSHARE" != sysvol ] && [ "$NETSHARE" != netlogon ] && [ "$NETSHARE" != applications ]
				then
					echo '
								<option value="'"$NETSHARE"'____SERVER____'"$SERVER"'____">'"$SERVER_SHORT"': '"$NETSHARE"'</option>'
				fi
			done
		fi
	done

	echo '					
							</select>
						</td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Archive_Exam_Accounts"><span class="icon-large-tooltip">'$"Choose the storage area to archive the exam accounts to."'</span></a></td>
				</tbody>
			</table>
			<input value="'$"Submit"'" class="button primary" type="submit">
			<script>
				ChangePasswords()
			</script>

		</form>
	</display-karoshicontent>
	</body>
	</html>
	'
fi
exit

