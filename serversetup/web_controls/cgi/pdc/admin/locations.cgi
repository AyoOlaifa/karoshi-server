#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Locations"
TITLEHELP=$"Locations are used to assign printers."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Locations"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/view_karoshi_web_admin_log.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
	<form action="asset_register_view.cgi" name="AssetRegister" method="post">
		<button class="button" name="AssetRegister" value="_">
			'$"Asset Register"'
		</button>

		<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="ShowPrinters" value="_">
			'$"Show Printers"'
		</button>
	</form>
	<form action="/cgi-bin/admin/locations2.cgi" method="post">
		<table>
			<tbody>
				<tr>
					<td class="karoshi-input">'$"New location"'</td>
					<td><input required="required" class="karoshi-input" name="____NEWLOCATION____" size="15" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="'"$HELPURL"'"><span class="icon-large-tooltip">'$"Enter the name of a location or room that you want to add."'</span></a></td>
				</tr>
			</tbody>
		</table>
		<input value="Submit" type="submit" class="button primary">
	</form>'

if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi
#Show current rooms
if [ "$LOCATION_COUNT" -gt 0 ]
then
	echo '
	<form action="/cgi-bin/admin/locations2.cgi" method="post">
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th class="karoshi-input"><b>'$"Locations"'</b></th>
				<th style="width: 70px;"><b>'$"Delete"'</b></th>
			</tr>
		</thead>
		<tbody>'
COUNTER=1
while [ "$COUNTER" -lt "$LOCATION_COUNT" ]
do
	LOCATION=$(sed -n "$COUNTER,$COUNTER""p" /var/lib/samba/netlogon/locations.txt)
	echo '
			<tr>
				<td>'"$LOCATION"'</td>
				<td>
					<button class="info" name="DoDelete" value="____DELETE____'"$LOCATION"'____">
						<img src="/images/submenus/client/delete_location.png" alt="'$"Delete"' '"$LOCATION"'">
						<span>'$"Delete"' '"$LOCATION"'</span>
					</button>
				</td>
			</tr>'
	let COUNTER="$COUNTER"+1
done
echo '
		</tbody>
	</table>
	</form>
	<br>'
fi
echo '
</display-karoshicontent>
</body>
</html>'
exit
