#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Configure DHCP"
TITLEHELP=$"Enter in the settings that you want to use for your client devices."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Configure_DHCP"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="dhcp_view_leases.cgi" method="post">
	<button class="button" name="_ConfigureDHCP_" value="_">
		'$"DHCP Leases"'
	</button>

	<button formaction="dhcp_reservations.cgi" class="button" name="_DoDHCPReservations_" value="_">
		'$"Reservations"'
	</button>
</form>
<form action="/cgi-bin/admin/dhcp.cgi" method="post">
'

#Get current dhcp data

if [ -f /opt/karoshi/server_network/dhcp/dhcp_settings ]
then
	source /opt/karoshi/server_network/dhcp/dhcp_settings
fi

#Domain name server - use this server ip
DOMAINNAMESERVER=$(hostname -i | cut -d" " -f1)

#Guess some useful numbers if needed
[ -z "$ROUTER" ] && ROUTER=$(grep gateway /etc/network/interfaces | sed -n 1,1p | cut -d' ' -f2)
[ -z "$SUBNETMASK" ] && SUBNETMASK=$(grep netmask /etc/network/interfaces | sed -n 1,1p | cut -d' ' -f2)
[ -z "$SUBNET" ] && SUBNET=$(ipcalc -n "$DOMAINNAMESERVER"/"$SUBNETMASK" | grep ^Network: | sed 's/ * / /g' | cut -d' ' -f2 | cut -d"/" -f1)
[ -z "$DEFAULTLEASETIME" ] && DEFAULTLEASETIME=21600
[ -z "$MAXLEASETIME" ] && MAXLEASETIME=43200

if [ -d /opt/karoshi/server_network/zones/internal/additional_domain_controllers ]
then
	#Get a list of all domain controllers in the internal zone
	for SERVER in $(ls -1 /opt/karoshi/server_network/zones/internal/additional_domain_controllers/)
	do
		DOMAINNAMESERVER="$DOMAINNAMESERVER, $(dig "$SERVER" +short)"
	done
fi

DOMAINNAMESERVER=$(echo "$DOMAINNAMESERVER" | sed 's/,,//g' | sed 's/,$//g')
NETBIOSSERVER="$DOMAINNAMESERVER"

#Check for a secondary dhcp server
SECONDARY_DHCP_SERVER=$([ -d /opt/karoshi/server_network/zones/internal/additional_domain_controllers/ ] && ls -1 /opt/karoshi/server_network/zones/internal/additional_domain_controllers/ | sed -n 1,1p)

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"DNS Servers"'</td>
			<td><input class="karoshi-input" required="required" value="'"$DOMAINNAMESERVER"'" name="_DOMAINNAMESERVER_" tabindex= "1" size="20" type="text" readonly="readonly"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Netbios Name Servers"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "1" value="'"$NETBIOSSERVER"'" name="_NETBIOSSERVER_" size="20" type="text" readonly="readonly"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Gateway"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "2" value="'"$ROUTER"'" name="_ROUTER_" size="20" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Subnet"'</td>
			<td><input class="karoshi-input" required="required" value="'"$SUBNET"'" name="_SUBNET_" size="20" tabindex= "3" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Subnet Mask"'</td>
			<td><input class="karoshi-input" required="required" value="'"$SUBNETMASK"'" name="_SUBNETMASK_" size="20" tabindex= "3" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Start Address"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "4" value="'"$STARTADDRESS"'" name="_STARTADDRESS_" size="20" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"End Address"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "5" value="'"$ENDADDRESS"'" name="_ENDADDRESS_" size="20" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Default Lease Time"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "6" value="'"$DEFAULTLEASETIME"'" name="_DEFAULTLEASETIME_" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Configure_DHCP"><span class="icon-large-tooltip">'$"Enter in the default lease time in seconds."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Max Lease Time"'</td>
			<td><input class="karoshi-input" required="required" tabindex= "7" value="'"$MAXLEASETIME"'" name="_MAXLEASETIME_" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Configure_DHCP"><span class="icon-large-tooltip">'$"Enter in the maximum lease time in seconds."'</span></a></td>
		</tr>'

#Show secondary DHCP server
if [ ! -z "$SECONDARY_DHCP_SERVER" ]
then
	echo '
		<tr>
			<td>'$"Secondary DHCP Server"'</td><td>'"$SECONDARY_DHCP_SERVER"'</td>
			<td></td>
		</tr>'
fi

echo '
	</tbody>
</table>'

if [ ! -z "$SECONDARY_DHCP_SERVER" ]
then
	echo '
<input type="hidden" name="_SECONDARYSERVER_" value="'"$SECONDARY_DHCP_SERVER"'">'
fi

echo '
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit

