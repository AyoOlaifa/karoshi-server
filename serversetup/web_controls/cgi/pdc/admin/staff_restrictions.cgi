#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Staff Restrictions"
TITLEHELP=$"Enter in the usernames of any members of staff that you do not want to be able to access the staff section of the web management."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/staff_restrictions.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
<form action="/cgi-bin/admin/remote_management_restrict.cgi" method="post">
	<button class="button" name="_RestrictAccess_" value="_">
		'$"Restrict Access"'
	</button>
</form>
<form action="/cgi-bin/admin/staff_restrictions2.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Add staff name"'</td>
				<td><input required="required" class="karoshi-input" name="_STAFFNAME_" size="25" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the usernames of any members of staff that you do not want to be able to access the staff section of the web management."'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="Submit" type="submit">
</form>
<form action="/cgi-bin/admin/staff_restrictions2.cgi" method="post">'

if [ -f /opt/karoshi/web_controls/staff_restrictions.txt ]
then
	STAFF_COUNT=$(wc -l < /opt/karoshi/web_controls/staff_restrictions.txt)
else
	STAFF_COUNT=0
fi

ICON1=/images/submenus/file/delete.png

#Show restricted staff list
if [ "$STAFF_COUNT" -gt 0 ]
then
	echo '
	<table id="myTable" class="tablesorter" style="text-align: left;" >
		<thead>
			<tr>
				<th class="karoshi-input"><b>'$"Restricted Staff"'</b></th>
				<th class="karoshi-input"><b>Remove</b></th>
			</tr>
		</thead>
		<tbody>'
	COUNTER=1
	while [ "$COUNTER" -le "$STAFF_COUNT" ]
	do
		STAFFNAME=$(sed -n $COUNTER,$COUNTER'p' /opt/karoshi/web_controls/staff_restrictions.txt)
		echo '
			<tr>
				<td>'"$STAFFNAME"'</td>
				<td>
					<button class="info" name="_DeleteUser_" value="_STAFFNAME_'$STAFFNAME'_DELETE_'$STAFFNAME'_">
						<img src="'"$ICON1"'" alt="'$"Remove"'">
						<span>'$"Remove"' '"$STAFFNAME"'</span><br>
					</button>
				</td>
			</tr>'
		let COUNTER="$COUNTER"+1
	done
	echo '
		</tbody>
	</table>'
fi

echo '
</display-karoshicontent>
</form>
</body>
</html>'
exit
