#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Import Enrollment Numbers or staff codes"
TITLEHELP=$"This allows you to import enrollment numbers or staff codes to user accounts."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Import_Enrolment_Numbers"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/bulk_user_creation_import_enrollment_numbers.cgi" METHOD="POST">

				<button class="button" formaction="bulk_user_creation_upload_fm.cgi" name="_BulkUserCreation_" value="_">
					'$"Bulk User Creation"'
				</button>

				<button class="button" formaction="bulk_user_creation_view_passwords_fm.cgi" name="_ViewNewPasswords_" value="_">
					'$"View New Passwords"'
				</button>

				<button class="button" formaction="user_image_upload_fm.cgi" name="_ImportUserImages_" value="_">
					'$"Import User Images"'
				</button>

				<button class="button" formaction="csv_set_passwords_fm.cgi" name="_SetUserPasswords_" value="_">
					'$"Set User Passwords"'
				</button>

				<br>
				<br>
		
			<table>
				<tr>
					<td class="karoshi-input">'$"CSV file"'</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35"> <a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Import_Enrolment_Numbers"><span class="icon-large-tooltip">'$"The CSV format is username, enrollment number or staff code."'</span></a></td>
				</tr>
			</table>
			<input value="'$"Submit"'" class="button primary" type="submit">
			</form>
		</display-karoshicontent>
	</body>
</html>
'
exit
