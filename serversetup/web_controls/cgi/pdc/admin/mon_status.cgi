#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Network Monitoring"
TITLEHELP=$"This will provide a monitoring server for your network."' '$"The monitor server provides visual, email, and text alerts for any devices that are offline. You can add devices to be monitored in the web management."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#System_Status"
TOOLTIPS=inforight

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Get defcon level
DEFCON=5
[ -f /opt/karoshi/server_network/mon/network_status ] && source /opt/karoshi/server_network/mon/network_status

HEIGHT=15
PADHEIGHT=6

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);

$(document).ready(function() 
    { 
        $("#myTable1").tablesorter({
	headers: {
	3: { sorter: "ipAddress" },
    		}
		});
    } 
);

</script>

<style type="text/css">

 .row { vertical-align: top; height:auto !important; display: block !important; margin-left: 0 !important; }
 .list {display:none; }
 .show {display: none; }
 .hide:target + .show {display: inline-block; }
 .hide:target {display: none; }
 .hide:target ~ .list {display:inline; }
 @media print { .hide, .show { display: none; } }
 </style>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/change_password_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi


echo '
	<form action="/cgi-bin/admin/monitors_add_fm.cgi" method="post">
		<button class="button" name="_AddMonitor_" value="_">
			'$"Add Monitor"'
		</button>

		<button formaction="/cgi-bin/admin/monitors_view.cgi" class="button" name="_ViewMonitors_" value="_">
			'$"View Monitors"'
		</button>

		<button formaction="/cgi-bin/admin/monitors_view_email_alerts_fm.cgi" class="button" name="_ViewEmailAlerts_" value="_">
			'$"E-Mail Alerts"'
		</button>
'

if [ -f /opt/karoshi/server_network/mon/activate_changes ]
then
	echo '
		<button formaction="/cgi-bin/admin/monitors_activate_changes.cgi" class="button primary" name="_NetworkStatus_" value="_">
			'$"Activate Changes"'
		</button>'
fi

echo '</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/mon_status.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/mon_status "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:"
if [ "$?" = 102 ]
then
	echo '<br><br>'$"A monitoring server has not been setup."'<br>'
fi

echo '
</display-karoshicontent>'

echo "
<script>
setTimeout(\"window.open(self.location, '_self');\", 120000);
</script>
"

echo '</body>
</html>'
exit
