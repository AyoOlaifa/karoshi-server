#!/bin/bash
#Change password
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=5
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

TITLE=$"Setup Server Monitoring"' - '"$SERVERNAME"
TITLEHELP=$"This will provide a monitoring server for your network."' '$"The monitor server provides visual, email, and text alerts for any devices that are offline. You can add devices to be monitored in the web management."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The Server choice cannot be blank."
	show_status
fi


#Check to see if this module has already been installed on the server
if [ -f /opt/karoshi/server_network/servers/"$SERVERNAME"/monitoring ]
then
	STATUSMSG=$"This module has already been set up on this server."
fi

echo '
<form action="/cgi-bin/admin/module_monitoring.cgi" method="post">
<input name="_SERVERNAME_" value="'"$SERVERNAME"'" type="hidden">
<b>'$"Description"'</b><br><br>
'$"This will provide a monitoring server for your network."' '$"The monitor server provides visual, email, and text alerts for any devices that are offline. You can add devices to be monitored in the Web Management."''

if [ ! -z "$STATUSMSG" ]
then
	echo ''"$STATUSMSG"'<br><br>'
fi

echo '<br><br>
<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
