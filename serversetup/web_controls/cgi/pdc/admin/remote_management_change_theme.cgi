#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Change Theme"
TITLEHELP=$"Choose the theme that you want for the Web Management."'<br><br>'$"This will not affect other web management users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Web_Management_Themes"

#Get current stylesheet.
CurrentStyleSheet=$(echo "$STYLESHEET" | cut -d. -f1)
TITLE="$TITLE $CurrentStyleSheet"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/remote_management_change_theme2.cgi" method="post">'

#Show link to change global theme if this is an admin user.
if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) -gt 0 ]]
then
	echo '
	<button formaction="/cgi-bin/admin/remote_management_change_global_theme.cgi" class="button" name="_ChangeGlobalTheme_" value="_">
		'$"Change Global Theme"'
	</button>
	<br>
	<br>
'
fi

echo '
<table>
	<tbody>
		<tr>'

StyleCount=1

if [ "$MOBILE" = yes ]
then
	MaxStyles=2
else
	MaxStyles=3
fi

for Theme in $(ls -1 /var/www/html_karoshi/responsive/assets/css/*.css | cut -d"/" -f8 | sed 's/.css//g' | sed 's/defaultstyle//g' | sed 's/main//g' | sed 's/bluegrey//g' | sed 's/greyblue//g' | sed 's/bronze//g' | sed 's/default//g' | sed 's/font-awesome.min//g' | sed '/^$/d')
do

	StyleSheetPreview=$(basename "$Theme")
	if [ "$StyleSheetPreview" != "$CurrentStyleSheet" ]
	then
		ButtonClass="button"
	else
		ButtonClass="button primary"
	fi
	echo '
		<td>
			<button class="'"$ButtonClass"'" name="_SetTheme_" value="_THEMECHOICE_'"$StyleSheetPreview"'_">
				'"$StyleSheetPreview"'
			</button>
		</td>'
	let StyleCount="$StyleCount"+1
	if [ "$StyleCount" -gt "$MaxStyles" ]
	then
		echo '
		</tr><tr>'
		StyleCount=1
	fi
done

[ "$StyleCount" = 1 ] && echo "
			<td></td>"
echo '
		</tr>
	</tbody>
</table>
<br>'

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
