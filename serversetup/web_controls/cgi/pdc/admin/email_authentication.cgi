#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Authentication"
TITLEHELP=$"By default all E-Mail clients have to authenticate to be able to send E-Mails."'<br><br>'$"Here you can allow TCPIP addresses that you want to be able to send E-Mail without having to authenticate."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Authentication"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	0: { sorter: "ipAddress" }
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%+' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=16
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = delete ] || [ "$ACTION" = reallydelete ] || [ "$ACTION" = reallyadd ]
then
	#Assign TCPIP
	DATANAME=TCPIP
	get_data
	TCPIP="$DATAENTRY"
fi

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = delete ]
then
	#Assign COMMENT
	DATANAME=COMMENT
	get_data
	COMMENT="$DATAENTRY"
fi


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_authentication.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallydelete ]
then
	#Check to see that TCPIP is not blank
	if [ -z "$TCPIP" ]
	then
		MESSAGE=$"You have not entered a TCPIP address."
		show_status
	fi
fi

if [ "$ACTION" = reallyadd ]
then
	#Check to see that COMMENT is not blank
	if [ -z "$COMMENT" ]
	then
		MESSAGE=$"You have not entered a comment for the TCPIP address."
		show_status
	fi

	#Check that we have some sort of useful ip address.
	if [[ $(ipcalc -n "$TCPIP" | grep -c INVALID) -gt 0 ]]
	then
		MESSAGE=$"The TCPIP address is incorrect."
		show_status	
	fi
fi

if [ "$ACTION" = add ] || [ "$ACTION" = delete ]
then
	ACTION2=view
	BUTTONTXT=$"View"
	TITLETXT=$"Add TCPIP Address"
	[ "$ACTION" = delete ] && TITLETXT=$"Delete TCPIP Address"
fi

if [ "$ACTION" = view ] || [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallydelete ]
then
	ACTION2=add
	BUTTONTXT=$"Add"
	TITLETXT=$"Bypass E-Mail Authentication"
fi

echo '
<form action="/cgi-bin/admin/email_authentication.cgi" method="post">
	<button class="button" name="____DoAction____" value="____ACTION____'"$ACTION2"'____">
		'"$BUTTONTXT"'
	</button>
</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_authentication.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$TCPIP:$COMMENT:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/email_authentication
EXIT_STATUS="$?"

if [ "$EXIT_STATUS" = 102 ]
then
	MESSAGE=$"A folder with this name already exists."
	show_status	
fi

if [ "$EXIT_STATUS" = 103 ]
then
	MESSAGE=$"This share already exits in samba."
	show_status	
fi

echo '
</display-karoshicontent>
</body>
</html>'
exit
