#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Join a client to the domain"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
<br>
<b>'$"Windows Clients"'</b>
<br>
<br>
'$"Windows client computers are joined to the domain in the same way as for a windows network. "'
<br>
'$"The username required to do this is Administrator. The password will be the system password that you chose earlier."'<br>'$"You can also join client computers to the domain using any accounts that are members of the itadmin group."'
<br>
<br>
'$"Once you have successfully joined a client you can test it with the new user that you have just created."'
<br>
<br>
<b>'$"Karoshi Linux Clients"'</b>
<br>
<br>
'$"Karoshi linux clients can be joined to the domain using the icons provided on the desktop for the local administrator user account on the linux client."'
</display-karoshicontent>
</body>
</html>'
exit
