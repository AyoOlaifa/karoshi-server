#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk
#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add a New User"
TITLEHELP=$"Add new users to your system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User"

source /opt/karoshi/server_network/security/password_settings
#Force users to change password on first logon if it has been set.
[ -z "$CHANGEPASSFIRSTLOGIN" ] && CHANGEPASSFIRSTLOGIN=no
if [ "$CHANGEPASSFIRSTLOGIN" = yes ]
then
	NEXTLOGON1=selected
	NEXTLOGON2=""
else
	NEXTLOGON2=selected
	NEXTLOGON1=""
fi

#Delete the existing add user image if it has been uploaded but not used
[ -d "/var/www/karoshi/add_user_image/$REMOTE_USER" ] && rm -f -R "/var/www/karoshi/add_user_image/$REMOTE_USER"

#Get install type
INSTALL_TYPE=education
if [ -f /opt/karoshi/server_network/install_type ]
then
	INSTALL_TYPE=$(sed -n 1,1p /opt/karoshi/server_network/install_type)
fi

if [ -f /opt/karoshi/server_network/default_username_style ]
then
	source /opt/karoshi/server_network/default_username_style
	[ "$DEFAULTSTYLE" = 1 ] && SELECT1='selected="selected"'
	[ "$DEFAULTSTYLE" = 2 ] && SELECT2='selected="selected"'
	[ "$DEFAULTSTYLE" = 3 ] && SELECT3='selected="selected"'
	[ "$DEFAULTSTYLE" = 4 ] && SELECT4='selected="selected"'
	[ "$DEFAULTSTYLE" = 5 ] && SELECT5='selected="selected"'
	[ "$DEFAULTSTYLE" = 6 ] && SELECT6='selected="selected"'
	[ "$DEFAULTSTYLE" = 7 ] && SELECT7='selected="selected"'
	[ "$DEFAULTSTYLE" = 8 ] && SELECT8='selected="selected"'
	[ "$DEFAULTSTYLE" = 9 ] && SELECT9='selected="selected"'
	[ "$DEFAULTSTYLE" = 10 ] && SELECT10='selected="selected"'
else
	DEFAULTSTYLE=1
	SELECT1='selected="selected"'
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
FILE=$(echo "$DATA" | cut -s -d_ -f5)
ROWCOUNT=7

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form name="myform" action="/cgi-bin/admin/add_user.cgi" method="post">'

#Check that this server is not part of a federated setup
if [ -f /opt/karoshi/server_network/servers/"$HOSTNAME"/federated_server ]
then
	echo $"This server is part of a federated system. Users must be created on the main federation server." '</div></div></body></html>'
	exit
fi

#Get request data if asked
if [ ! -z "$FILE" ]
then
	if [ -f "/opt/karoshi/user_requests/new_users/$FILE" ]
	then
		NEW_USER_DATA=$(sed -n 1,1p "/opt/karoshi/user_requests/new_users/$FILE")
		FORENAME=$(echo "$NEW_USER_DATA" | cut -d: -f1)
		SURNAME=$(echo "$NEW_USER_DATA" | cut -d: -f2)
		GROUP=$(echo "$NEW_USER_DATA" | cut -d: -f3)
		ENROLLMENTNUMBER=$(echo "$NEW_USER_DATA" | cut -d: -f4)
		echo '<input name="____REQUESTFILE____" value="'"$FILE"'" type="hidden">'
	fi
fi

#Custom scripts
echo '
<script>
function rewriteselect() {
document.myform.____USERNAMESTYLE____.options.selectedIndex.length = 0;
var firstnameValue = (document.myform.____FIRSTNAME____.value).toLowerCase();
var surnameValue = (document.myform.____SURNAME____.value).toLowerCase();
var enrollmentValue = document.myform.____ENROLLMENTNUMBER____.value;
var yearValue = document.myform.____GROUP____.value;
var selectedstyle = document.myform.____USERNAMESTYLE____.value
var status1 = false;
var status2 = false;
var status3 = false;
var status4 = false;
var status5 = false;
var status6 = false;
var status7 = false;
var status8 = false;
var status9 = false;
var status10 = false;

var el = document.getElementById("extraoptions1");
el.innerHTML = "";

var el = document.getElementById("extraoptions2");
el.innerHTML = "";

if (selectedstyle == "userstyleS2") {
alert("style2");
	status2 = "true";
}

if (selectedstyle == "userstyleS3") {
	status3 = "true";
}

if (selectedstyle == "userstyleS4") {
	status4 = "true";
}

if (selectedstyle == "userstyleS5") {
	status5 = "true";
}

if (selectedstyle == "userstyleS6") {
	status6 = "true";
}

if (selectedstyle == "userstyleS7") {
	status7 = "true";
}

if (selectedstyle == "userstyleS8") {
	status8 = "true";
}

if (selectedstyle == "userstyleS9") {
	status9 = "true";
}

if (selectedstyle == "userstyleS10") {
	var el = document.getElementById("extraoptions1");
el.innerHTML = "Username";

var el = document.getElementById("extraoptions2");


el.innerHTML = "<input tabindex= \"6\" value=\"'"$USERNAME"'\" name=\"____USERNAME____\" class=\"karoshi-input\" size=\"20\" type=\"text\">";

usernameValue = "'$"Enter a username"'";
	status10 = "true";
}

if (yearValue.indexOf("yr") != -1) {
	yearValue = yearValue[4]+yearValue[5];
} else {
	yearValue = "";
}

if (firstnameValue == "") {
	firstnameValue = "arnold";
}

if (surnameValue == "") {
	surnameValue = "user";
}

if (enrollmentValue == "") {
	enrollmentValue = "Enrollment number";
}

'

if [ "$INSTALL_TYPE" = education ]
then
	echo 'document.myform.____USERNAMESTYLE____.options[0]=new Option("" + firstnameValue[0] + surnameValue + yearValue, "userstyleS1", false, status1);
document.myform.____USERNAMESTYLE____.options[1]=new Option("" + yearValue + firstnameValue[0] + surnameValue, "userstyleS2", false, status2);
document.myform.____USERNAMESTYLE____.options[2]=new Option("" + surnameValue + firstnameValue[0] + yearValue, "userstyleS3", false, status3);
document.myform.____USERNAMESTYLE____.options[3]=new Option("" + firstnameValue + "." + surnameValue + yearValue, "userstyleS4", false, status4);
document.myform.____USERNAMESTYLE____.options[4]=new Option("" + surnameValue + "." + firstnameValue + yearValue, "userstyleS5", false, status5);
document.myform.____USERNAMESTYLE____.options[5]=new Option("" + yearValue + surnameValue + firstnameValue[0], "userstyleS6", false, status6);
document.myform.____USERNAMESTYLE____.options[6]=new Option("" + yearValue + firstnameValue + surnameValue[0], "userstyleS7", false, status7);
document.myform.____USERNAMESTYLE____.options[7]=new Option("" + firstnameValue + surnameValue[0], "userstyleS8", false, status8);
document.myform.____USERNAMESTYLE____.options[8]=new Option("" + enrollmentValue, "userstyleS9", false, status9);
document.myform.____USERNAMESTYLE____.options[9]=new Option("" + usernameValue, "userstyleS10", false, status10);'
else
	echo 'document.myform.____USERNAMESTYLE____.options[0]=new Option("" + firstnameValue[0] + surnameValue + yearValue, "userstyleS1", false, status1);
document.myform.____USERNAMESTYLE____.options[1]=new Option("" + surnameValue + firstnameValue[0] + yearValue, "userstyleS3", false, status3);
document.myform.____USERNAMESTYLE____.options[2]=new Option("" + firstnameValue + "." + surnameValue + yearValue, "userstyleS4", false, status4);
document.myform.____USERNAMESTYLE____.options[3]=new Option("" + surnameValue + "." + firstnameValue + yearValue, "userstyleS5", false, status5);
document.myform.____USERNAMESTYLE____.options[4]=new Option("" + firstnameValue + surnameValue[0], "userstyleS8", false, status8);
document.myform.____USERNAMESTYLE____.options[5]=new Option("" + usernameValue, "userstyleS10", false, status10);'

fi
echo '}
</script>
'

echo '
<script>
function showRow(rowId) {
for (var i = 1; i <= '"$ROWCOUNT"'; i++) {
	document.getElementById(rowId+i).style.display = "";
    }
}

function hideRow(rowId) {
for (var i = 1; i <= '"$ROWCOUNT"'; i++) {
	document.getElementById(rowId+i).style.display = "none";
    }
}

function hideLink(linkId) {
    document.getElementById(linkId).style.display = "none";
}

function showLink(linkId) {
    document.getElementById(linkId).style.display = "";
}
</script>

<button type="button" id="ToggleAdvanced" class="button-no-border icon icon-large fa-chevron-circle-down" onclick="showRow('\''advanced'\''); showLink('\'ToggleBasic\''); hideLink('\'ToggleAdvanced\'');"></button>
<button style="display: none;" type="button" id="ToggleBasic" class="button-no-border icon icon-large fa-chevron-circle-up" onclick="hideRow('\''advanced'\''); showLink('\'ToggleAdvanced\''); hideLink('\'ToggleBasic\'');"></button>

<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Forename"'</td>
			<td><input required="required" tabindex= "1" value="'"$FORENAME"'" name="____FIRSTNAME____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User"><span class="icon-large-tooltip">'$"Enter the firstname for this user."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Surname"'</td>
			<td><input required="required" tabindex= "2" value="'"$SURNAME"'" name="____SURNAME____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"Enter the surname for this user."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Password"'</td>
			<td><input required="required" pattern=".{'"$MINPASSLENGTH"',128}" title="'$"Password length required:"' '"$MINPASSLENGTH"'" tabindex= "3" name="____PASSWORD1____" class="karoshi-input" size="20" type="password"></td>
			<td>
				<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"Enter a password and confirm it in the box below."'<br><br>'$"The following special characters are allowed"'<br><br> space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~	~<br><br>'

[ "$PASSWORDCOMPLEXITY" = on ] && echo ''$"Upper and lower case characters and numbers are required."'<br><br>'

echo ''$"The Minimum password length is "''"$MINPASSLENGTH"'.<br></span></a>
			</td>
		</tr>
		<tr>
			<td>'$"Confirm Password"'</td>
			<td><input required="required" pattern=".{'"$MINPASSLENGTH"',128}" title="'$"Password length required:"' '"$MINPASSLENGTH"'" tabindex= "4" name="____PASSWORD2____" class="karoshi-input" size="20" type="password"></td>
			<td></td>
		</tr>
		<tr id="advanced1" style="display:none;">
			<td>'$"User code"'</td>
			<td><input tabindex= "5" value="'"$ENROLLMENTNUMBER"'" name="____ENROLLMENTNUMBER____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"Student enrolment number or staff code. This field can be left blank."'</span></a></td>
		</tr>
		<tr id="advanced2" style="display:none;">
			<td>'$"Room Number"'</td>
			<td><input tabindex= "6" value="'"$ROOMNUMBER"'" name="____ROOMNUMBER____" class="karoshi-input" size="20" type="text"></td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"The room number for the user. This field can be left blank."'</span></a></td>
		</tr>
		<tr id="advanced3" style="display:none;">
			<td>'$"Telephone Number"'</td>
			<td><input tabindex= "7" value="'"$TELEPHONENUMBER"'" name="____TELEPHONENUMBER____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"The telephone number for the user. This field can be left blank."'</span></a></td>
		</tr>
		<tr id="advanced4" style="display:none;">
			<td>'$"Fax Number"'</td>
			<td><input tabindex= "8" value="'"$FAXNUMBER"'" name="____FAXNUMBER____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"The fax number for the user. This field can be left blank."'</span></a></td>
		</tr>
		<tr id="advanced5" style="display:none;">
			<td>'$"Mobile Number"'</td>
			<td><input tabindex= "9" value="'"$MOBILENUMBER"'" name="____MOBILENUMBER____" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"The mobile number for the user. This field can be left blank."'</span></a></td>
		</tr>
		<tr id="advanced6" style="display:none;">
			<td>'$"Change at next logon"'</td>
			<td>
				<select  tabindex= "10" name="____NEXTLOGON____" class="karoshi-input">
					<option value="y" '"$NEXTLOGON1"'>'$"Yes"'</option>
					<option value="n" '"$NEXTLOGON2"'>'$"No"'</option>
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User"><span class="icon-large-tooltip">'$"This will force the user to change their password at next logon."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Primary group"'</td>
			<td>'

if [ -z "$FILE" ]
then
	/opt/karoshi/web_controls/group_dropdown_list | sed 's/<select name="_GROUP_"/<select tabindex="11" name="____GROUP____"/g'| sed 's/class="karoshi-input">/class="karoshi-input" onClick="rewriteselect();">/g'
else
	/opt/karoshi/web_controls/group_dropdown_list | sed 's/<select name="_GROUP_"/<select tabindex="12" name="____GROUP____"/g' | sed 's/<option><\/option>/<option selected="selected">'"$GROUP"'<\/option>/g'
fi
echo '
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"The groups give different levels of access."' '$"The itadmin group is for the network administrator."' '$"Only members of itadmin and the tech groups gain administrator access to windows computers joined to the domain."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Username style"'</td>
			<td>
				<select  tabindex= "13" name="____USERNAMESTYLE____" class="karoshi-input" onClick="rewriteselect();">
					<option value="userstyleS1" '"$SELECT1"'>'$"auser09"'</option>'
[ "$INSTALL_TYPE" = education ] && echo '
					<option value="userstyleS2" '"$SELECT2"'>'$"09auser"'</option>'
echo '
					<option value="userstyleS3" '"$SELECT3"'>'$"usera09"'</option>
					<option value="userstyleS4" '"$SELECT4"'>'$"arnold.user09"'</option>
					<option value="userstyleS5" '"$SELECT5"'>'$"user.arnold09"'</option>'
[ "$INSTALL_TYPE" = education ] && echo '
					<option value="userstyleS6" '"$SELECT6"'>'$"09usera"'</option>'
[ "$INSTALL_TYPE" = education ] && echo '
					<option value="userstyleS7" '"$SELECT7"'>'$"09arnoldu"'</option>'
echo '
				<option value="userstyleS8" '"$SELECT8"'>'$"arnoldu"'</option>'
[ "$INSTALL_TYPE" = education ] && echo '
					<option value="userstyleS9" '"$SELECT9"'>'$"Enrollment number as username"'</option>'
echo '
					<option value="userstyleS10" '"$SELECT10"'>'$"Enter a username"'</option>
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Username_Styles"><span class="icon-large-tooltip">'$"Choose the username style you require."'</span></a></td>
		</tr>
		<tr>
			<td><span id="extraoptions1"></span></td>
			<td><span id="extraoptions2"></span></td>
			<td></td>
		</tr>

		<tr id="advanced7" style="display:none; vertical-align:top;">
			<td>'$"User Photo"'</td>
			<td>
				<iframe src="/cgi-bin/admin/add_user_upload_image_fm.cgi" width="135" height="165" scrolling="no" style="overflow:hidden; border: none;"></iframe>
			</td>
			<td style="vertical-align:top"><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Detailed_Explanation"><span class="icon-large-tooltip">'$"Click on the image to upload a JPG image for this user."'<br><br>'$"User images default to width 120px height 150px."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit

