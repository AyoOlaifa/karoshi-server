#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Firewall Rules"
TITLEHELP=$"Firewall Rules"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Firewall_Rules"
TOOLTIPS=inforight


#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

DATA=$(cat | tr -cd 'A-Za-z0-9\._:&%\-+' | sed 's/___/TRIPLEUNDERSCORE/g' | sed 's/_/UNDERSCORE/g' | sed 's/TRIPLEUNDERSCORE/_/g')

#########################
#Assign data to variables
#########################
END_POINT=22
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

if [[ $(ls -1 /opt/karoshi/server_network/servers/ | wc -l) = 1 ]]
then
	SERVERNAME=$(hostname-fqdn)
	SERVERTYPE=network
	SERVERMASTER=notset
else

	#Assign SERVERNAME
	DATANAME=SERVERNAME
	get_data
	SERVERNAME="$DATAENTRY"

	#Assign SERVERTYPE
	DATANAME=SERVERTYPE
	get_data
	SERVERTYPE="$DATAENTRY"
fi

if [ ! -z "$SERVERNAME" ]
then
	ShortServerName=$(echo "$SERVERNAME" | cut -d. -f1)
	TITLE="$TITLE - $ShortServerName"
fi

#Generate page layout later than normal so that we have caputured the servername for display at the top.
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	4: { sorter: false}
    		}
		});
    } 
);
</script>
'

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign RULESET
DATANAME=RULESET
get_data
RULESET=$(echo "$DATAENTRY" | sed 's/+/-/g' | sed 's/UNDERSCORE/_/g')

if [ -z "$ACTION" ]
then
	ACTION=view
fi

if [ -z "$SERVERNAME" ]
then
	SERVERNAME=notset
else
	if [ "$MOBILE" = yes ]
	then
		SERVERNAME2=$(echo "${SERVERNAME:0:9}" | cut -d. -f1)
	else
		SERVERNAME2=$(echo "$SERVERNAME" | cut -d. -f1)
	fi
fi

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyedit ]
then

	#Assign FIREWALLACTION
	DATANAME=FIREWALLACTION
	get_data
	FIREWALLACTION="$DATAENTRY"

	#Assign PORTS
	DATANAME=PORTS
	get_data
	PORTS=$(echo "$DATAENTRY" | sed 's/%3A/-/g' | sed 's/%2C/,/g' | tr -cd "0-9\-," | sed 's/,,/,/g')

	#Assign PROTOCOL
	DATANAME=PROTOCOL
	get_data
	PROTOCOL=$(echo "$DATAENTRY" | sed 's/%3A/-/g' | sed 's/%2C/,/g')

	#Assign TCPIP
	DATANAME=TCPIP
	get_data
	TCPIP="$DATAENTRY"
fi

function show_status {
echo '<script>
alert("'"$MESSAGE"'");
window.location = "/cgi-bin/admin/firewall.cgi";
</script></div></body></html>'
exit
}

if [ "$ACTION" = edit ] || [ "$ACTION" = delete ]
then
	if [ -z "$RULESET" ]
	then
		MESSAGE=$"The ruleset cannot be blank."
		show_status
	fi
fi

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyedit ]
then

	if [ -z "$RULESET" ]
	then
		MESSAGE=$"You have not entered the name of the ruleset."
		show_status
	fi	

	if [ -z "$PORTS" ]
	then
		MESSAGE=$"You have not entered in any ports."
		show_status
	fi
	#Check to see that valid ports have been entered.

	for PORT in $(echo "$PORTS" | sed 's/,/ /g' | sed 's/-/ /g' )
	do
		#Check that the port is not blank
		if [ -z "$PORT" ]
		then
			MESSAGE=$"You have entered in a blank port."
			show_status
		fi 
		#Check that the port is within range
		if [ "$PORT" -gt 65535 ]
		then
			MESSAGE=$"You have entered in a port that is larger than the maximum port number."
			show_status
		fi
		if [ "$PORT" -le 0 ]
		then
			MESSAGE=$"You cannot have zero as a port number."
			show_status
		fi
	done
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


if [ "$SERVERNAME" != notset ]
then
	echo '
		<form action="/cgi-bin/admin/firewall.cgi" method="post">
			<button class="button" name="SelectServer" value="_">
				'$"Select Server"'
			</button>
	'

	if [ "$ACTION" = view ] || [ "$ACTION" = reallydelete ] || [ "$ACTION" = reallyedit ] || [ "$ACTION" = reallyadd ] && [ "$SERVERNAME" != notset ]
	then
		echo '
				<button formaction="/cgi-bin/admin/firewall.cgi" class="button" name="___AddRule___" value="___ACTION___add___SERVERTYPE___'"$SERVERTYPE"'___SERVERMASTER___'"$SERVERMASTER"'___SERVERNAME___'"$SERVERNAME"'___">
					'$"Add Rule"'
				</button>'
	fi

	if [ "$ACTION" = add ] || [ "$ACTION" = edit ] || [ "$ACTION" = delete ]
	then
		echo '
				<button formaction="/cgi-bin/admin/firewall.cgi" class="button" name="___ViewRules___" value="___ACTION___view___SERVERTYPE___'"$SERVERTYPE"'___SERVERMASTER___'"$SERVERMASTER"'___SERVERNAME___'"$SERVERNAME"'___">
					'$"View Rules"'
				</button>'
	fi

	echo '</form>'
fi

echo '<form id="FirewallData" action="/cgi-bin/admin/firewall.cgi" method="post">'

if [ "$SERVERNAME" = notset ]
then
	#Show list of servers
	/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"View Rules" view "" "___"
else
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/firewall.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$SERVERNAME:$SERVERTYPE:$SERVERMASTER:$ACTION:$RULESET:$FIREWALLACTION:$PROTOCOL:$PORTS:$TCPIP:" | sudo -H /opt/karoshi/web_controls/exec/firewall
fi

echo '</form></display-karoshicontent></body></html>'
exit
