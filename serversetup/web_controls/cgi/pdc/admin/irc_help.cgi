#!/bin/bash
#Change password
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"IRC"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin
source /opt/karoshi/server_network/domain_information/domain_name
REALM2=$(echo $REALM | cut -d. -f1)
REALM2="${REALM2:0:8}"

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<iframe src="https://webchat.freenode.net?nick='$REALM2'_'$REMOTE_USER'&channels=%23karoshi&prompt=1&uio=MTE9MzY5cf" width="100%" height="650px" frameBorder="0">
</iframe>
</display-karoshicontent>
</body>
</html>'
exit

