#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Monitorix System Monitor"
TITLEHELP=$"Monitorix is a system monitoring tool."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=System_Monitoring"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/monitorix.cgi" name="selectservers" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Monitor Interval"'</td>
			<td style="vertical-align: top; text-align: left;">
				<select class="karoshi-input" name="____INTERVAL____">
					<option value="D">'$"Daily"'</option>
					<option value="W">'$"Weekly"'</option>
					<option value="M">'$"Monthly"'</option>
					<option value="Y">'$"Yearly"'</option>
				</select>
			</td>
		</tr>
	</tbody>
</table>
<br>
'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Show statistics" no no ____

echo '
</display-karoshicontent>
</form>
</body>
</html>'
exit
