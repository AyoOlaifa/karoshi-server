#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

TIMEOUT=300
STYLESHEET=defaultstyle.css
if [ -f /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER" ]
then
	source /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER"	
fi
export TEXTDOMAIN=karoshi-server

TITLE=$"View Event Logs"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

DATA=$(echo "$QUERY_STRING" | tr -cd 'A-Za-z0-9\_.-/-' | sed 's/\.\.\///g')

#If we have not received any data via get then try and get it from post
if [ -z "$DATA" ]
then
	DATA=$(cat | tr -cd 'A-Za-z0-9\_.-/-%' | sed 's/\.\.\///g')
fi

#########################
#Assign data to variables
#########################
END_POINT=45
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

if [[ $(ls -1 /opt/karoshi/server_network/servers/ | wc -l) = 1 ]]
then
	SERVERNAME=$(hostname-fqdn)
	#SERVERTYPE=network
	#SERVERMASTER=notset
else
	#Assign SERVERNAME
	DATANAME=SERVERNAME
	get_data
	SERVERNAME="$DATAENTRY"
fi

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign LOGFILE
DATANAME=LOGFILE
get_data
LOGFILE="$DATAENTRY"

#Assign TAIL_LENGTH
DATANAME=TAIL_LENGTH
get_data
TAIL_LENGTH="$DATAENTRY"

if [ ! -z "$SERVERNAME" ]
then
	ShortServerName=$(echo "$SERVERNAME" | cut -d. -f1)
	TITLE="$TITLE - $ShortServerName"
fi

#Generate page layout later than normal so that we have caputured the servername for display at the top.
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

function show_status {
echo '<script>
alert("'"$MESSAGE"'");
</script></body></html>'
exit
}

#Check data

if [ -z "$SERVERNAME" ]
then
	SERVERNAME=viewservers
	SERVERNAME2=""
else
	SERVERNAME2="$SERVERNAME"
fi

if [ -z "$LOGFILE" ]
then
	LOGFILE=viewlist
	LOGFILE2=""
	EVENT=""
else
	LOGFILE2="${LOGFILE//%3A/:}"
		
	#Get time, date and category of the event
	CATEGORY=$(echo "$LOGFILE2" | cut -d"-" -f1)

	#case "$CATEGORY" in
	#	syslog)
	#	    CATEGORY2=$"System Log"
	#	    ;;
	#	 
	#	backup)
	#	    CATEGORY2=$"Backup Log"
	#	    ;;
	#	 
	#	restore)
	#	     CATEGORY2=$"Restore Log"
	#	    ;;
	#esac
	DAY=$(echo "$LOGFILE2" | cut -d"-" -f4)
	MONTH=$(echo "$LOGFILE2" | cut -d"-" -f3)
	YEAR=$(echo "$LOGFILE2" | cut -d"-" -f2)
	TIME=$(echo "$LOGFILE2" | cut -d"-" -f5)
	EVENT="$DAY-$MONTH-$YEAR $TIME"
fi

if [ "$ACTION" != viewevent ]
then
	EVENT=""
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

if [ "$SERVERNAME" = viewservers ]
then
	#Show list of servers
	echo '<form action="/cgi-bin/admin/view_logs.cgi" method="post">'
	/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"View Event Logs" viewlist
	echo '</form>'
else
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/view_logs.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$SERVERNAME:$ACTION:$LOGFILE:$TAIL_LENGTH:" | sudo -H /opt/karoshi/web_controls/exec/view_logs
fi

echo '</display-karoshicontent></body></html>'


exit

