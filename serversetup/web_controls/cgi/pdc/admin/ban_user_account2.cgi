#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Ban User Account"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%/+-')
#########################
#Assign data to variables
#########################
END_POINT=18
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign HOUR
DATANAME=HOUR
get_data
HOUR="$DATAENTRY"

#Assign MINUTES
DATANAME=MINUTES
get_data
MINUTES="$DATAENTRY"

#Assign DAY
DATANAME=DAY
get_data
DAY="$DATAENTRY"

#Assign MONTH
DATANAME=MONTH
get_data
MONTH="$DATAENTRY"

#Assign YEAR
DATANAME=YEAR
get_data
YEAR="$DATAENTRY"

#Assign INCIDENT
DATANAME=INCIDENT
get_data
INCIDENT="$DATAENTRY"

#Assign ACTIONTAKEN
DATANAME=ACTIONTAKEN
get_data
ACTIONTAKEN="$DATAENTRY"

#Assign STUDENTS
DATANAME=STUDENTS
get_data
STUDENTS="$DATAENTRY"

#Assign BANLENGTH
DATANAME=BANLENGTH
get_data
BANLENGTH="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/ban_user_account.cgi";
</script>
</body></html>'
exit
}

function input_error {

echo '<b>'$MESSAGE'</b><br>'
echo '<form action="/cgi-bin/admin/ban_user_account.cgi" method="post">'
echo '<input type="hidden" id="_DAY_" name="_DAY_" value="'$DAY'" />'
echo '<input type="hidden" id="_MONTH_" name="_MONTH_" value="'$MONTH'" />'
echo '<input type="hidden" id="_YEAR_" name="_YEAR_" value="'$YEAR'" />'
echo '<input type="hidden" id="_HOUR_" name="_HOUR_" value="'$HOUR'" />'
echo '<input type="hidden" id="_MINUTES_" name="_MINUTES_" value="'$MINUTES'" />'
echo '<input type="hidden" id="_STUDENTS_" name="_STUDENTS_" value="'$STUDENTS'" />'
echo '<input type="hidden" id="_INCIDENT_" name="_INCIDENT_" value="'$INCIDENT'" />'
echo '<input type="hidden" id="_ACTIONTAKEN_" name="_ACTIONTAKEN_" value="'$"User account banned."'" />'
echo '<input type="hidden" id="_BANLENGTH_" name="_BANLENGTH_" value="'$BANLENGTH'" />'
echo '<input value='$Back' type="submit"></div></form></div></body></html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [[ ! -f /opt/karoshi/web_controls/web_access_admin ]] || [ -z "$REMOTE_USER" ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that HOUR is not blank
if [ -z "$HOUR" ]
then
	MESSAGE=$"The time must not be blank."
	input_error
fi
#Check to see that MINUTES is not blank
if [ -z "$MINUTES" ]
then
	MESSAGE=$"The time must not be blank."
	input_error
fi
#Check to see that DAY is not blank
if [ -z "$DAY" ]
then
	MESSAGE=$"The date must not be blank."
	input_error
fi
#Check to see that MONTH is not blank
if [ -z "$MONTH" ]
then
	MESSAGE=$"The date must not be blank."
	input_error
fi
#Check to see that YEAR is not blank
if [ -z "$YEAR" ]
then
	MESSAGE=$"The date must not be blank."
	input_error
fi
#Check to see that INCIDENT is not blank
if [ -z "$INCIDENT" ]
then
	MESSAGE=$"The incident must not be blank."
	input_error
fi
#Check to see that ACTIONTAKEN is not blank
if [ "$ACTIONTAKEN" = null ]
then
	MESSAGE=$"The action taken must not be blank."
	input_error
fi
#Check to see that STUDENTS is not blank
if [ -z "$STUDENTS" ]
then
	MESSAGE=$"You have not entered any students."
	input_error
fi
#Check that HOUR has two digits
if [ "${#HOUR}" != 2 ]
then
	MESSAGE=$"The hour must have two digits."
	HOUR="??"
	input_error
fi
#Check that MINUTES has two digits
if [ "${#MINUTES}" != 2 ]
then
	MESSAGE=$"The minutes must have two digits."
	MINUTES=""
	input_error
fi
#Check that day has two digits
if [ "${#DAY}" != 2 ]
then
	MESSAGE=$"The day must have two digits."
	DAY="??"
	input_error
fi

#Check that month has two digits
if [ "${#MONTH}" != 2 ]
then
	MESSAGE=$"The month must have two digits."
	MONTH="??"
	input_error
fi
#Check that year has four digits
if [ "${#YEAR}" != 4 ]
then
	MESSAGE=$"The year must have four digits."
	YEAR="????"
	input_error
fi
#Check that ban length is a number
if [ ! -z "$BANLENGTH" ]
then
	BANLENGTH=$(echo "$BANLENGTH" | tr -cd '0-9\n')
	if [ -z "$BANLENGTH" ]
	then
		MESSAGE=$"The duration of the user ban must be a number."
		BANLENGTH="??"
		input_error
	fi
	#Check that ban length does not start with zero
	if [ "${BANLENGTH:0:1}" = 0 ]
	then
		BANLENGTH="${BANLENGTH:1}"
	fi
fi

#Check that date and time does not have question marks in it!
if [[ $(echo $DAY$MONTH$YEAR$HOUR$MINUTES | grep -c ?) != 0 ]]
then
	MESSAGE=$"Please correct the date error."
	input_error
fi
#Check that student usernames exist
STUDENT_ARRAY=( $(echo "$STUDENTS" | sed 's/+/ /g') )
STUDENT_ARRAY_COUNT="${#STUDENT_ARRAY[@]}"
COUNTER=0
while [ $COUNTER -lt $STUDENT_ARRAY_COUNT ]
do
	STUDENT_USERNAME="${STUDENT_ARRAY[$COUNTER]}"
	[ -z "$STUDENT_USERNAME" ] && STUDENT_USERNAME=not_set
	#Check to see that the user exists
	echo "$Checksum:$STUDENT_USERNAME" | sudo -H /opt/karoshi/web_controls/exec/existcheck_user
	if [ $USEREXISTSTATUS = "$?" ]
	then
		MESSAGE=$"This user does not exist."
		show_status
	fi
	let COUNTER=$COUNTER+1
done
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/ban_user_account2.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DAY:$MONTH:$YEAR:$HOUR:$MINUTES:$INCIDENT:"$"User account banned."":$STUDENTS:$BANLENGTH" | sudo -H /opt/karoshi/web_controls/exec/ban_user_account
MESSAGE=$"Ban User account completed."
show_status
