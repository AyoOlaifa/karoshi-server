#!/bin/bash
#Change password
#Copyright (C) 2017  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Update Collabora Office"
TITLEHELP=$"This will update Collabora Office to the latest version."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Collabora_Office"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=7
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=view

#Check password settings
source /opt/karoshi/server_network/security/password_settings

echo '<form action="/cgi-bin/admin/update_collabora_office.cgi" method="post">'

if [ "$ACTION" =  update ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/update_collabora_office.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:" | sudo -H /opt/karoshi/web_controls/exec/update_collabora_office
	if [ "$?" = 0 ]
	then
		echo "<ul><li>"$"Collabora has now been updated""</li></ul>"
	fi
else
	echo 'This will update Collabora Office to the latest version.<br><br>Collabora Code version information can be viewed here:<br><br>
<a href="https://hub.docker.com/r/collabora/code/tags/" target="_blank">https://hub.docker.com/r/collabora/code/tags/</a><br><input type="hidden" name="_ACTION_" value="update"><br><input value="'$"Submit"'" class="button primary" type="submit">'
fi



echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit

