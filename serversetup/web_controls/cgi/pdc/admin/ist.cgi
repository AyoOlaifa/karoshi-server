#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Internet Usage Trends"
TITLEHELP=$"Internet Usage Trends"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Internet_Usage_Trends"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')

#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign CATEGORY
DATANAME=CATEGORY
get_data
CATEGORY=$(echo "$DATAENTRY" | sed 's/+*+/+/g' | sed 's/^+//g' | sed 's/+$//g')

#Assign SEARCHTERMS
DATANAME=SEARCHTERMS
get_data
SEARCHTERMS=$(echo "$DATAENTRY" | sed 's/+*+/+/g' | sed 's/^+//g' | sed 's/+$//g')

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign SEARCHDATE
DATANAME=SEARCHDATE
get_data
SEARCHDATE="$DATAENTRY"

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>'



if [ ! -z "$ACTION" ] && [ "$ACTION" = viewuserdata ]
then

	echo '<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	0: { sorter: false},
	2: { sorter: false},
	3: { sorter: "ipAddress" },
	4: { sorter: false}
    		}
		});
    } 
);
</script>'
else
	echo '<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>'

fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

ICON1=/images/submenus/system/edit.png
ICON2=/images/submenus/system/delete.png
ICON3=/images/submenus/internet/detailed_logs.png

function show_status {
echo '<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/ist.cgi";
</script>
</body>
</html>'
exit
}

function check_categories {
	if [  -d /opt/karoshi/server_network/ist/categories/ ]
	then
		if [[ $(ls -1 /opt/karoshi/server_network/ist/categories/ | wc -l) = 0 ]]
		then
			ACTION=addcategory
			CATEGORY=""
		fi
	else
		ACTION=addcategory
		CATEGORY=""
	fi
}

if [ -z "$ACTION" ]
then
	ACTION=viewcategories
	check_categories
fi

if [ -z "$SERVERNAME" ]
then
	SERVERNAME=notset
fi
if [ -z "$SERVERTYPE" ]
then
	SERVERTYPE=notset
fi

#########################
#Check data
#########################
if [ "$ACTION" != viewcategories ] && [ "$ACTION" != addcategory ] && [ "$ACTION" != reallyaddcategory ] && [ "$ACTION" != editcategory ] && [ "$ACTION" != deletecategory ] && [ "$ACTION" != reallydeletecategory ] && [ "$ACTION" != viewdata ] && [ "$ACTION" != viewuserdata ]
then
	MESSAGE=$"You have not entered a correct action."
	show_status
fi


if [ "$ACTION" = viewcategories ] || [ "$ACTION" = reallyaddcategory ] || [ "$ACTION" = reallydeletecategory ]
then
	ACTION2=addcategory
	ACTIONTEXT=$"Add Category"
	ICON="$ICON4"
else
	ACTION2=viewcategories
	ACTIONTEXT=$"View Categories"
	ICON="$ICON5"
fi


echo '
<form action="/cgi-bin/admin/ist.cgi" method="post">
	<button class="button" name="_DoAction_" value="_ACTION_'"$ACTION2"'_">
		'"$ACTIONTEXT"'
	</button>
</form>'


if [ "$ACTION" = deletecategory ]
then
	#Prompt to delete the category
	CATEGORY="${CATEGORY//%2B/ }"

	echo '<form action="/cgi-bin/admin/ist.cgi" method="post"><input type="hidden" name="_ACTION_" value="reallydeletecategory"><input type="hidden" name="_CATEGORY_" value="'"$CATEGORY"'">
	'"$CATEGORY"': Are you sure you want to delete this category?<br><br><input value="'$"Submit"'" class="button" type="submit"></form>'
	
fi

if [ "$ACTION" = reallyaddcategory ] || [ "$ACTION" = reallydeletecategory ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/ist.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$ACTION:$CATEGORY:$SEARCHTERMS:" | sudo -H /opt/karoshi/web_controls/exec/ist
	ACTION=viewcategories
	check_categories
fi

if [ "$ACTION" = addcategory ] || [ "$ACTION" = editcategory ]
then
	if [ ! -z "$CATEGORY" ]
	then
		CATEGORY="${CATEGORY//%2B/+}"
		SEARCHTERMS=$(sed 's/\\|/ /g' < /opt/karoshi/server_network/ist/categories/"$CATEGORY")
		CATEGORY="${CATEGORY//+/ }"
	fi

	#Show for for adding catergories
	echo '<form action="/cgi-bin/admin/ist.cgi" method="post"><input type="hidden" name="_ACTION_" value="reallyaddcategory"><table class="standard" style="text-align: left;" ><tbody>
	<tr><td style="width: 180px; height: 40px;" colspan="2"><b>'$"Add an Internet Trend Category"'</b></td><td></td></tr>
	<tr><td>'$"Category"'</td><td>'
	if [ -z "$CATEGORY" ]
	then
		echo '<input tabindex= "1" value="'"$CATEGORY"'" name="_CATEGORY_" class="karoshi-input" size="20" type="text">'
	else
		echo ''"$CATEGORY"'<input type="hidden" name="_CATEGORY_" value="'"$CATEGORY"'">'
	fi
	echo '</td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Username_Styles"><span class="icon-large-tooltip">'$"Enter in a name for the category you want to add."'</span></a></td></tr>
	<tr><td style="width: 180px;">'$"Search Criteria"'</td><td><input tabindex= "2" value="'"$SEARCHTERMS"'" name="_SEARCHTERMS_" class="karoshi-input" size="20" type="text"></td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_User#Username_Styles"><span class="icon-large-tooltip">'$"Enter in the key words to be searched for separated by spaces."'</span></a></td></tr>
	</tbody></table><br>
	<input value="'$"Submit"'" class="button" type="submit"></form>
	'
fi

if [ "$ACTION" = viewcategories ]
then
	echo '<form action="/cgi-bin/admin/ist.cgi" method="post"><table id="myTable" class="tablesorter" style="text-align: left;" ><thead>
<tr><th style="width: 180px;"><b>'$"Category"'</b></th><th style="width: 250px;"><b>'$"Search Criteria"'</b></th><th style="width: 60px;"><b>'$"Edit"'</b></th><th style="width: 60px;"><b>'$"Delete"'</b></th><th style="width: 60px;"><b>'$"View"'</b></th></tr></thead><tbody>'
	for CATEGORY in $(ls -1 /opt/karoshi/server_network/ist/categories/)
	do
		CATEGORY2="${CATEGORY//+/ }"
		SEARCH=$(sed 's/\\|/ /g' < /opt/karoshi/server_network/ist/categories/"$CATEGORY" )
		echo '<tr><td>'"$CATEGORY2"'</td><td>'"$SEARCH"'</td><td>
				<button class="info" name="_Edit_" value="_ACTION_editcategory_CATEGORY_'"$CATEGORY"'_">
				<img src="'"$ICON1"'" alt="'$"Edit"'">
				<span>'$"Edit"'<br>'"$CATEGORY2"'</span>
				</button>
			</td><td>
				<button class="info" name="_Delete_" value="_ACTION_deletecategory_CATEGORY_'"$CATEGORY"'_">
				<img src="'"$ICON2"'" alt="'$"Delete"'">
				<span>'$"Delete"'<br>'"$CATEGORY2"'</span>
				</button>
			</td><td>

				<button class="info" name="_View_" value="_ACTION_viewdata_CATEGORY_'"$CATEGORY"'_">
				<img src="'"$ICON3"'" alt="'$"View"'">
				<span>'$"View"'<br>'"$CATEGORY2"'</span>
				</button>
			</td></tr>'
	done
	echo '</tbody></table></form>'
fi

if [ "$ACTION" = viewdata ] || [ "$ACTION" = viewuserdata ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/ist.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$ACTION:$CATEGORY:$SEARCHTERMS:$USERNAME:$SEARCHDATE:" | sudo -H /opt/karoshi/web_controls/exec/ist
fi

echo '</display-karoshicontent></body></html>'
exit

