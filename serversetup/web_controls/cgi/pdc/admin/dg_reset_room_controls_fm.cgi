#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Reset Room Controls"
TITLEHELP=$"This will schedule times when all internet room controls are reset to allow internet access."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Room_Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="dg_room_controls_fm.cgi" method="post">
	<button class="button" name="_ViewRoomControls_" value="_">
		'$"Room Controls"'
	</button>
</form>'

ICON2=/images/submenus/internet/reset_room_controls_delete.png
echo '
<form action="/cgi-bin/admin/dg_reset_room_controls.cgi" method="post"><br>
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input"><b>'$"Time"'</b></td>
				<td>
					<select name="_HOURS_" style="width: 90px; display: inline;">
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
					</select> : 
					<select name="_MINUTES_" style="width: 90px; display: inline;">
						<option value="00">00</option>
						<option value="05">05</option>
						<option value="10">10</option>
						<option value="15">15</option>
						<option value="20">20</option>
						<option value="25">25</option>
						<option value="30">30</option>
						<option value="35">35</option>
						<option value="40">40</option>
						<option value="45">45</option>
						<option value="50">50</option>
						<option value="55">55</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<input name="_ACTION_add_" type="submit" class="button primary" value="'$"Add reset time"'">
</form>'

#Show any existing reset times
if [ -d /opt/karoshi/server_network/internet_room_controls_reset ]
then
	if [[ $(ls -1 /opt/karoshi/server_network/internet_room_controls_reset | wc -l) -gt 0 ]]
	then
		echo '

<table id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Reset times"'</b></th>
			<th style="width: 70px; vertical-align: top;">'$"Delete"'</th>
		</tr>
	</thead>
	<tbody>'

		for RESETTIMES in /opt/karoshi/server_network/internet_room_controls_reset/*
		do
			RESETTIME=$(basename "$RESETTIMES")
			echo '
		<tr>
			<td>'"$RESETTIME"'</td>
			<td>
				<form style="display: inline;" action="/cgi-bin/admin/dg_reset_room_controls.cgi" method="post">
					<button class="info" name="_DeleteTime_" value="_ACTION_delete_TIME_'"$RESETTIME"'_">
					<img src="'"$ICON2"'" alt="'$"Delete"'">
					<span>'$"Delete time"' - '"$RESETTIME"'</span>
					</button>
				</form>
			</td>
		</tr>'
		done
		echo '
	</tbody>
</table>'
	fi
fi

echo '
</display-karoshicontent>
</body>
</html>'
exit

