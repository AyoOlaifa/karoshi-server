#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Boot Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/view_karoshi_web_admin_log.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Generate navigation bar
if [ "$MOBILE" = no ]
then
	ICON1=/images/assets/locationm.png
	ICON6=/images/assets/search.png
	SEARCHW=200
else
	ICON1=/images/assets/locationm.png
	ICON6=/images/assets/searchm.png
	SEARCHW=150
fi

if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi

if [ "$LOCATION_COUNT" != 0 ]
then
	if [ -d /opt/karoshi/asset_register/locations/ ]
	then
		if [[ $(ls -1 /opt/karoshi/asset_register/locations/ | wc -l) -gt 0 ]]
		then
			ROWCOUNT=7
			[ "$MOBILE" = yes ] && ROWCOUNT=4
			WIDTH=90
			[ "$MOBILE" = yes ] && WIDTH=70

			echo '
			<form action="/cgi-bin/admin/client_boot_controls.cgi" method="post">
				<table>
					<tbody>
						<tr>
							<td class="karoshi-input">'$"Search"'</td>
							<td class="karoshi-input"><input class="karoshi-input" tabindex= "1" name="_LOCATION_SEARCHNOTVALID_SEARCH_" size="20" type="text"></td>
							<td>
								<button class="info" name="_Search_" value="_BUTTON_">
									<img src="'"$ICON6"'" alt="'$"Search"'">
									<span>'$"Search"'</span>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>'


			echo '
			<form action="/cgi-bin/admin/client_boot_controls.cgi" method="post">
				<table>
					<tbody>
						<tr>'
			LOCCOUNTER=1

			for LOCATIONS in /opt/karoshi/asset_register/locations/*
			do
				LOCATION=$(basename "$LOCATIONS")
				[ "$LOCCOUNTER" = "$ROWCOUNT" ] && echo '</tr><tr>'
				let LOCCOUNTER="$LOCCOUNTER"+1
				[ "$LOCCOUNTER" -gt "$ROWCOUNT" ] && LOCCOUNTER=1
				echo '
							<td style="width: '"$WIDTH"'px; vertical-align:middle;">
								<button class="info" name="_ShowLocation_" value="_LOCATION_'"$LOCATION"'_">
									<img src="'"$ICON1"'" alt="'"$LOCATION"'">
									<span>'$"Client Boot Controls"'<br>'"$LOCATION"'</span>
								</button>
							</td>
							<td>'"$LOCATION"'</td>'
			done
			echo '
						</tr>
					</tbody>
				</table>
			</form>'
		fi
	else
		echo $"The asset register is not in use."
	fi
else
	echo $"The asset register is not in use."
fi

echo '
		</display-karoshicontent>
	</body>
</html>'
exit

