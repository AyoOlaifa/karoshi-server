#!/bin/bash
#Copyright (C) 2008  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Site Internet Logs"
TITLEHELP=$"Internet logs are updated every three minutes."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=9
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SEARCH
DATANAME=SEARCH
get_data
SEARCH="$DATAENTRY"

#Assign DATE
DATANAME=DATE
get_data
DATE=$(echo "$DATAENTRY" | tr -cd '0-9-')

function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/dg_view_site_logs_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that SEARCH is not blank
if [ -z "$SEARCH" ]
then
	MESSAGE=$"The log search cannot be blank."
	show_status
fi

#Check to see that DATE is not blank
if [ -z "$DATE" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

DAY=$(echo "$DATE" | cut -d- -f1)
MONTH=$(echo "$DATE" | cut -d- -f2)
YEAR=$(echo "$DATE" | cut -d- -f3)

#Check to see that DAY is not blank
if [ -z "$DAY" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check to see that MONTH is not blank
if [ -z "$MONTH" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check to see that YEAR is not blank
if [ -z "$YEAR" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check that day is not greater than 31
if [ "$DAY" -gt 31 ]
then
	MESSAGE=$"Date input error."
	show_status
fi

#Check that the month is not greater than 12
if [ "$MONTH" -gt 12 ]
then
	MESSAGE=$"Date input error."
	show_status
fi

#Check that the year is in range
if [ "$YEAR" -lt 2006 ] || [ "$YEAR" -gt 3006 ]
then
	MESSAGE=$"The year is not valid."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/dg_view_site_logs.cgi | cut -d' ' -f1)
#View logs
echo '<form action="/cgi-bin/admin/dg_view_site_logs2.cgi" name="selectedsites" method="post">
<input name="_LOGDATE_" value="'"$DAY"'-'"$MONTH"'-'"$YEAR"'" type="hidden">
<button class="button" formaction="dg_view_site_logs_fm.cgi" name="_ChooseSite_" value="_">
	'$"Choose Website"'
</button>
<br>
<br>
'

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SEARCH:$DAY:$MONTH:$YEAR:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/dg_view_site_logs
EXEC_STATUS="$?"
if [ "$EXEC_STATUS" = 102 ]
then
	MESSAGE=$"No log exists for this date."
	show_status
fi
if [ $EXEC_STATUS = 103 ]
then
	MESSAGE=$"No sites for this search exist in this log."
	show_status
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
