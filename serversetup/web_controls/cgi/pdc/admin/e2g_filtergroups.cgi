#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk
#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Filter Management"

#Generate head section
source /opt/karoshi/web_controls/generate_head_section_admin

TITLE=""

#Generate page top
source /opt/karoshi/web_controls/generate_page_top_admin

#Page content
echo '
<div id="karoshicontent">
</div>
'

#Generate side bar
source /opt/karoshi/web_controls/generate_sidebar_admin

#Add in scripts
/opt/karoshi/web_controls/generate_scripts

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter();
	$("#myTable2").tablesorter();  
    } 
);
</script>'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+%' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=16
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1

		if [ "$DO_BREAK" = yes ]
		then
			DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
			break
		else
			DATAENTRY="$DATAENTRY""_"$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		fi
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
DO_BREAK=yes
get_data
ACTION="$DATAENTRY"

#Assign FILTERNAME
DATANAME=FILTERNAME
get_data
FILTERNAME="${DATAENTRY//REPLACEUNDERSCORE/_}"

#Assign FILTERDATA
DATANAME=FILTERDATA
get_data
FILTERDATA="${DATAENTRY//REPLACEUNDERSCORE/_}"

#Assign FILTERDATA2
DATANAME=FILTERDATA2
get_data
FILTERDATA2="${DATAENTRY//REPLACEUNDERSCORE/_}"

#Assign FILTERDATA3
DATANAME=FILTERDATA3
get_data
FILTERDATA3="${DATAENTRY//REPLACEUNDERSCORE/_}"

if [ ! -z "$ACTION" ]
then
	if [ "$ACTION" = modifycustomsite ]
	then
		END_POINT=30
		DATANAME=FILTERDATA4
		DO_BREAK=no
		get_data
		FILTERDATA4="${DATAENTRY//REPLACEUNDERSCORE/_}"
	fi

	if [ "$ACTION" = reallyaddtime ]
	then
		END_POINT=30
		DATANAME=HOUR1
		DO_BREAK=yes
		get_data
		HOUR1="${DATAENTRY//REPLACEUNDERSCORE/_}"

		DATANAME=HOUR2
		DO_BREAK=yes
		get_data
		HOUR2="${DATAENTRY//REPLACEUNDERSCORE/_}"

		DATANAME=MINUTES1
		DO_BREAK=yes
		get_data
		MINUTES1="${DATAENTRY//REPLACEUNDERSCORE/_}"

		DATANAME=MINUTES2
		DO_BREAK=yes
		get_data
		MINUTES2="${DATAENTRY//REPLACEUNDERSCORE/_}"

		DATANAME=DAYOFWEEK
		DO_BREAK=yes
		get_data
		DAYOFWEEK="${DATAENTRY//REPLACEUNDERSCORE/_}"
	fi
fi

[ -z "$ACTION" ] && ACTION=view 

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/e2g_filtergroups.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$FILTERNAME:$FILTERDATA:$FILTERDATA2:$FILTERDATA3:$FILTERDATA4:$MOBILE:$HOUR1:$MINUTES1:$HOUR2:$MINUTES2:$DAYOFWEEK:" | sudo -H /opt/karoshi/web_controls/exec/e2g_filtergroups

echo '</display-karoshicontent></body></html>'
exit

