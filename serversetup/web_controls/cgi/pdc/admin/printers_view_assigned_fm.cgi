#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View Assigned Printers"
TITLEHELP=$"This shows the printers that have been assigned to locations."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Assigned_Printers"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Check that a print server has been declared
function show_status {
echo '<script>
alert("'$"A print server has not yet been set up."'");
window.location = "karoshi_servers_view.cgi";
</script>
</body></html>'
exit
}

[ ! -f /opt/karoshi/server_network/printserver ] && show_status

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form action="/cgi-bin/admin/printers_view_assigned.cgi" method="post">
		<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
			'$"Show Printers"'
		</button>

		<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinters_" value="_">
			'$"Delete Printer"'
		</button>

		<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
			'$"Locations"'
		</button>
		</form>
		<form action="/cgi-bin/admin/printers_view_assigned.cgi" method="post">'

#Check to see that locations.txt exists
if [ ! -f /var/lib/samba/netlogon/locations.txt ]
then
	echo $"No Printers have been assigned to a location."
	echo '</display-karoshicontent></body></html>'
	exit
fi
COUNTER=$(grep -n ^--start-- /var/lib/samba/netlogon/printers.txt | cut -d: -f1)
let COUNTER="$COUNTER"+1
NOOFLINES=$(wc -l < /var/lib/samba/netlogon/printers.txt)

if [ "$COUNTER" -le "$NOOFLINES" ]
then
	#Create top of table
	echo '
	<table id="myTable" class="tablesorter" style="text-align: left;">
		<thead>
			<tr>
				<th style="width: 200px; height: 20px;"><b>'$"Location"'</b></th><th style="width: 150px;"><b>'$"Assigned Printers"'</b></th><th style="width: 80px;">'$"Default"'</th><th style="width: 80px;">'$"Remove"'</th></tr></thead><tbody>'
	#Show locations and printers
	while [ "$COUNTER" -le "$NOOFLINES" ]
	do
		DATAENTRY=$(sed -n "$COUNTER,$COUNTER""p" /var/lib/samba/netlogon/printers.txt)
		#Assign data entry to an array
		if [ ! -z "$DATAENTRY" ]
		then
			DATARRAY=( ${DATAENTRY//,/ } )
			ARRAYCOUNT="${#DATARRAY[@]}"
			let ARRAYCOUNT="$ARRAYCOUNT"-1
			DEFAULTPRINTER="${DATARRAY[$ARRAYCOUNT]}"
			#Show printers
			ARRAYCOUNTER=2
			while [ "$ARRAYCOUNTER" -lt "$ARRAYCOUNT" ]
			do
				#Show location 
				echo '<tr><td style="height: 35px;">'"${DATARRAY[0]}"'</td><td>'"${DATARRAY[$ARRAYCOUNTER]}"'</td>'
				#Show printer actions
				#Set default option
				if [ "${DATARRAY[$ARRAYCOUNTER]}" != "$DEFAULTPRINTER" ]
				then
					echo '<td style="text-align: center;">

				<button class="info" name="____SetDefault____" value="____PRINTACTION____default:'"${DATARRAY[0]}"':'"${DATARRAY[$ARRAYCOUNTER]}"'____">
				<img src="/images/help/printer_make_default.png" alt="'$"Set Default"'">
				<span>'$"Set Default"'</span>
				</button>
				</td>'
				else
					echo '<td style="text-align: center;">
				<a class="info" href="javascript:void(0)"><img class="images" alt="" src="/images/help/printer_default.png"><span>'$"Default Printer"'</span></a>
				'
				fi

				#Delete option
				echo '<td>
				<button class="info" name="____RemovePrinter____" value="____PRINTACTION____delete:'"${DATARRAY[0]}"':'"${DATARRAY[$ARRAYCOUNTER]}"'____">
				<img src="/images/submenus/printer/remove_printer.png" alt="'$"Remove Printer"'">
				<span>'$"Remove Printer"'</span>
				</button>
				</td></tr>'
				let ARRAYCOUNTER="$ARRAYCOUNTER"+1
			done
			#Clear array
			unset DATARRAY
		fi
		let COUNTER="$COUNTER"+1
	done
	#End table
	echo '</tbody></table>'

else
	echo '<ul><li>'$"No Printers have been assigned"'</li></ul>'
fi

echo '</display-karoshicontent></form></body></html>'
exit

