#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Desktop Icons"
TITLEHELP=$"Choose the profiles that you want to copy the new profile to."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_a_new_profile"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script>
<!--
function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}
// -->
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

THISYEAR=$(date +%Y)
let GROUPSTART=$THISYEAR-8
let GROUPEND=$THISYEAR+5

echo '<form action="/cgi-bin/admin/windows_client_profile_select.cgi" name="selectgroups" method="post">'

WIDTH1=15
WIDTH2=100
#Check to see if any files have been uploaded
FILECOUNT=0
if [ -d /var/www/karoshi/win_profile_upload/ ]
then
	FILECOUNT=`ls -1 /var/www/karoshi/win_profile_upload/ | wc -l`
	FILEDATA=`ls -1 /var/www/karoshi/win_profile_upload/ | sed -n 1,1p`
	FILENAME=`echo "$FILEDATA" | grep '\<zip\>'`
	[ `echo $FILENAME'null' | sed 's/ //g'` = null ] && FILENAME=`echo "$FILEDATA" | grep '\<tar\.gz\>'`
fi

if [ $FILECOUNT != 1 ]
then
	echo ''$"An incorrect number of files have been uploaded."'</div></div></body></html>'
	exit
fi

if [ -z "$FILENAME" ]
then
	echo ''$"You have not uploaded a zip or tar.gz archive."'</div></div></body></html>'
	exit
else
	FILENAME=`ls -1 /var/www/karoshi/win_profile_upload/ | sed -n 1,1p`
	#Correct spaces
	FILENAME2=`echo $FILENAME | sed 's/ /SPACECORRECT/g'`
	[ -f /var/www/karoshi/win_profile_upload/$FILENAME2 ] || mv /var/www/karoshi/win_profile_upload/"$FILENAME" /var/www/karoshi/win_profile_upload/$FILENAME2
fi
#Show list of profiles to choose from

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Uploaded file"'</td>
			<td>'$FILENAME'</td>
		</tr>
		<tr>
			<td>'$"Windows Version"'</td>
			<td>
				<select name="___WINDOWSVER___" class="karoshi-input">'
for WINDOWSVERSION in $(ls -1 /opt/karoshi/server_network/clients/windows_client_versions | sort -V)
do
	source /opt/karoshi/server_network/clients/windows_client_versions/"$WINDOWSVERSION"
	echo '
				<option value="'$WINDOWS_VERSION'">'$WINDOWS_NAME'</option>'
done	
echo '
				</select>
			</td>
		</tr>
	</tbody>
</table>
'$"Select the groups that you want to copy this profile to."''

FILENAME=$FILENAME2
echo '

  <br><input name="___FILENAME___" value="'$FILENAME'" type="hidden">
  <br>
<table>
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Group"'</b></th>
			<th></th>
		</tr>
	</thead>
	<tbody>'

for GROUPNAMES in /opt/karoshi/server_network/group_information/*
do
	GROUPNAME=$(basename "$GROUPNAMES")
	if [ $GROUPNAME != optional_groups ]
	then
		echo '
		<tr>
			<td>'"$GROUPNAME"'</td>
			<td style="text-align:right"><input id="PriGroup'"$GROUPNAME"'" name="___PRIGROUP___" value="'"$GROUPNAME"'" type="checkbox"><label for="PriGroup'"$GROUPNAME"'"> </label></td>
		</tr>'
	fi
done

echo '</tbody></table><br>
  <input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"> <input type="button" class="button" onclick="SetAllCheckBoxes('\'selectgroups\'', '\'___PRIGROUP___\'', true);" value="'$"Select all"'">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
