#!/bin/bash
#Change my password
#Copyright (C) 2007  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
STYLESHEET=defaultstyle.css
[ -f /opt/karoshi/web_controls/global_prefs ] && source /opt/karoshi/web_controls/global_prefs

TITLE=$"Change My Password"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_all

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/all/change_my_password.cgi" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Username"'</td>
			<td><input class="karoshi-input" tabindex= "1" name="____USERNAME____" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Please enter in your username."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Current Password"'</td>
			<td><input class="karoshi-input" tabindex= "2" name="____PASSWORD1____" size="20" type="password"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the password that you use to log into the system."'</span></a></td>
		</tr>
		<tr>
			<td>'$"New Password"'</td>
			<td><input class="karoshi-input" tabindex= "3" name="____PASSWORD2____" size="20" type="password"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the new password that you want to have."'<br><br>'
[ "$PASSWORDCOMPLEXITY" = on ] && echo ''$"Upper and lower case characters and numbers are required."'<br><br>'
	echo ''$"The Minimum password length is "''"$MINPASSLENGTH"'.<br><br><b>'$"Allowed Special Characters"'</b><br><br>'$"The following special characters are allowed"'<br><br> space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~</span></a></td>
		</tr>
       <tr>
        		<td>'$"Confirm New Password"'</td>
       			<td><input class="karoshi-input" tabindex= "4" name="____PASSWORD3____" size="20" type="password"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in your new password again here."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit
