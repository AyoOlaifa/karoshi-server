#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
LOG_DATE=$(date +%F)
########################
#Check md5checksum
########################
if ! test -f /opt/karoshi/web_controls/checksums/admin_checksums/email_quota_settings2_cgi
then
	echo "$(date): email_quota_settings_apply - No Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
source /opt/karoshi/web_controls/checksums/admin_checksums/email_quota_settings2_cgi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_quota_settings2.cgi | cut -d' ' -f1)
[ -z "$Checksum" ] && Checksum=not_set
if [ "$Checksum"'check' != "$email_quota_settings2_cgi"'check' ]
then
	echo "$(date): email_quota_settings_apply - Incorrect Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

########################
#Get variables
########################
if [ "$#" != 1 ]
then
	echo "$(date): email_quota_settings_apply - incorrect number of arguments" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
REMOTE_USER=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f1)
REMOTE_ADDR=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f2)
RemoteCheckSum=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f3)
ADMIN_EMAIL=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f4)
THRESHOLD1=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f5)
THRESHOLD2=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f6)
THRESHOLD3=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f7)
THRESHOLD4=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f8)
INTERVAL1=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f9)
INTERVAL2=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f10)
INTERVAL3=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f11)
INTERVAL4=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\%-' | cut -s -d: -f12)

########################
#Check data
########################
if [ "$RemoteCheckSum"'check' != "$Checksum"'check' ]
then
	echo "$(date): email_quota_settings_apply - Not called by email_quota_settings2.cgi" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_USER" ]
then
	echo "$(date): email_quota_settings_apply - Blank remote user" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_ADDR" ]
then
	echo "$(date): email_quota_settings_apply - Blank remote tcpip address" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$ADMIN_EMAIL" ]
then
	echo "$(date): email_quota_settings_apply - Blank admin email" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$THRESHOLD1" ]
then
	echo "$(date): email_quota_settings_apply - Blank threshold1" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$THRESHOLD2" ]
then
	echo "$(date): email_quota_settings_apply - Blank threshold2" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$THRESHOLD3" ]
then
	echo "$(date): email_quota_settings_apply - Blank threshold3" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$THRESHOLD4" ]
then
	echo "$(date): email_quota_settings_apply - Blank threshold4" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$INTERVAL1" ]
then
	echo "$(date): email_quota_settings_apply - Blank interval1" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$INTERVAL2" ]
then
	echo "$(date): email_quota_settings_apply - Blank interval2" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$INTERVAL3" ]
then
	echo "$(date): email_quota_settings_apply - Blank interval3" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$INTERVAL4" ]
then
	echo "$(date): email_quota_settings_apply - Blank interval4" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	echo "$(date): email_quota_settings_apply - access denied to $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
############################
#Apply settings
############################
echo "$(date): email_quota_settings_apply - by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"

EMAILSERVER=$(sed -n 1,1p /opt/karoshi/server_network/emailserver)

if [ -z "$EMAILSERVER" ] 
then
	echo "$(date): email_quota_settings_apply - blank email server by $REMOTE_USER from $REMOTE_ADD" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi


if [[ "$EMAILSERVER" != $(hostname-fqdn) ]]
then
	ssh -x -o PasswordAuthentication=no "$EMAILSERVER" '
	echo "#!/bin/bash
#Settings
ADMIN_EMAIL='"$ADMIN_EMAIL"'
#Warning thresholds in percent
THRESHOLD1='"$THRESHOLD1"'
THRESHOLD2='"$THRESHOLD2"'
THRESHOLD3='"$THRESHOLD3"'
THRESHOLD4='"$THRESHOLD4"'
#Warning intervals in days
INTERVAL1='"$INTERVAL1"'
INTERVAL2='"$INTERVAL2"'
INTERVAL3='"$INTERVAL3"'
INTERVAL4='"$INTERVAL4"'" > /opt/karoshi/postfixdata/mailboxcheck_settings
	'
else
	echo "#!/bin/bash
#Settings
ADMIN_EMAIL=$ADMIN_EMAIL
#Warning thresholds in percent
THRESHOLD1=$THRESHOLD1
THRESHOLD2=$THRESHOLD2
THRESHOLD3=$THRESHOLD3
THRESHOLD4=$THRESHOLD4
#Warning intervals in days
INTERVAL1=$INTERVAL1
INTERVAL2=$INTERVAL2
INTERVAL3=$INTERVAL3
INTERVAL4=$INTERVAL4" > /opt/karoshi/postfixdata/mailboxcheck_settings
fi
exit

