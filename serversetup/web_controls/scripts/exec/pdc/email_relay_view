#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
LOG_DATE=$(date +%F)

########################
#Check md5checksum
########################
if ! test -f /opt/karoshi/web_controls/checksums/admin_checksums/email_relay_cgi
then
	echo "$(date): email_relay_view" - No Checksum >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
source /opt/karoshi/web_controls/checksums/admin_checksums/email_relay_cgi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_relay.cgi | cut -d' ' -f1)
[ -z "$Checksum" ] && Checksum=not_set
if [ $Checksum'check' != $email_relay_cgi'check' ]
then
	echo "$(date): email_relay_view - Incorrect Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

########################
#Get variables
########################
numArgs="$#"
if [ "$numArgs" != 1 ]
then
	echo "$(date): email_relay_view - incorrect number of arguments" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
REMOTE_USER=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\-' | cut -s -d: -f1)
REMOTE_ADDR=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\-' | cut -s -d: -f2)
RemoteCheckSum=$(echo "$*" | tr -cd 'A-Za-z0-9\._:\-' | cut -s -d: -f3)
########################
#Check data
########################
if [ $RemoteCheckSum'check' != $Checksum'check' ]
then
	echo "$(date): email_relay_view - Not called by email_relay_view.cgi" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_USER" ]
then
	echo "$(date): email_relay_view - Blank remote user" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_ADDR" ]
then
	echo "$(date): email_relay_view - Blank remote tcpip address" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	echo "$(date): email_relay_view - access denied to $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
echo "$(date): email_relay_view - by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"


TEXTDOMAIN=karoshi-server

############################
#Apply relay settings
############################
EMAILSERVER=$(sed -n 1,1p /opt/karoshi/server_network/emailserver)

if [ -z "$EMAILSERVER" ] 
then
	echo "$(date): email_relay_view - blank email server by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

RELAYHOST=""
if [[ "$EMAILSERVER" = $(hostname-fqdn) ]]
then
	grep ^"relayhost =" /etc/postfix/main.cf | sed -n 1,1p | cut -d"=" -f2 | sed "s/ //g" | sed "s/\[//g" | sed "s/\]//g"
else
	RELAYHOST=$(ssh -x -o PasswordAuthentication=no "$EMAILSERVER" '
	grep ^"relayhost =" /etc/postfix/main.cf | sed -n 1,1p | cut -d"=" -f2 | sed "s/ //g" | sed "s/\[//g" | sed "s/\]//g"

')

if [ -z "$RELAYHOST" ]
then
	CHECK2="checked=\"checked\""
else
	CHECK1="checked=\"checked\""
fi

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Send E-mail directly"'</td>
			<td><input id="Direct" name="_RELAY_" value="direct" '$CHECK1' type="radio"><label for="Direct"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Use_E-Mail_Relay#Send_E-Mail_directly"><span class="icon-large-tooltip">'$"Choose this option of you want to send your emails directly out to the internet."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Use E-mail relay"'</td>
			<td><input id="Relay" name="_RELAY_" value="relay" '$CHECK2' type="radio"><label for="Relay"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Relay"'</td>
			<td><input class="karoshi-input" name="_RADDRESS_" size="30" type="text" value="'$RELAYHOST'"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Use_E-Mail_Relay#Use_E-Mail_relay"><span class="icon-large-tooltip">'$"Enter in the address of the email relay that you want to relay the email through."'</span></a></td>
		</tr>
	</tbody>
</table>'
fi


